﻿using Furion.DependencyInjection;
using Furion.FriendlyException;
using Microsoft.AspNetCore.Mvc.Filters;
using Queer.Util.Helper;
using Queer.Util.Model;
using System.Threading.Tasks;

namespace Queer.WebApi.Handlers
{
    /// <summary>
    /// 异常日志操作
    /// </summary>
    public class LogExceptionHandler : IGlobalExceptionHandler, ISingleton
    {
        public Task OnExceptionAsync(ExceptionContext context)
        {
            // 写日志文件
            if (context.Exception.GetType() == typeof(BusinessException))
                LogHelper.Error("业务异常：" + context.Exception);
            else
                LogHelper.Error("系统异常：" + context.Exception);

            return Task.CompletedTask;
        }
    }
}
