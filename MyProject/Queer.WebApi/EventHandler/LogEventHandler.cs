﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.EventBridge;
using Queer.Entity;
using Queer.Util.Helper;

namespace Queer.WebApi.EventHandler
{
    /// <summary>
    /// 日志总线
    /// </summary>
    [EventHandler("log")]
    public class LogEventHandler : IEventHandler
    {
        private readonly IRepository<LogApiEntity> _logApiDB;

        public LogEventHandler(IRepository<LogApiEntity> logApiDB)
        {
            _logApiDB = logApiDB;
        }

        /// <summary>
        /// API日志
        /// </summary>
        [EventMessage("logapi")]
        public async Task InitServerAsync(EventMessage<LogApiEntity> eventMessage)
        {
            var logApiEntity = eventMessage.Payload;
            logApiEntity.Id = IdGeneratorHelper.Instance.GetId();
            logApiEntity.CreateTime = DateTime.Now;
            await _logApiDB.InsertNowAsync(logApiEntity);
        }



    }
}
