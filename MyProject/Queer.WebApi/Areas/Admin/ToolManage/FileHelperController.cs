﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Queer.Util.Helper;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.ToolManage
{
    /// <summary>
    /// 文件上传、下载
    /// </summary>
    [Route("ToolManage/[controller]")]
    public class FileHelperController : BaseAdminController
    {
        /// <summary>
        /// 文件批量上传
        /// </summary>
        /// <param name="uploadFileType"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost, NonUnify]
        public async Task<TData<UpLoadFileResult>> UploadFile([FromForm] int uploadFileType, List<IFormFile> file)
        {
            FileHelper LH = new FileHelper();
            return await LH.UploadFileAsync(uploadFileType, file);
        }

        /// <summary>
        /// 单个文件上传
        /// </summary>
        [HttpPost, NonUnify]
        public async Task<TData> UploadSingleFile([FromForm] int uploadFileType, IFormCollection file)
        {
            return await FileHelper.UploadFile(uploadFileType, file.Files);
        }

        /// <summary>
        /// 单个Base图片上传
        /// </summary>
        [HttpPost, NonUnify]
        public TData UploadImgFile([FromBody] UploadImages uploadImages)
        {
            return FileHelper.UploadFile(uploadImages);
        }
    }
}