﻿using Microsoft.AspNetCore.Mvc;
using Queer.Util.Helper;
using Queer.Util.Model;
using System;

namespace Queer.WebApi.Areas.Admin.ToolManage
{
    /// <summary>
    /// 服务器信息
    /// </summary>
    [Route("ToolManage/[controller]")]
    public class ServerController : BaseAdminController
    {
        #region 获取数据（新）

        /// <summary>
        /// 获取服务器状态
        /// </summary>
        [HttpGet]
        public TData<ComputerInfo> GetServerStatus()
        {
            TData<ComputerInfo> obj = new TData<ComputerInfo>();
            ComputerInfo computerInfo = ComputerHelper.GetComputerInfo();

            obj.Data = computerInfo;
            obj.Tag = 1;
            return obj;
        }

        /// <summary>
        /// 获取服务器基本参数
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public TData<object> GetServerInfo()
        {
            string ip = NetHelper.GetWanIp(); // 服务器外网IP
            string ipLocation = IpLocationHelper.GetIpLocation(ip); // IP位置
            string serviceName = Environment.MachineName; // 服务器名称
            string systemOs = System.Runtime.InteropServices.RuntimeInformation.OSDescription; // 服务器系统
            string lanIp = @NetHelper.GetLanIp(); // 局域网IP
            string osArchitecture = System.Runtime.InteropServices.RuntimeInformation.OSArchitecture.ToString(); // 系统架构
            string processorCount = Environment.ProcessorCount.ToString(); // CPU核心数
            string frameworkDescription = System.Runtime.InteropServices.RuntimeInformation.FrameworkDescription; // .net core版本
            string ramUse = ((Double)System.Diagnostics.Process.GetCurrentProcess().WorkingSet64 / 1048576).ToString("N2") + " MB";
            string startTime = System.Diagnostics.Process.GetCurrentProcess().StartTime.ToString("yyyy-MM-dd HH:mm");

            TData<object> obj = new TData<object>();
            obj.Data = new { ip, ipLocation, serviceName, systemOs, lanIp, osArchitecture, processorCount, frameworkDescription, ramUse, startTime };
            obj.Tag = 1;
            return obj;
        }

        #endregion

    }
}