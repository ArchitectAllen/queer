﻿using Microsoft.AspNetCore.Mvc;
using Queer.CodeGenerator;
using Queer.IBusiness.SystemManage;
using Queer.Model;
using Queer.Model.Result;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using StackExchange.Profiling.Internal;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Queer.WebApi.Areas.Admin.ToolManage
{
    /// <summary>
    /// 代码生成
    /// </summary>
    [Route("ToolManage/[controller]")]
    public class CodeGeneratorController : BaseAdminController
    {
        private readonly IDatabaseTableBLL _databaseTableBLL;
        private readonly SingleTableTemplate _singleTableTemplate;

        public CodeGeneratorController(IDatabaseTableBLL databaseTableBLL, SingleTableTemplate singleTableTemplate)
        {
            _databaseTableBLL = databaseTableBLL;
            _singleTableTemplate = singleTableTemplate;
        }

        #region 获取数据

        [HttpGet]
        public async Task<TData<List<ZtreeInfo>>> GetTableFieldTreeListJson([FromQuery] string tableName)
        {
            TData<List<ZtreeInfo>> obj = await _databaseTableBLL.GetTableFieldZtreeList(tableName);
            return obj;
        }

        [HttpGet]
        public async Task<TData<List<ZtreeInfo>>> GetTableFieldTreePartListJson([FromQuery] string tableName, [FromQuery] int upper = 0)
        {
            TData<List<ZtreeInfo>> obj = await _databaseTableBLL.GetTableFieldZtreeList(tableName);
            if (obj.Data != null)
            {
                // 基础字段不显示出来
                obj.Data.RemoveAll(p => BaseField.BaseFieldList.Contains(p.name));
            }
            return obj;
        }


        [HttpGet]
        public async Task<TData<List<TableFieldInfo>>> GetTableFieldListJson([FromQuery] string tableName)
        {
            TData<List<TableFieldInfo>> obj = await _databaseTableBLL.GetTableFieldList(tableName);
            if (obj.Data != null)
            {
                // 基础字段不显示出来
                obj.Data.RemoveAll(p => BaseField.BaseFieldList.Contains(p.TableColumn));
            }
            return obj;
        }

        /// <summary>
        /// 获取代码生成的配置
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <param name="module">生成模块</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<TData<BaseConfigModel>> GetBaseConfigJson([FromQuery] string tableName, [FromQuery] string module)
        {
            TData<BaseConfigModel> obj = new TData<BaseConfigModel>();

            string tableDescription = string.Empty;
            TData<List<TableFieldInfo>> tDataTableField = await _databaseTableBLL.GetTableFieldList(tableName);
            List<string> columnList = tDataTableField.Data.Where(p => !BaseField.BaseFieldList.Contains(p.TableColumn)).Select(p => p.TableColumn).ToList();

            string serverPath = GlobalContext.HostingEnvironment.ContentRootPath;

            obj.Data = _singleTableTemplate.GetBaseConfig(serverPath, module, tableName, tableDescription, columnList);

            var t = obj.Data.ToJson();

            obj.Tag = 1;
            return obj;
        }

        /// <summary>
        /// 获取模块信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public TData<string[]> GetModules()
        {
            TData<string[]> obj = new TData<string[]>();
            obj.Data = SingleTableTemplate.modules;
            obj.Tag = 1;
            return obj;
        }

        #endregion

        #region 提交数据

        [HttpPost]
        public async Task<TData<object>> CodePreviewJson([FromBody] BaseConfigModel baseConfig)
        {
            TData<object> obj = new TData<object>();
            if (string.IsNullOrEmpty(baseConfig.OutputConfig.OutputModule))
            {
                obj.Message = "请选择输出到的模块";
                return obj;
            }

            TData<List<TableFieldInfo>> objTable = await _databaseTableBLL.GetTableFieldList(baseConfig.TableName);
            DataTable dt = DataTableHelper.ListToDataTable(objTable.Data);  // 用DataTable类型，避免依赖

            string codeEntity = _singleTableTemplate.BuildEntity(baseConfig, dt);
            string codeEntityParam = _singleTableTemplate.BuildEntityParam(baseConfig, dt);
            string codeService = _singleTableTemplate.BuildService(baseConfig, dt);
            string codeBusiness = _singleTableTemplate.BuildBusiness(baseConfig);
            string codeIService = _singleTableTemplate.BuildIService(baseConfig, dt);
            string codeIBusiness = _singleTableTemplate.BuildIBusiness(baseConfig);

            string codeController = _singleTableTemplate.BuildController(baseConfig);
            string codeIndex = _singleTableTemplate.BuildIndex(baseConfig, dt);
            string codeForm = _singleTableTemplate.BuildForm(baseConfig, dt);

            var json = new
            {
                CodeEntity = HttpUtility.HtmlEncode(codeEntity),
                CodeEntityParam = HttpUtility.HtmlEncode(codeEntityParam),
                CodeService = HttpUtility.HtmlEncode(codeService),
                CodeBusiness = HttpUtility.HtmlEncode(codeBusiness),
                CodeIService = HttpUtility.HtmlEncode(codeIService),
                CodeIBusiness = HttpUtility.HtmlEncode(codeIBusiness),
                CodeController = HttpUtility.HtmlEncode(codeController),
                CodeIndex = HttpUtility.HtmlEncode(codeIndex),
                CodeForm = HttpUtility.HtmlEncode(codeForm)
            };
            obj.Data = json;
            obj.Tag = 1;

            return obj;
        }

        [HttpPost]
        public async Task<TData<List<KeyValue>>> CodeGenerateJson([FromBody] BaseConfigModel baseConfig)
        {
            TData<List<KeyValue>> obj = new TData<List<KeyValue>>();

            List<KeyValue> result = await _singleTableTemplate.CreateCode(baseConfig, HttpUtility.UrlDecode(baseConfig.Code));
            obj.Data = result;
            obj.Tag = 1;

            return obj;
        }

        #endregion

    }
}