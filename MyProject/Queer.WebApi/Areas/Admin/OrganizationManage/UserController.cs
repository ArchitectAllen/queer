﻿using Furion.DatabaseAccessor.Extensions;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Queer.Entity;
using Queer.IBusiness.OrganizationManage;
using Queer.Model.Param.OrganizationManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.OrganizationManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 12:41
    /// 描 述：用户信息控制器类
    /// </summary>

    [Route("OrganizationManage/[controller]")]
    public class UserController : BaseAdminController
    {
        private readonly IUserBLL _userBLL;

        public UserController(IUserBLL userBLL)
        {
            _userBLL = userBLL;
        }

        #region 获取数据

        /// <summary>
        /// 条件查询
        /// </summary>
        [HttpGet]
        public async Task<TData<List<UserEntity>>> GetListJson([FromQuery] UserListParam param)
        {
            TData<List<UserEntity>> obj = await _userBLL.GetList(param);
            return obj;
        }

        /// <summary>
        /// 条件查询-分页
        /// </summary>
        [HttpGet]
        public async Task<TData<List<UserEntity>>> GetPageListJson([FromQuery] UserListParam param, [FromQuery] Pagination pagination)
        {
            TData<List<UserEntity>> obj = await _userBLL.GetPageList(param, pagination);
            return obj;
        }

        /// <summary>
        /// 根据ID查询
        /// </summary>
        [HttpGet]
        public async Task<TData<UserEntity>> GetFormJson([FromQuery] long id)
        {
            TData<UserEntity> obj = await _userBLL.GetEntity(id);
            return obj;
        }

        /// <summary>
        /// 页面信息初始化
        /// </summary>
        [HttpGet]
        public async Task<TData<object>> UserPageLoad()
        {
            TData<object> obj = await _userBLL.UserPageLoad();
            return obj;
        }

        /// <summary>
        /// 获取用户关联信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<TData<object>> GetUserDlcInfo([FromQuery] long userId)
        {
            var user = new UserEntity() { Id = userId };
            var obj = new TData<object>();

            // 查询关联信息
            await _userBLL.GetUserBelong(user);

            obj.Tag = 1;
            obj.Data = new { RoleIds = user.RoleIds ?? "", PositionIds = user.PositionIds ?? "" };

            return obj;
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 新增/修改 数据
        /// </summary>
        [HttpPost]
        public async Task<TData<string>> SaveFormJson([FromBody] UserEntity entity)
        {
            TData<string> obj = await _userBLL.SaveForm(entity);
            return obj;
        }

        /// <summary>
        /// 基础信息修改
        /// </summary>
        [HttpPost]
        public async Task<TData> UpdateBaseData([FromBody] UserBaseDataModel model)
        {
            var entity = model.Adapt<UserEntity>();
            await entity.UpdateNowAsync(ignoreNullValues: true);

            TData obj = new TData() { Tag = 1 };
            return obj;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        [HttpPost]
        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)
        {
            TData obj = await _userBLL.DeleteForm(model.ids);
            return obj;
        }

        /// <summary>
        /// 修改用户状态
        /// </summary>
        [HttpPost]
        public async Task<TData> ChangeUserState([FromBody] UserEntity entity)
        {
            TData obj = await _userBLL.ChangeUserState(entity);
            return obj;
        }

        [HttpPost]
        public async Task<TData<long>> ResetPasswordJson([FromBody] UserEntity entity)
        {
            TData<long> obj = await _userBLL.ResetPassword(entity);
            return obj;
        }

        #endregion
    }
}
