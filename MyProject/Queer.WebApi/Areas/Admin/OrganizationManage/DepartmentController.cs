﻿using Microsoft.AspNetCore.Mvc;
using Queer.Entity;
using Queer.IBusiness.OrganizationManage;
using Queer.Model.Param.OrganizationManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.OrganizationManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-06 14:12
    /// 描 述：部门信息控制器类
    /// </summary>

    [Route("OrganizationManage/[controller]")]
    public class DepartmentController : BaseAdminController
    {
        private readonly IDepartmentBLL _departmentBLL;
        private readonly IUserBLL _userBLL;

        public DepartmentController(IDepartmentBLL departmentBLL, IUserBLL userBLL)
        {
            _departmentBLL = departmentBLL;
            _userBLL = userBLL;
        }

        #region 获取数据

        /// <summary>
        /// 条件查询
        /// </summary>
        [HttpGet]
        public async Task<TData<List<DepartmentEntity>>> GetListJson([FromQuery] DepartmentListParam param)
        {
            TData<List<DepartmentEntity>> obj = await _departmentBLL.GetList(param);
            return obj;
        }

        /// <summary>
        /// 条件查询-分页
        /// </summary>
        [HttpGet]
        public async Task<TData<List<DepartmentEntity>>> GetPageListJson([FromQuery] DepartmentListParam param, [FromQuery] Pagination pagination)
        {
            TData<List<DepartmentEntity>> obj = await _departmentBLL.GetPageList(param, pagination);
            return obj;
        }

        /// <summary>
        /// 根据ID查询
        /// </summary>
        [HttpGet]
        public async Task<TData<DepartmentEntity>> GetFormJson([FromQuery] long id)
        {
            TData<DepartmentEntity> obj = await _departmentBLL.GetEntity(id);
            return obj;
        }

        /// <summary>
        /// 用户下拉框用
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<TData<List<UserEntity>>> GetUserListJson()
        {
            TData<List<UserEntity>> obj = await _userBLL.GetList(null);
            return obj;
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 新增/修改 数据
        /// </summary>
        [HttpPost]
        public async Task<TData<string>> SaveFormJson([FromBody] DepartmentEntity entity)
        {
            TData<string> obj = await _departmentBLL.SaveForm(entity);
            return obj;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        [HttpPost]
        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)
        {
            TData obj = await _departmentBLL.DeleteForm(model.ids);
            return obj;
        }

        #endregion
    }
}
