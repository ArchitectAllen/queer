﻿using Microsoft.AspNetCore.Mvc;
using Queer.Entity;
using Queer.IBusiness.OrganizationManage;
using Queer.Model.Param.OrganizationManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.OrganizationManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-06 14:10
    /// 描 述：职位信息控制器类
    /// </summary>

    [Route("OrganizationManage/[controller]")]
    public class PositionController : BaseAdminController
    {
        private readonly IPositionBLL _positionBLL;

        public PositionController(IPositionBLL positionBLL)
        {
            _positionBLL = positionBLL;
        }

        #region 获取数据

        /// <summary>
        /// 条件查询
        /// </summary>
        [HttpGet]
        public async Task<TData<List<PositionEntity>>> GetListJson([FromQuery] PositionListParam param)
        {
            TData<List<PositionEntity>> obj = await _positionBLL.GetList(param);
            return obj;
        }

        /// <summary>
        /// 条件查询-分页
        /// </summary>
        [HttpGet]
        public async Task<TData<List<PositionEntity>>> GetPageListJson([FromQuery] PositionListParam param, [FromQuery] Pagination pagination)
        {
            TData<List<PositionEntity>> obj = await _positionBLL.GetPageList(param, pagination);
            return obj;
        }

        /// <summary>
        /// 根据ID查询
        /// </summary>
        [HttpGet]
        public async Task<TData<PositionEntity>> GetFormJson([FromQuery] long id)
        {
            TData<PositionEntity> obj = await _positionBLL.GetEntity(id);
            return obj;
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 新增/修改 数据
        /// </summary>
        [HttpPost]
        public async Task<TData<string>> SaveFormJson([FromBody] PositionEntity entity)
        {
            TData<string> obj = await _positionBLL.SaveForm(entity);
            return obj;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        [HttpPost]
        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)
        {
            TData obj = await _positionBLL.DeleteForm(model.ids);
            return obj;
        }

        #endregion
    }
}
