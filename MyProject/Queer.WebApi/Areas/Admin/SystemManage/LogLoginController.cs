﻿using Microsoft.AspNetCore.Mvc;
using Queer.Entity;
using Queer.IBusiness.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 12:55
    /// 描 述：登陆日志控制器类
    /// </summary>

    [Route("SystemManage/[controller]")]
    public class LogLoginController : BaseAdminController
    {
        private readonly ILogLoginBLL _logLoginBLL;

        public LogLoginController(ILogLoginBLL logLoginBLL)
        {
            _logLoginBLL = logLoginBLL;
        }

        #region 获取数据

        /// <summary>
        /// 条件查询
        /// </summary>
        [HttpGet]
        public async Task<TData<List<LogLoginEntity>>> GetListJson([FromQuery] LogLoginListParam param)
        {
            TData<List<LogLoginEntity>> obj = await _logLoginBLL.GetList(param);
            return obj;
        }

        /// <summary>
        /// 条件查询-分页
        /// </summary>
        [HttpGet]
        public async Task<TData<List<LogLoginEntity>>> GetPageListJson([FromQuery] LogLoginListParam param, [FromQuery] Pagination pagination)
        {
            TData<List<LogLoginEntity>> obj = await _logLoginBLL.GetPageList(param, pagination);
            return obj;
        }

        /// <summary>
        /// 根据ID查询
        /// </summary>
        [HttpGet]
        public async Task<TData<LogLoginEntity>> GetFormJson([FromQuery] long id)
        {
            TData<LogLoginEntity> obj = await _logLoginBLL.GetEntity(id);
            return obj;
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 新增/修改 数据
        /// </summary>
        [HttpPost]
        public async Task<TData<string>> SaveFormJson([FromBody] LogLoginEntity entity)
        {
            TData<string> obj = await _logLoginBLL.SaveForm(entity);
            return obj;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        [HttpPost]
        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)
        {
            TData obj = await _logLoginBLL.DeleteForm(model.ids);
            return obj;
        }

        #endregion
    }
}
