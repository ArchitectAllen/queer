﻿using Furion.DatabaseAccessor;
using Microsoft.AspNetCore.Mvc;
using Queer.Cache;
using Queer.Entity;
using Queer.IBusiness.SystemManage;
using Queer.Model;
using Queer.Model.Param.SystemManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-06 09:46
    /// 描 述：角色信息控制器类
    /// </summary>

    [Route("SystemManage/[controller]")]
    public class RoleController : BaseAdminController
    {
        private readonly IRoleBLL _roleBLL;
        private readonly IMenuBLL _menuBLL;

        public RoleController(IRoleBLL roleBLL, IMenuBLL menuBLL)
        {
            _roleBLL = roleBLL;
            _menuBLL = menuBLL;
        }

        #region 获取数据

        /// <summary>
        /// 条件查询
        /// </summary>
        [HttpGet]
        public async Task<TData<List<RoleEntity>>> GetListJson([FromQuery] RoleListParam param)
        {
            TData<List<RoleEntity>> obj = await _roleBLL.GetList(param);
            return obj;
        }

        /// <summary>
        /// 条件查询-分页
        /// </summary>
        [HttpGet]
        public async Task<TData<List<RoleEntity>>> GetPageListJson([FromQuery] RoleListParam param, [FromQuery] Pagination pagination)
        {
            TData<List<RoleEntity>> obj = await _roleBLL.GetPageList(param, pagination);
            return obj;
        }

        /// <summary>
        /// 根据ID查询
        /// </summary>
        [HttpGet]
        public async Task<TData<RoleEntity>> GetFormJson([FromQuery] long id)
        {
            TData<RoleEntity> obj = await _roleBLL.GetEntity(id);
            return obj;
        }

        /// <summary>
        /// 查询权限菜单树
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<TData<List<ZtreeInfo>>> QueryRoleAuthTree([FromQuery] long id)
        {
            TData<List<ZtreeInfo>> allMenu = await _menuBLL.GetZtreeList(null);
            TData<RoleEntity> roleInfo = await _roleBLL.GetEntity(id);

            if (string.IsNullOrEmpty(roleInfo.Data.MenuIds))
                return allMenu;

            foreach (ZtreeInfo ztree in allMenu.Data)
            {
                foreach (string _Id in roleInfo.Data.MenuIds.Split(','))
                {
                    if (long.Parse(_Id) == ztree.id)
                        ztree.@checked = true;
                }
            }

            return allMenu;
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 新增/修改 数据
        /// </summary>
        [HttpPost]
        public async Task<TData<string>> SaveFormJson([FromBody] RoleEntity entity)
        {
            TData<string> obj = await _roleBLL.SaveForm(entity);
            return obj;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        [HttpPost]
        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)
        {
            TData obj = await _roleBLL.DeleteForm(model.ids);
            return obj;
        }

        /// <summary>
        /// 保存权限
        /// </summary>
        [HttpPost]
        [UnitOfWork]
        public async Task<TData> SaveRoleAuth([FromRoute] long RoleId, [FromBody] ParamModel model)
        {
            TData data = await _roleBLL.SaveRoleAuth(RoleId, model.ids);
            return data;
        }

        #endregion
    }
}