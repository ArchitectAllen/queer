﻿using Microsoft.AspNetCore.Mvc;
using Queer.Entity;
using Queer.IBusiness.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-18 16:04
    /// 描 述：数据字典控制器类
    /// </summary>

    [Route("SystemManage/[controller]")]
    public class DataDictController : BaseAdminController
    {
        private readonly IDataDictBLL _dataDictBLL;

        public DataDictController(IDataDictBLL dataDictBLL)
        {
            _dataDictBLL = dataDictBLL;
        }

        #region 获取数据

        /// <summary>
        /// 条件查询
        /// </summary>
        [HttpGet]
        public async Task<TData<List<DataDictEntity>>> GetListJson([FromQuery] DataDictListParam param)
        {
            TData<List<DataDictEntity>> obj = await _dataDictBLL.GetList(param);
            return obj;
        }

        /// <summary>
        /// 条件查询-分页
        /// </summary>
        [HttpGet]
        public async Task<TData<List<DataDictEntity>>> GetPageListJson([FromQuery] DataDictListParam param, [FromQuery] Pagination pagination)
        {
            TData<List<DataDictEntity>> obj = await _dataDictBLL.GetPageList(param, pagination);
            return obj;
        }

        /// <summary>
        /// 根据ID查询
        /// </summary>
        [HttpGet]
        public async Task<TData<DataDictEntity>> GetFormJson([FromQuery] long id)
        {
            TData<DataDictEntity> obj = await _dataDictBLL.GetEntity(id);
            return obj;
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 新增/修改 数据
        /// </summary>
        [HttpPost]
        public async Task<TData<string>> SaveFormJson([FromBody] DataDictEntity entity)
        {
            TData<string> obj = await _dataDictBLL.SaveForm(entity);
            return obj;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        [HttpPost]
        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)
        {
            TData obj = await _dataDictBLL.DeleteForm(model.ids);
            return obj;
        }

        #endregion
    }
}