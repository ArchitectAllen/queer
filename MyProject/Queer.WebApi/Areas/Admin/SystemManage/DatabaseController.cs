﻿using Microsoft.AspNetCore.Mvc;
using Queer.IBusiness.SystemManage;
using Queer.Model.Result;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.SystemManage
{
    [Route("SystemManage/[controller]")]
    public class DatabaseController : BaseAdminController
    {
        private IDatabaseTableBLL _databaseTableBLL;

        public DatabaseController(IDatabaseTableBLL databaseTableBLL)
        {
            _databaseTableBLL = databaseTableBLL;
        }

        #region 获取数据

        [HttpGet]
        public async Task<TData<List<TableInfo>>> GetTableListJson([FromQuery] string tableName)
        {
            TData<List<TableInfo>> obj = await _databaseTableBLL.GetTableList(tableName);
            return obj;
        }

        [HttpGet]
        public async Task<TData<List<TableInfo>>> GetTablePageListJson([FromQuery] string tableName, [FromQuery] Pagination pagination)
        {
            TData<List<TableInfo>> obj = await _databaseTableBLL.GetTablePageList(tableName, pagination);
            return obj;
        }

        [HttpGet]
        public async Task<TData<List<TableFieldInfo>>> GetTableFieldListJson([FromQuery] string tableName)
        {
            TData<List<TableFieldInfo>> obj = await _databaseTableBLL.GetTableFieldList(tableName);
            return obj;
        }

        #endregion
    }
}