﻿using Furion.DatabaseAccessor;
using Microsoft.AspNetCore.Mvc;
using Queer.Entity;
using Queer.Entity.SystemManage;
using Queer.IBusiness.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 11:25
    /// 描 述：菜单控制器类
    /// </summary>

    [Route("SystemManage/[controller]")]
    public class MenuController : BaseAdminController
    {
        private readonly IMenuBLL _menuBLL;
        private readonly IApiAuthorizeBLL _apiAuthorizeBLL;

        public MenuController(IMenuBLL menuBLL, IApiAuthorizeBLL apiAuthorizeBLL)
        {
            _menuBLL = menuBLL;
            _apiAuthorizeBLL = apiAuthorizeBLL;
        }

        #region 获取数据

        /// <summary>
        /// 条件查询
        /// </summary>
        [HttpGet]
        public async Task<TData<List<MenuEntity>>> GetListJson([FromQuery] MenuListParam param)
        {
            TData<List<MenuEntity>> obj = await _menuBLL.GetList(param);
            return obj;
        }

        /// <summary>
        /// 条件查询-分页
        /// </summary>
        [HttpGet]
        public async Task<TData<List<MenuEntity>>> GetPageListJson([FromQuery] MenuListParam param, [FromQuery] Pagination pagination)
        {
            TData<List<MenuEntity>> obj = await _menuBLL.GetPageList(param, pagination);
            return obj;
        }

        /// <summary>
        /// 根据ID查询
        /// </summary>
        [HttpGet]
        public async Task<TData<MenuEntity>> GetFormJson([FromQuery] long id)
        {
            TData<MenuEntity> obj = await _menuBLL.GetEntity(id);
            return obj;
        }

        /// <summary>
        /// 根据权限标识查询菜单URL
        /// </summary>
        [HttpGet]
        public async Task<TData<List<ApiAuthorizeEntity>>> GetAccessByMenu([FromQuery] ApiAuthorizeListParam param)
        {
            var obj = await _apiAuthorizeBLL.GetList(param);
            return obj;
        }


        #endregion

        #region 提交数据

        /// <summary>
        /// 新增/修改 数据
        /// </summary>
        [HttpPost]
        public async Task<TData<string>> SaveFormJson([FromBody] MenuEntity entity)
        {
            TData<string> obj = await _menuBLL.SaveForm(entity);
            return obj;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        [HttpPost]
        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)
        {
            TData obj = await _menuBLL.DeleteForm(model.ids);
            return obj;
        }

        /// <summary>
        /// 保存权限标识对应的url
        /// </summary>
        [HttpPost]
        [UnitOfWork]
        public async Task<TData> SaveAccess([FromBody] ApiAuthorizeSaveParam param)
        {
            TData obj = await _apiAuthorizeBLL.SaveAccess(param.Authorize, param.Urls);
            return obj;
        }


        #endregion
    }
}
