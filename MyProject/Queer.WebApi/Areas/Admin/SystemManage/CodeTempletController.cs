﻿using Microsoft.AspNetCore.Mvc;
using Queer.Entity;
using Queer.IBusiness.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 12:28
    /// 描 述：代码模板控制器类
    /// </summary>

    [Route("SystemManage/[controller]")]
    public class CodeTempletController : BaseAdminController
    {
        private readonly ICodeTempletBLL _codeTempletBLL;

        public CodeTempletController(ICodeTempletBLL codeTempletBLL)
        {
            _codeTempletBLL = codeTempletBLL;
        }

        #region 获取数据

        /// <summary>
        /// 条件查询
        /// </summary>
        [HttpGet]
        public async Task<TData<List<CodeTempletEntity>>> GetListJson([FromQuery] CodeTempletListParam param)
        {
            TData<List<CodeTempletEntity>> obj = await _codeTempletBLL.GetList(param);
            return obj;
        }

        /// <summary>
        /// 条件查询-分页
        /// </summary>
        [HttpGet]
        public async Task<TData<List<CodeTempletEntity>>> GetPageListJson([FromQuery] CodeTempletListParam param, [FromQuery] Pagination pagination)
        {
            TData<List<CodeTempletEntity>> obj = await _codeTempletBLL.GetPageList(param, pagination);
            return obj;
        }

        /// <summary>
        /// 根据ID查询
        /// </summary>
        [HttpGet]
        public async Task<TData<CodeTempletEntity>> GetFormJson([FromQuery] long id)
        {
            TData<CodeTempletEntity> obj = await _codeTempletBLL.GetEntity(id);
            return obj;
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 新增/修改 数据
        /// </summary>
        [HttpPost]
        public async Task<TData<string>> SaveFormJson([FromBody] CodeTempletEntity entity)
        {
            TData<string> obj = await _codeTempletBLL.SaveForm(entity);
            return obj;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        [HttpPost]
        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)
        {
            TData obj = await _codeTempletBLL.DeleteForm(model.ids);
            return obj;
        }

        #endregion
    }
}
