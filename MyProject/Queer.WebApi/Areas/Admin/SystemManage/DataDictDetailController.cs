﻿using Microsoft.AspNetCore.Mvc;
using Queer.Entity;
using Queer.IBusiness.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Model.Result.SystemManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.WebApi.Areas.Admin.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-18 16:04
    /// 描 述：数据字典键值控制器类
    /// </summary>
    [Route("SystemManage/[controller]")]
    public class DataDictDetailController : BaseAdminController
    {
        private readonly IDataDictDetailBLL _dataDictDetailBLL;

        public DataDictDetailController(IDataDictDetailBLL dataDictDetailBLL)
        {
            _dataDictDetailBLL = dataDictDetailBLL;
        }

        #region 获取数据

        /// <summary>
        /// 条件查询
        /// </summary>
        [HttpGet]
        public async Task<TData<List<DataDictDetailEntity>>> GetListJson([FromQuery] DataDictDetailListParam param)
        {
            TData<List<DataDictDetailEntity>> obj = await _dataDictDetailBLL.GetList(param);
            return obj;
        }

        /// <summary>
        /// 条件查询-分页
        /// </summary>
        [HttpGet]
        public async Task<TData<List<DataDictDetailEntity>>> GetPageListJson([FromQuery] DataDictDetailListParam param, [FromQuery] Pagination pagination)
        {
            TData<List<DataDictDetailEntity>> obj = await _dataDictDetailBLL.GetPageList(param, pagination);
            return obj;
        }

        /// <summary>
        /// 根据ID查询
        /// </summary>
        [HttpGet]
        public async Task<TData<DataDictDetailEntity>> GetFormJson([FromQuery] long id)
        {
            TData<DataDictDetailEntity> obj = await _dataDictDetailBLL.GetEntity(id);
            return obj;
        }

        /// <summary>
        /// 根据条件查询获取select控件所需内容,不管多个还是单个字典类型，返回都是List。如果是单个类型，获取List[0]数据
        /// </summary>
        [HttpGet]
        public TData<List<DataDictDetailSelect>> GetListJsonForSelect([FromQuery] DataDictDetailListParam param)
        {
            TData<List<DataDictDetailSelect>> obj = _dataDictDetailBLL.GetListJsonForSelect(param);
            return obj;
        }
        #endregion

        #region 提交数据

        /// <summary>
        /// 新增/修改 数据
        /// </summary>
        [HttpPost]
        public async Task<TData<string>> SaveFormJson([FromBody] DataDictDetailEntity entity)
        {
            TData<string> obj = await _dataDictDetailBLL.SaveForm(entity);
            return obj;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        [HttpPost]
        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)
        {
            TData obj = await _dataDictDetailBLL.DeleteForm(model.ids);
            return obj;
        }

        #endregion
    }
}
