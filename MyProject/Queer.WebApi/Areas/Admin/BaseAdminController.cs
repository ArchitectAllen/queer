﻿using Furion.DynamicApiController;
using Microsoft.AspNetCore.Mvc;

namespace Queer.WebApi.Areas.Admin
{
    [ApiDescriptionSettings("Admin")]
    [ServiceFilter(typeof(AuthorizeFilterAttribute))]
    public class BaseAdminController : IDynamicApiController
    {
    }
}