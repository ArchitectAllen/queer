﻿using Microsoft.AspNetCore.Mvc;
using Queer.Cache;
using Queer.Entity;
using Queer.Entity.SystemManage;
using Queer.Enum;
using Queer.Enum.SystemManage;
using Queer.IBusiness.OrganizationManage;
using Queer.IBusiness.SystemManage;
using Queer.Model.Result.SystemManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using Queer.WebApi.Areas.Admin;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Queer.Web.Area.Admin.HomeManage
{
    /// <summary>
    /// 主页
    /// </summary>
    [Route("HomeManage/[controller]")]
    public class HomeController : BaseAdminController
    {
        private readonly IMenuBLL _MenuBLL;
        private readonly IMenuAuthorizeBLL _sysMenuAuthorizeBLL;
        private readonly OperatorCache _operatorCache;

        public HomeController(IMenuAuthorizeBLL sysMenuAuthorizeBLL, IMenuBLL MenuBLL, OperatorCache operatorCache)
        {
            _MenuBLL = MenuBLL;
            _sysMenuAuthorizeBLL = sysMenuAuthorizeBLL;
            _operatorCache = operatorCache;
        }

        /// <summary>
        /// 获取导航菜单信息和用户信息
        /// </summary>
        [HttpGet]
        public async Task<TData<object>> GetPageListAndUserInfo()
        {
            OperatorInfo operatorInfo = await _operatorCache.Current();

            TData<List<MenuEntity>> objMenu = await _MenuBLL.GetList(null);
            List<MenuEntity> menuList = objMenu.Data;
            menuList = menuList.Where(p => p.MenuStatus == StatusEnum.Yes.ParseToInt()).ToList();

            if (operatorInfo.IsSystem != 1)
            {
                var objMenuAuthorize = await _sysMenuAuthorizeBLL.GetAuthorizeList(operatorInfo);
                List<long?> authorizeMenuIdList = objMenuAuthorize.Data.Select(p => p.MenuId).ToList();
                menuList = menuList.Where(p => authorizeMenuIdList.Contains(p.Id)).ToList();
            }

            #region  导航栏数据处理

            List<MenuResult> menuResult = new List<MenuResult>();
            foreach (var menu in menuList.Where(p => p.ParentId == 0).OrderBy(p => p.MenuSort))
            {
                MenuResult menu_a = new MenuResult();
                menu_a.url = HttpHelper.IsUrl(menu.MenuUrl) ? menu.MenuUrl : "javascript:;";
                menu_a.icon = menu.MenuIcon;
                menu_a.name = menu.MenuName;
                menu_a.subMenus = new List<MenuResult>();

                foreach (var secondMenu in menuList.Where(p => p.ParentId == menu.Id).OrderBy(p => p.MenuSort))
                {
                    MenuResult menu_b = new MenuResult();
                    menu_b.url = HttpHelper.IsUrl(secondMenu.MenuUrl) ? secondMenu.MenuUrl : "javascript:;";
                    menu_b.name = secondMenu.MenuName;
                    menu_b.url = secondMenu.MenuUrl;

                    if (menuList.Where(p => p.ParentId == secondMenu.Id && p.MenuType != (int)MenuTypeEnum.Button).Count() != 0)
                    {
                        menu_b.subMenus = new List<MenuResult>();
                        foreach (var thirdMenu in menuList.Where(p => p.ParentId == secondMenu.Id).OrderBy(p => p.MenuSort))
                        {
                            MenuResult menu_c = new MenuResult();
                            menu_c.url = HttpHelper.IsUrl(thirdMenu.MenuUrl) ? thirdMenu.MenuUrl : "javascript:;";
                            menu_c.name = thirdMenu.MenuName;
                            menu_c.url = thirdMenu.MenuUrl;

                            menu_b.subMenus.Add(menu_c);
                        }
                    }

                    menu_a.subMenus.Add(menu_b);
                }
                menuResult.Add(menu_a);
            }

            #endregion

            TData<object> data = new TData<object>();
            data.Tag = 1;
            data.Data = new { operatorInfo, menuResult };
            return data;
        }

        /// <summary>
        /// 获取当前用户的真实名称
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<TData<string>> GetUserRealName()
        {
            var user = await _operatorCache.Current();
            TData<string> obj = new TData<string>()
            {
                Data = user.RealName,
                Tag = 1
            };
            return obj;
        }

        /// <summary>
        /// 获取用户权限信息
        /// </summary>
        [HttpGet]
        public async Task<TData<UserAuthorizeInfo>> GetUserAuthorizeJson()
        {
            TData<UserAuthorizeInfo> obj = new TData<UserAuthorizeInfo>();
            OperatorInfo operatorInfo = await _operatorCache.Current();

            TData<List<MenuAuthorizeInfo>> objMenuAuthorizeInfo = await _sysMenuAuthorizeBLL.GetAuthorizeList(operatorInfo);
            obj.Data = new UserAuthorizeInfo();
            obj.Data.IsSystem = operatorInfo.IsSystem;
            if (objMenuAuthorizeInfo.Tag == 1)
            {
                obj.Data.MenuAuthorize = objMenuAuthorizeInfo.Data;
            }
            obj.Tag = 1;
            return obj;
        }


        /// <summary>
        /// 获取用户权限信息
        /// </summary>
        [HttpGet]
        public TData ClearThisUserCache()
        {
            _operatorCache.ClearThisUserCache();
            TData obj = new TData();
            obj.Tag = 1;
            return obj;
        }
    }
}