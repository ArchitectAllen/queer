﻿using Furion;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;
using Queer.Util;
using Queer.WebApi.Handlers;

namespace Queer.WebApi
{
    [AppStartup(2)]
    public class ApiStartup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCorsAccessor();

            // JWT 验证
            services.AddJwt<JwtHandler>(enableGlobalAuthorize: true);

            services.AddControllers()
                    .AddNewtonsoftJson(options =>
                    {
                        // 返回数据首字母不小写，CamelCasePropertyNamesContractResolver是小写
                        options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                    })
                    .AddInjectWithUnifyResult()
                    .AddFriendlyException() // 注册友好异常服务
                    .AddUnifyResult<ExResultProvider>();

            // 注册拦截器
            services.AddScoped<Areas.Admin.AuthorizeFilterAttribute>();

            services.Configure<IISServerOptions>(options => options.AllowSynchronousIO = true);

            //使用本地缓存必须添加
            services.AddMemoryCache();

            // 注册远程请求功能
            services.AddRemoteRequest();

            // 注册总线功能
            services.AddEventBridge();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCorsAccessor();

            app.UseAuthentication();
            app.UseAuthorization();//需要在注册微信 SDK 之后执行

            app.UseInject();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // 记录启动的日志
            GlobalContext.LogWhenStart(env);
        }


    }
}