﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.LinqBuilder;
using Microsoft.EntityFrameworkCore;
using Queer.Entity;
using Queer.Enum.OrganizationManage;
using Queer.IService.OrganizationManage;
using Queer.Model.Param.OrganizationManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Queer.Service.OrganizationManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 13:08
    /// 描 述：用户关联信息服务类
    /// </summary>

    public class UserBelongService : IUserBelongService, ITransient
    {
        private readonly IRepository<UserBelongEntity> _userBelongDB;

        public UserBelongService(IRepository<UserBelongEntity> userBelongDB)
        {
            _userBelongDB = userBelongDB;
        }

        #region 获取数据

        /// <summary>
        /// 带条件查询所有
        /// </summary>
        public async Task<List<UserBelongEntity>> GetList(UserBelongListParam param)
        {
            #region 查询条件

            IQueryable<UserBelongEntity> query = _userBelongDB.AsQueryable(false);

            if (param.UserId.HasValue)
                query = query.Where(a => a.UserId == param.UserId);

            #endregion

            var data = await query.ToListAsync();
            return data;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        public async Task<List<UserBelongEntity>> GetPageList(UserBelongListParam param, Pagination pagination)
        {
            #region 查询条件

            IQueryable<UserBelongEntity> query = _userBelongDB.AsQueryable(false);
            /*

            */
            var data = await query.OrderByDescending(a => a.Id).ToPagedListAsync(pagination.PageIndex, pagination.PageSize);

            #endregion

            // 分页参数赋值
            pagination.TotalCount = data.TotalCount;
            return data.Items.ToList();
        }

        /// <summary>
        /// 根据ID获取对象
        /// </summary>
        public async Task<UserBelongEntity> GetEntity(long id)
        {
            var list = await _userBelongDB.AsQueryable(p => p.Id == id).ToListAsync();
            return list.FirstOrDefault();
        }

        /// <summary>
        /// 查询多个ID主键数据
        /// </summary>
        public async Task<List<UserBelongEntity>> GetListByIds(string ids)
        {
            if (ids.IsNullOrEmpty())
                throw new BusinessException("参数不合法！");

            var idArr = TextHelper.SplitToArray<long>(ids, ',').ToList();
            var data = await _userBelongDB.AsQueryable(a => idArr.Contains(a.Id.GetValueOrDefault())).ToListAsync();

            return data;
        }

        #endregion

        #region 提交数据

        public async Task SaveForm(UserBelongEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await _userBelongDB.InsertNowAsync(entity);
            }
            else
            {
                await _userBelongDB.UpdateNowAsync(entity, ignoreNullValues: true);
            }
        }

        public async Task DeleteForm(string ids)
        {
            if (string.IsNullOrWhiteSpace(ids))
                throw new BusinessException("参数不合法！");

            var _ids = ids.Split(",");
            await _userBelongDB.BatchDeleteAsync(_ids);
        }

        public async Task SaveUserRoles(long userId, List<long> roleIds)
        {
            foreach (var roleId in roleIds.Distinct())
            {
                var entity = new UserBelongEntity();
                entity.UserId = userId;
                entity.BelongId = roleId;
                entity.BelongType = UserBelongTypeEnum.Role.ParseToInt();

                await SaveForm(entity);
            }
        }

        public async Task SaveUserPositions(long userId, List<long> pIds)
        {
            foreach (var pId in pIds.Distinct())
            {
                var entity = new UserBelongEntity();
                entity.UserId = userId;
                entity.BelongId = pId;
                entity.BelongType = UserBelongTypeEnum.Position.ParseToInt();

                await SaveForm(entity);
            }
        }

        public async Task DeleteByUserId(long userId)
        {
            var ids = await _userBelongDB.Where(a => a.UserId == userId).Select(a => a.Id.GetValueOrDefault()).ToListAsync();
            await _userBelongDB.BatchDeleteAsync(ids);
        }

        #endregion

        #region 私有方法

        #endregion
    }
}
