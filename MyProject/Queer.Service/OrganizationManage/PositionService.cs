﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.LinqBuilder;
using Microsoft.EntityFrameworkCore;
using Queer.Entity;
using Queer.IService.OrganizationManage;
using Queer.Model.Param.OrganizationManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Queer.Service.OrganizationManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-06 14:10
    /// 描 述：职位信息服务类
    /// </summary>

    public class PositionService : IPositionService, ITransient
    {
        private readonly IRepository<PositionEntity> _positionDB;

        public PositionService(IRepository<PositionEntity> positionDB)
        {
            _positionDB = positionDB;
        }

        #region 获取数据

        /// <summary>
        /// 带条件查询所有
        /// </summary>
        public async Task<List<PositionEntity>> GetList(PositionListParam param)
        {
            var data = await ListFilter(param).ToListAsync();
            return data;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        public async Task<List<PositionEntity>> GetPageList(PositionListParam param, Pagination pagination)
        {
            #region 查询条件

            var data = await ListFilter(param).OrderByDescending(a => a.Id).ToPagedListAsync(pagination.PageIndex, pagination.PageSize);

            #endregion

            // 分页参数赋值
            pagination.TotalCount = data.TotalCount;
            return data.Items.ToList();
        }

        /// <summary>
        /// 根据ID获取对象
        /// </summary>
        public async Task<PositionEntity> GetEntity(long id)
        {
            var data = await _positionDB.AsQueryable(p => p.Id == id).FirstOrDefaultAsync();
            return data;
        }

        /// <summary>
        /// 查询多个ID主键数据
        /// </summary>
        public async Task<List<PositionEntity>> GetListByIds(string ids)
        {
            if (ids.IsNullOrEmpty())
                throw new BusinessException("参数不合法！");

            var idArr = TextHelper.SplitToArray<long>(ids, ',').ToList();
            var data = await _positionDB.AsQueryable(a => idArr.Contains(a.Id.GetValueOrDefault())).ToListAsync();

            return data;
        }

        #endregion

        #region 提交数据

        public async Task SaveForm(PositionEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await _positionDB.InsertNowAsync(entity);
            }
            else
            {
                await _positionDB.UpdateNowAsync(entity, ignoreNullValues: true);
            }
        }

        public async Task DeleteForm(string ids)
        {

            //string sql = "Delete From SysPosition Where Id in (" + ids + ")";
            //await _positionDB.SqlNonQueryAsync(sql);

            if (string.IsNullOrWhiteSpace(ids))
                throw new BusinessException("参数不合法！");

            var _ids = ids.Split(",");
            await _positionDB.BatchFakeDeleteAsync(_ids);
        }

        #endregion

        #region 私有方法

        private IQueryable<PositionEntity> ListFilter(PositionListParam param)
        {
            IQueryable<PositionEntity> query = _positionDB.AsQueryable(false);

            if (param != null)
            {
                // 职位名称
                if (!string.IsNullOrEmpty(param.PositionName))
                    query = query.Where(p => p.PositionName.Contains(param.PositionName));
            }

            return query;
        }
        #endregion
    }
}
