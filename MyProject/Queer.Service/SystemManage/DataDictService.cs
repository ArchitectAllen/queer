﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.LinqBuilder;
using Microsoft.EntityFrameworkCore;
using Queer.Entity;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Queer.Service.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-18 16:04
    /// 描 述：数据字典服务类
    /// </summary>

    public class DataDictService : IDataDictService, ITransient
    {
        private readonly IRepository<DataDictEntity> _dataDictDB;

        public DataDictService(IRepository<DataDictEntity> dataDictDB)
        {
            _dataDictDB = dataDictDB;
        }

        #region 获取数据

        /// <summary>
        /// 带条件查询所有
        /// </summary>
        public async Task<List<DataDictEntity>> GetList(DataDictListParam param)
        {
            IQueryable<DataDictEntity> query = ListFilter(param);

            var data = await query.ToListAsync();
            return data;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        public async Task<List<DataDictEntity>> GetPageList(DataDictListParam param, Pagination pagination)
        {
            IQueryable<DataDictEntity> query = ListFilter(param);

            var data = await query.OrderByDescending(a => a.Id).ToPagedListAsync(pagination.PageIndex, pagination.PageSize);

            // 分页参数赋值
            pagination.TotalCount = data.TotalCount;
            return data.Items.ToList();
        }

        /// <summary>
        /// 根据ID获取对象
        /// </summary>
        public async Task<DataDictEntity> GetEntity(long id)
        {
            var list = await _dataDictDB.AsQueryable(p => p.Id == id).ToListAsync();
            return list.FirstOrDefault();
        }

        /// <summary>
        /// 查询多个ID主键数据
        /// </summary>
        public async Task<List<DataDictEntity>> GetListByIds(string ids)
        {
            if (ids.IsNullOrEmpty())
                throw new BusinessException("参数不合法！");

            var idArr = TextHelper.SplitToArray<long>(ids, ',').ToList();
            var data = await _dataDictDB.AsQueryable(a => idArr.Contains(a.Id.GetValueOrDefault())).ToListAsync();

            return data;
        }

        #endregion

        #region 提交数据

        public async Task SaveForm(DataDictEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await _dataDictDB.InsertNowAsync(entity);
            }
            else
            {
                await _dataDictDB.UpdateNowAsync(entity, ignoreNullValues: true);
            }
        }

        public async Task DeleteForm(string ids)
        {
            if (string.IsNullOrWhiteSpace(ids))
                throw new BusinessException("参数不合法！");

            var _ids = ids.Split(",");

            await _dataDictDB.BatchFakeDeleteAsync(_ids);
        }

        #endregion

        #region 私有方法

        private IQueryable<DataDictEntity> ListFilter(DataDictListParam param)
        {
            IQueryable<DataDictEntity> query = _dataDictDB.AsQueryable(false);
            if (param != null)
            {
                if (!string.IsNullOrEmpty(param.DictType))
                    query = query.Where(p => p.DictType.Contains(param.DictType));

                if (!string.IsNullOrEmpty(param.Remark))
                    query = query.Where(p => p.Remark.Contains(param.Remark));
            }

            query = query.Where(a => a.IsDelete != true);

            return query;
        }


        #endregion
    }
}