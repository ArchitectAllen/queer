﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.LinqBuilder;
using Microsoft.EntityFrameworkCore;
using Queer.Entity;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Queer.Service.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 12:28
    /// 描 述：代码模板服务类
    /// </summary>

    public class CodeTempletService : ICodeTempletService, ITransient
    {
        private readonly IRepository<CodeTempletEntity> _codeTempletDB;

        public CodeTempletService(IRepository<CodeTempletEntity> codeTempletDB)
        {
            _codeTempletDB = codeTempletDB;
        }

        #region 获取数据

        /// <summary>
        /// 带条件查询所有
        /// </summary>
        public async Task<List<CodeTempletEntity>> GetList(CodeTempletListParam param)
        {
            #region 查询条件

            IQueryable<CodeTempletEntity> query = _codeTempletDB.AsQueryable(false);
            /*

            */
            #endregion

            var data = await query.ToListAsync();
            return data;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        public async Task<List<CodeTempletEntity>> GetPageList(CodeTempletListParam param, Pagination pagination)
        {
            #region 查询条件

            IQueryable<CodeTempletEntity> query = _codeTempletDB.AsQueryable(false);
            /*

            */
            var data = await query.OrderByDescending(a => a.Id).ToPagedListAsync(pagination.PageIndex, pagination.PageSize);

            #endregion

            // 分页参数赋值
            pagination.TotalCount = data.TotalCount;
            return data.Items.ToList();
        }

        /// <summary>
        /// 根据ID获取对象
        /// </summary>
        public async Task<CodeTempletEntity> GetEntity(long id)
        {
            var list = await _codeTempletDB.AsQueryable(p => p.Id == id).ToListAsync();
            return list.FirstOrDefault();
        }


        /// <summary>
        /// 查询多个ID主键数据
        /// </summary>
        public async Task<List<CodeTempletEntity>> GetListByIds(string ids)
        {
            if (ids.IsNullOrEmpty())
                throw new BusinessException("参数不合法！");

            var idArr = TextHelper.SplitToArray<long>(ids, ',').ToList();
            var data = await _codeTempletDB.AsQueryable(a => idArr.Contains(a.Id.GetValueOrDefault())).ToListAsync();

            return data;
        }

        #endregion

        #region 提交数据

        public async Task SaveForm(CodeTempletEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await _codeTempletDB.InsertNowAsync(entity);
            }
            else
            {
                // 默认赋值
                await _codeTempletDB.UpdateNowAsync(entity, ignoreNullValues: true);
            }
        }

        public async Task DeleteForm(string ids)
        {
            if (string.IsNullOrWhiteSpace(ids))
                throw new BusinessException("参数不合法！");

            var _ids = ids.Split(",");
            await _codeTempletDB.BatchDeleteAsync(_ids);
        }

        #endregion

        #region 私有方法

        #endregion
    }
}
