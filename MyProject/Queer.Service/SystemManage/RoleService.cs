﻿using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Furion.LinqBuilder;
using Microsoft.EntityFrameworkCore;
using Queer.Entity;
using Queer.Enum.SystemManage;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Queer.Service.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-06 09:46
    /// 描 述：角色信息服务类
    /// </summary>

    public class RoleService : IRoleService, ITransient
    {
        private readonly IRepository<RoleEntity> _roleDB;
        private readonly IRepository<MenuAuthorizeEntity> _menuAuthorizeDB;

        public RoleService(IRepository<RoleEntity> roleDB, IRepository<MenuAuthorizeEntity> menuAuthorizeDB)
        {
            _roleDB = roleDB;
            _menuAuthorizeDB = menuAuthorizeDB;
        }

        #region 获取数据

        /// <summary>
        /// 带条件查询所有
        /// </summary>
        public async Task<List<RoleEntity>> GetList(RoleListParam param)
        {
            #region 查询条件

            IQueryable<RoleEntity> query = _roleDB.AsQueryable(false);
            /*

            */
            #endregion

            var data = await query.ToListAsync();
            return data;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        public async Task<List<RoleEntity>> GetPageList(RoleListParam param, Pagination pagination)
        {
            #region 查询条件

            IQueryable<RoleEntity> query = _roleDB.AsQueryable(false);
            /*

            */
            var data = await query.OrderByDescending(a => a.Id).ToPagedListAsync(pagination.PageIndex, pagination.PageSize);

            #endregion

            // 分页参数赋值
            pagination.TotalCount = data.TotalCount;
            return data.Items.ToList();
        }

        /// <summary>
        /// 根据ID获取对象
        /// </summary>
        public async Task<RoleEntity> GetEntity(long id)
        {
            var list = await _roleDB.AsQueryable(p => p.Id == id).ToListAsync();
            return list.FirstOrDefault();
        }

        /// <summary>
        /// 查询多个ID主键数据
        /// </summary>
        public async Task<List<RoleEntity>> GetListByIds(string ids)
        {
            if (ids.IsNullOrEmpty())
                throw new BusinessException("参数不合法！");

            var idArr = TextHelper.SplitToArray<long>(ids, ',').ToList();
            var data = await _roleDB.AsQueryable(a => idArr.Contains(a.Id.GetValueOrDefault())).ToListAsync();

            return data;
        }

        #endregion

        #region 提交数据

        public async Task SaveForm(RoleEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                if (_roleDB.Any(a => a.RoleName == entity.RoleName && a.IsDelete != true))
                    throw new BusinessException("该角色已存在！");

                await _roleDB.InsertNowAsync(entity);
            }
            else
                await _roleDB.UpdateNowAsync(entity);
        }

        public async Task DeleteForm(string ids)
        {
            if (string.IsNullOrWhiteSpace(ids))
                throw new BusinessException("参数不合法！");

            var _ids = ids.Split(",");
            await _roleDB.BatchDeleteAsync(_ids);
        }

        public async Task SaveRoleAuth(long roleId, string menuIds)
        {
            var items = await _menuAuthorizeDB.Where(a => a.AuthorizeId == roleId).ToListAsync();
            foreach (var item in items)
                await item.DeleteNowAsync();

            // 角色对应的菜单、页面和按钮权限
            if (!string.IsNullOrEmpty(menuIds))
            {
                foreach (long menuId in TextHelper.SplitToArray<long>(menuIds, ','))
                {
                    MenuAuthorizeEntity menuAuthorizeEntity = new MenuAuthorizeEntity();
                    menuAuthorizeEntity.AuthorizeId = roleId;
                    menuAuthorizeEntity.MenuId = menuId;
                    menuAuthorizeEntity.AuthorizeType = AuthorizeTypeEnum.Role.ParseToInt();
                    await _menuAuthorizeDB.InsertNowAsync(menuAuthorizeEntity);
                }
            }
        }

        #endregion

        #region 私有方法


        #endregion
    }
}
