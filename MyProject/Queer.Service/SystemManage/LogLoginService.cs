﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.LinqBuilder;
using Microsoft.EntityFrameworkCore;
using Queer.Entity;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Queer.Service.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 12:55
    /// 描 述：登陆日志服务类
    /// </summary>

    public class LogLoginService : ILogLoginService, ITransient
    {
        private readonly IRepository<LogLoginEntity> _logLoginDB;
        private readonly IRepository<UserEntity> _userDB;

        public LogLoginService(IRepository<LogLoginEntity> logLoginDB, IRepository<UserEntity> userDB)
        {
            _logLoginDB = logLoginDB;
            _userDB = userDB;
        }

        #region 获取数据

        /// <summary>
        /// 带条件查询所有
        /// </summary>
        public async Task<List<LogLoginEntity>> GetList(LogLoginListParam param)
        {
            IQueryable<LogLoginEntity> query = ListFilter(param);
            var data = await query.ToListAsync();
            return data;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        public async Task<List<LogLoginEntity>> GetPageList(LogLoginListParam param, Pagination pagination)
        {
            IQueryable<LogLoginEntity> query = ListFilter(param);
            var data = await query.OrderByDescending(a => a.Id).ToPagedListAsync(pagination.PageIndex, pagination.PageSize);

            // 分页参数赋值
            pagination.TotalCount = data.TotalCount;
            return data.Items.ToList();
        }

        /// <summary>
        /// 根据ID获取对象
        /// </summary>
        public async Task<LogLoginEntity> GetEntity(long id)
        {
            var list = await _logLoginDB.AsQueryable(p => p.Id == id).ToListAsync();
            return list.FirstOrDefault();
        }

        /// <summary>
        /// 查询多个ID主键数据
        /// </summary>
        public async Task<List<LogLoginEntity>> GetListByIds(string ids)
        {
            if (ids.IsNullOrEmpty())
                throw new BusinessException("参数不合法！");

            var idArr = TextHelper.SplitToArray<long>(ids, ',').ToList();
            var data = await _logLoginDB.AsQueryable(a => idArr.Contains(a.Id.GetValueOrDefault())).ToListAsync();

            return data;
        }

        #endregion

        #region 提交数据

        public async Task SaveForm(LogLoginEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await _logLoginDB.InsertNowAsync(entity);
            }
            else
            {
                await _logLoginDB.UpdateNowAsync(entity, ignoreNullValues: true);
            }
        }

        public async Task DeleteForm(string ids)
        {
            if (string.IsNullOrWhiteSpace(ids))
                throw new BusinessException("参数不合法！");

            var _ids = ids.Split(",");
            await _logLoginDB.BatchDeleteAsync(_ids);
        }

        #endregion

        #region 私有方法

        private IQueryable<LogLoginEntity> ListFilter(LogLoginListParam param)
        {
            IQueryable<LogLoginEntity> query = from a in _logLoginDB.AsQueryable(false)
                                               join b in _userDB.AsQueryable(false) on a.CreateUserId equals b.Id
                                               into ab
                                               from res in ab.DefaultIfEmpty()
                                               select new LogLoginEntity()
                                               {
                                                   Id = a.Id,
                                                   Browser = a.Browser,
                                                   ExtraRemark = a.ExtraRemark,
                                                   IpAddress = a.IpAddress,
                                                   IpLocation = a.IpLocation,
                                                   LogStatus = a.LogStatus,
                                                   OS = a.OS,
                                                   Remark = a.Remark,
                                                   UserName = res.UserName,
                                                   CreateTime = a.CreateTime
                                               };

            if (!string.IsNullOrEmpty(param.UserName))
                query = query.Where(p => p.UserName.Contains(param.UserName));

            if (param.LogStatus > -1)
                query = query.Where(p => p.LogStatus == param.LogStatus);

            if (!string.IsNullOrEmpty(param.IpAddress))
                query = query.Where(p => p.IpAddress.Contains(param.IpAddress));

            if (param.StartTime.HasValue)
                query = query.Where(p => p.CreateTime >= param.StartTime);

            if (param.EndTime.HasValue)
                query = query.Where(p => p.CreateTime <= param.EndTime.Value.AddDays(1));

            return query;
        }

        #endregion
    }
}
