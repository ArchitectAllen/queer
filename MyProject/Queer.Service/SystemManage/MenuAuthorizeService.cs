﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.LinqBuilder;
using Microsoft.EntityFrameworkCore;
using Queer.Entity;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Queer.Service.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 11:16
    /// 描 述：菜单权限服务类
    /// </summary>

    public class MenuAuthorizeService : IMenuAuthorizeService, ITransient
    {
        private readonly IRepository<UserBelongEntity> _userBelongDB;
        private readonly IRepository<MenuAuthorizeEntity> _menuAuthorizeDB;

        public MenuAuthorizeService(IRepository<MenuAuthorizeEntity> menuAuthorizeDB, IRepository<UserBelongEntity> userBelongDB)
        {
            _menuAuthorizeDB = menuAuthorizeDB;
            _userBelongDB = userBelongDB;
        }

        #region 获取数据

        /// <summary>
        /// 带条件查询所有
        /// </summary>
        public async Task<List<MenuAuthorizeEntity>> GetList(MenuAuthorizeListParam param)
        {
            #region 查询条件

            IQueryable<MenuAuthorizeEntity> query = _menuAuthorizeDB.AsQueryable(false);
            /*

            */
            #endregion

            var data = await query.ToListAsync();
            return data;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        public async Task<List<MenuAuthorizeEntity>> GetPageList(MenuAuthorizeListParam param, Pagination pagination)
        {
            #region 查询条件

            IQueryable<MenuAuthorizeEntity> query = _menuAuthorizeDB.AsQueryable(false);
            /*

            */
            var data = await query.OrderByDescending(a => a.Id).ToPagedListAsync(pagination.PageIndex, pagination.PageSize);

            #endregion

            // 分页参数赋值
            pagination.TotalCount = data.TotalCount;
            return data.Items.ToList();
        }

        /// <summary>
        /// 根据ID获取对象
        /// </summary>
        public async Task<MenuAuthorizeEntity> GetEntity(long id)
        {
            var list = await _menuAuthorizeDB.AsQueryable(p => p.Id == id).ToListAsync();
            return list.FirstOrDefault();
        }

        /// <summary>
        /// 通过权限条件 获取菜单id
        /// </summary>
        /// <param name="menuListParam"></param>
        /// <returns></returns>
        public async Task<List<long>> GetMenuIdList(MenuListParam param)
        {
            IQueryable<MenuAuthorizeEntity> query = _menuAuthorizeDB.AsQueryable(false);
            if (param != null)
            {
                if (param.AuthorizeId.HasValue)
                    query = query.Where(t => t.AuthorizeId == param.AuthorizeId);

                if (param.AuthorizeType.HasValue)
                    query = query.Where(t => t.AuthorizeType == param.AuthorizeType);

                if (!param.AuthorizeIds.IsEmpty())
                {
                    long[] authorizeIdArr = TextHelper.SplitToArray<long>(param.AuthorizeIds, ',');
                    query = query.Where(t => authorizeIdArr.Contains(t.AuthorizeId.Value));
                }
            }
            var menuIds = await query.Select(a => a.MenuId.GetValueOrDefault()).ToListAsync();
            return menuIds;
        }

        /// <summary>
        /// 根据用户ID查询权限
        /// </summary>
        public async Task<List<MenuAuthorizeEntity>> GetUserAuth(long userId)
        {
            var roleIdsQuery = from a in _userBelongDB.AsQueryable(false) where a.UserId == userId && a.BelongType == 2 && a.IsDelete != true select a.BelongId ;
            var list_a = await (from a in _menuAuthorizeDB.AsQueryable(false) where roleIdsQuery.Contains(a.AuthorizeId) select a).ToListAsync();

            // 按用户查询
            var list_b = await _menuAuthorizeDB.Where(a => a.AuthorizeId.Value == userId).ToListAsync();

            return list_a.Union(list_b).Distinct().ToList();
        }

        #endregion

        #region 提交数据

        public async Task SaveForm(MenuAuthorizeEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {

                await _menuAuthorizeDB.InsertNowAsync(entity);
            }
            else
            {
                // 默认赋值

                await _menuAuthorizeDB.UpdateNowAsync(entity, ignoreNullValues: true);
            }
        }

        public async Task DeleteForm(string ids)
        {
            if (string.IsNullOrWhiteSpace(ids))
                throw new BusinessException("参数不合法！");

            string sql = "Delete From SysMenuAuthorize Where Id in (" + ids + ")";
            await _menuAuthorizeDB.SqlNonQueryAsync(sql);
        }

        #endregion

        #region 私有方法



        #endregion
    }
}
