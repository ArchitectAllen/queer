﻿using Furion;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Queer.Cache;
using Queer.Entity.SystemManage;
using System;

namespace Queer.EntityListener.SystemManage
{
    public class MenuListener : IEntityChangedListener<MenuEntity>
    {
        public void OnChanged(MenuEntity newEntity, MenuEntity oldEntity, DbContext dbContext, Type dbContextLocator, EntityState state)
        {
            App.GetService<MenuCache>().Remove();
        }
    }
}