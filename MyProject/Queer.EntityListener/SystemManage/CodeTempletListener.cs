﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Queer.Entity;
using System;

namespace Queer.EntityListener.SystemManage
{
    public class CodeTempletListener : IEntityChangedListener<CodeTempletEntity>
    {
        public void OnChanged(CodeTempletEntity newEntity, CodeTempletEntity oldEntity, DbContext dbContext, Type dbContextLocator, EntityState state)
        {
#if Relese
            throw new BusinessException("运行环境不允许操作！");
#endif
        }
    }
}