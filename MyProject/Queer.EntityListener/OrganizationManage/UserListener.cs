﻿using Furion;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Queer.Cache;
using Queer.Entity;
using Queer.Util;
using System;

namespace Queer.EntityListener.OrganizationManage
{
    public class UserListener : IEntityChangedListener<UserEntity>
    {
        public void OnChanged(UserEntity newEntity, UserEntity oldEntity, DbContext dbContext, Type dbContextLocator, EntityState state)
        {
            // 监听token修改
            if(!string.IsNullOrEmpty(newEntity.ApiToken) && newEntity.ApiToken != oldEntity.ApiToken)
            {
                // 单点登录的情况下
                if (GlobalContext.SystemConfig.SingleSign)
                    App.GetService<OperatorCache>().ClearCacheByToken(oldEntity.ApiToken);
            }
        }
    }
}