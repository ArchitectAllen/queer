﻿namespace Queer.Util.Helper
{
    /// <summary>
    /// Http连接操作帮助类 
    /// </summary>
    public class HttpHelper
    {
        /// <summary>
        /// 是否是网址
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static bool IsUrl(string url)
        {
            url = url.ParseToString().ToLower();
            if (url.StartsWith("http://") || url.StartsWith("https://"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}