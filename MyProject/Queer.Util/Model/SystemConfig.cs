﻿using Furion.ConfigurableOptions;

namespace Queer.Util.Model
{
    [OptionsSettings("SystemConfig")]
    public class SystemConfig : IConfigurableOptions
    {
        #region 配置字段

        /// <summary>
        /// 用户默认密码
        /// </summary>
        public string DefaultUserPWD { set; get; }

        /// <summary>
        /// 背景图片资源请求url
        /// </summary>
        public string[] BackgroundGetUrl { set; get; }

        public string PageFolder { get; set; }

        public string IgnoreToken { get; set; }

        public string LogAllApi { get; set; }

        public int SnowFlakeWorkerId { get; set; } = 1;

        /// <summary>
        /// Redis链接串
        /// </summary>
        public string RedisConnectionString { get; set; }

        /// <summary>
        /// 缓存使用类型
        /// </summary>
        public string CacheType { get; set; }

        /// <summary>
        /// 用户登录过期时间（分钟）
        /// </summary>
        public long? UserExpiredTime { get; set; } = 20;

        /// <summary>
        /// 跳过的URL
        /// </summary>
        public string[] IgnoreUrl { set; get; }

        /// <summary>
        /// 是否单点登录
        /// </summary>
        public bool SingleSign { set; get; } = true;

        #endregion

        #region 判断参数

        /// <summary>
        /// 缓存服务
        /// </summary>
        public string CacheService
        {
            get
            {
                if (CacheType.ToUpper() == "REDIS")
                    return "RedisCache";
                else
                    return "MemoryCache";
            }
        }


        #endregion
    }
}