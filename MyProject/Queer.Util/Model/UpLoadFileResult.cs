﻿using System.Collections.Generic;

namespace Queer.Util.Model
{
    /// <summary>
    /// 文件上传返回值
    /// </summary>
    public class UpLoadFileResult
    {
        public int count { get; set; }

        public string Message { get; set; }

        public List<UpLoadFileInfo> Files { get; set; }
    }

    /// <summary>
    /// 文件详细信息
    /// </summary>
    public class UpLoadFileInfo
    {
        public string filename { get; set; }

        public string path { get; set; }

        public int size { get; set; }

        public string fileType { get; set; }
    }
}
