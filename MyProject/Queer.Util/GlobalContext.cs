﻿using Furion;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Queer.Util.Helper;
using Queer.Util.Model;
using System;
using System.Reflection;
using System.Text;

namespace Queer.Util
{

    public class GlobalContext
    {
        public static IWebHostEnvironment HostingEnvironment
        {
            get
            {
                return App.WebHostEnvironment;
            }
        }

        public static string GetVersion()
        {
            Version version = Assembly.GetEntryAssembly().GetName().Version;
            return version.Major + "." + version.Minor;
        }

        /// <summary>
        /// 程序启动时，记录目录
        /// </summary>
        /// <param name="env"></param>
        public static void LogWhenStart(IWebHostEnvironment env)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("程序启动");
            sb.Append(" |ContentRootPath:" + env.ContentRootPath);
            sb.Append(" |WebRootPath:" + env.WebRootPath);
            sb.Append(" |IsDevelopment:" + env.IsDevelopment());
            LogHelper.Debug(sb.ToString());
        }

        /// <summary>
        /// 获取系统配置
        /// </summary>
        public static SystemConfig SystemConfig
        {
            get
            {
                return App.GetOptions<SystemConfig>();
            }
        }

    }
}