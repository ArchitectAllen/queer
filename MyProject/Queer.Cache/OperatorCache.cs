﻿using Furion.DependencyInjection;
using Queer.Enum;
using Queer.IService.OrganizationManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Queer.Cache
{
    /// <summary>
    /// 用户信息缓存
    /// </summary>
    public class OperatorCache : ITransient
    {
        private string _userCacheKeyFix = CacheKeys.UserCacheFix.ParseToString() + '_';

        private readonly ICache _cache;
        private readonly IUserService _userService;

        public OperatorCache(Func<string, ISingleton, object> resolveNamed, IUserService userService)
        {
            _cache = resolveNamed(GlobalContext.SystemConfig.CacheService, default) as ICache;
            _userService = userService;
        }

        /// <summary>
        /// 根据TOKEN查询用户信息，并添加至缓存
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task AddCurrent(string token)
        {
            OperatorInfo user = await _userService.GetUserByToken(token);
            if (user != null)
            {
                _cache.Set(_userCacheKeyFix + token, user, DateTime.Now.AddDays(7));
            }
        }

        /// <summary>
        /// 获取缓存用户信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<OperatorInfo> Current(string token = "")
        {
            OperatorInfo user = null;

            // 如果没传token，就拿请求中的token
            if (token.IsEmpty())
                token = GetToken();

            if (string.IsNullOrEmpty(token))
                return user;

            user = _cache.Get<OperatorInfo>(_userCacheKeyFix + token);

            if (user == null)
                user = await _userService.GetUserByToken(token);

            return user;
        }

        /// <summary>
        /// 更新用户缓存
        /// </summary>
        /// <param name="user"></param>
        public void UpdateOperatorInfo(OperatorInfo user)
        {
            var token = user.ApiToken;

            if (!token.StartsWith(_userCacheKeyFix))
                token = _userCacheKeyFix + token;

            _cache.Set(token, user, DateTime.Now.AddDays(1));
        }

        /// <summary>
        /// 获取token
        /// </summary>
        /// <returns></returns>
        public string GetToken()
        {
            string token = NetHelper.HttpContext.User.FindFirstValue("ApiToken").ParseToString();
            return token;
        }

        /// <summary>
        /// 根据TOKEN，清理缓存
        /// </summary>
        public void ClearCacheByToken(string token)
        {
            if (!token.StartsWith(_userCacheKeyFix))
                token = _userCacheKeyFix + token;

            _cache.Remove(token);
        }

        /// <summary>
        /// 清理当前登录用户缓存
        /// </summary>
        public void ClearThisUserCache()
        {
            var token = GetToken();
            if (!token.StartsWith(_userCacheKeyFix))
                token = _userCacheKeyFix + token;
            _cache.Remove(token);
        }
    }
}
