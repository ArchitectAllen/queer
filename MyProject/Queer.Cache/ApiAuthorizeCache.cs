﻿using Furion.DependencyInjection;
using Queer.Entity;
using Queer.Enum;
using Queer.IService.SystemManage;
using Queer.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Queer.Cache
{
    public class ApiAuthorizeCache : ITransient
    {
        private readonly string _cacheKey = CacheKeys.ApiAuthorizeCache.ParseToString() + '_';

        private readonly ICache _cache;
        private readonly IApiAuthorizeService _apiAuthorizeService;

        public ApiAuthorizeCache(Func<string, ISingleton, object> resolveNamed, IApiAuthorizeService apiAuthorizeService)
        {
            _cache = resolveNamed(GlobalContext.SystemConfig.CacheService, default) as ICache;
            _apiAuthorizeService = apiAuthorizeService;
        }

        /// <summary>
        /// 根据url获取权限标识
        /// </summary>
        public async Task<List<ApiAuthorizeEntity>> GetAuthorizeByUrl(string url = "")
        {
            var list = _cache.Get<List<ApiAuthorizeEntity>>(_cacheKey);
            if (list == null || list.Count() == 0)
            {
                list = await _apiAuthorizeService.GetList(null);
                _cache.Set(_cacheKey, list);
            }

            if (url.IsEmpty())
                return list;

            return list.Where(a => a.Url.ToLower() == url.ToLower()).ToList();
        }

    }
}