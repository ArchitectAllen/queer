﻿using Queer.Data.BaseEntity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Queer.Entity
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-06 14:10
    /// 描 述：职位信息实体类
    /// </summary>

    [Table("SysPosition")]
    public  class PositionEntity : BusinessEntity
    {
        /// <summary>
        /// 职位名称
        /// </summary>
        public string PositionName { get; set; }
        /// <summary>
        /// 职位排序
        /// </summary>
        public int? PositionSort { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

    }
}
