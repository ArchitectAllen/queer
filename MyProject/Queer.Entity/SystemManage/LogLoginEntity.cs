﻿using Queer.Data.BaseEntity;
using Queer.Data.DbContexts;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Queer.Entity
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 12:55
    /// 描 述：登陆日志实体类
    /// </summary>

    [Table("SysLogLogin")]
    public partial class LogLoginEntity : DefaultEntity
    {
        /// <summary>
        /// 执行状态(0失败 1成功)
        /// </summary>
        public int? LogStatus { get; set; }
        /// <summary>
        /// ip地址
        /// </summary>
        public string IpAddress { get; set; }
        /// <summary>
        /// ip位置
        /// </summary>
        public string IpLocation { get; set; }
        /// <summary>
        /// 浏览器
        /// </summary>
        public string Browser { get; set; }
        /// <summary>
        /// 操作系统
        /// </summary>
        public string OS { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 额外备注
        /// </summary>
        public string ExtraRemark { get; set; }

        /// <summary>
        /// 登录人ID
        /// </summary>
        public long? CreateUserId { set; get; }

        [NotMapped]
        public string UserName { get; set; }

        public DateTime? CreateTime { get; set; }
    }
}
