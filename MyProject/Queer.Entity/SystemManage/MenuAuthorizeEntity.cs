﻿using Newtonsoft.Json;
using Queer.Data.BaseEntity;
using Queer.Util.Helper;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Queer.Entity
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 11:16
    /// 描 述：菜单权限实体类
    /// </summary>
    [Table("SysMenuAuthorize")]
    public class MenuAuthorizeEntity : DefaultEntity
    {
        /// <summary>
        /// 菜单Id
        /// </summary>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? MenuId { get; set; }
        /// <summary>
        /// 授权Id(角色Id或者用户Id)
        /// </summary>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? AuthorizeId { get; set; }
        /// <summary>
        /// 授权类型(1角色 2用户)
        /// </summary>
        public int? AuthorizeType { get; set; }

        #region 重写方法

        public override bool Equals(object entity)
        {
            if (entity == null || entity.GetType() != typeof(MenuAuthorizeEntity))
                return false;

            if (this.Id != ((MenuAuthorizeEntity)entity).Id)
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            return (Id != null ? StringComparer.InvariantCulture.GetHashCode(Id) : 0);
        }

        #endregion
    }
}
