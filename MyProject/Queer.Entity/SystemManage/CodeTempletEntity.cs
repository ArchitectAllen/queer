﻿using Queer.Data.BaseEntity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Queer.Entity
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 12:28
    /// 描 述：代码模板实体类
    /// </summary>
    [Table("SysCodeTemplet")]
    public  class CodeTempletEntity : DefaultEntity
    {

        /// <summary>
        /// 代码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 标识
        /// </summary>
        public string Flag { get; set; }

        /// <summary>
        /// 语言类型
        /// </summary>
        public string Type { get; set; }

    }
}
