﻿using System.ComponentModel;

namespace Queer.Enum.SystemManage
{
    public enum AuthorizeTypeEnum
    {
        [Description("角色")]
        Role = 1,

        [Description("用户")]
        User = 2,
    }
}
