﻿using System.ComponentModel;

namespace Queer.Enum
{
    public enum StatusEnum
    {
        [Description("启用")]
        Yes = 1,

        [Description("禁用")]
        No = 0
    }

    public enum IsEnum
    {
        [Description("是")]
        Yes = 1,

        [Description("否")]
        No = 0
    }

    public enum NeedEnum
    {
        [Description("不需要")]
        NotNeed = 0,

        [Description("需要")]
        Need = 1
    }

    public enum OperateStatusEnum
    {
        [Description("失败")]
        Fail = 0,

        [Description("成功")]
        Success = 1
    }

    public enum UploadFileType
    {
        [Description("头像")]
        Portrait = 1,
        [Description("礼品图片")]
        GiftImage = 2,
        [Description("服务商证件照")]
        CertificatesPicts = 3,
        [Description("商家图片")]
        ProviderPicts = 4,
        [Description("商家图片")]
        BusinessLicenseNo = 5,
        [Description("医院图片")]
        HospitalPics = 6,
        [Description("机构图片")]
        OrganizationPics = 7,
        [Description("服务商商品列表展示图")]
        CommodityShowImage = 8,
        [Description("服务商合同附件")]
        ProviderContractFiles = 9,
        [Description("健康资讯文章内图")]
        HealthInfoImage = 10,
        [Description("体检报告相关文件")]
        MedicalExamFile = 11,
    }
    /// <summary>
    /// 登录用户类型
    /// </summary>
    public enum LoginUserType
    {
        [Description("服务商")]
        ServiceProvider = 1,
    }
}