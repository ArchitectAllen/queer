﻿namespace Queer.Enum
{
    public enum CacheKeys
    {
        /// <summary>
        /// 菜单缓存
        /// </summary>
        MenuCache,

        /// <summary>
        /// PC背景图片
        /// </summary>
        ImagePcCache,

        /// <summary>
        /// Phone背景图片
        /// </summary>
        ImagePhoneCache,

        /// <summary>
        /// 用户缓存前缀
        /// </summary>
        UserCacheFix,

        /// <summary>
        /// 菜单权限缓存
        /// </summary>
        MenuAuthorizeCache,

        /// <summary>
        /// 接口权限缓存
        /// </summary>
        ApiAuthorizeCache,
    }
}