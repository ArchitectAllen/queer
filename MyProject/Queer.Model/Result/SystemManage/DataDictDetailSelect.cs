﻿using Queer.Entity;
using System.Collections.Generic;

namespace Queer.Model.Result.SystemManage
{
    public class DataDictDetailSelect
    {
        public string DictType { get; set; }

        public List<DataDictDetailEntity> data { get; set; }
    }
}
