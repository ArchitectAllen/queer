﻿using Newtonsoft.Json;
using Queer.Data.BaseEntity;
using Queer.Util.Helper;
using System.ComponentModel.DataAnnotations.Schema;

namespace Queer.Model.Result.SystemManage
{
    public class MenuCacheModel
    {
        [JsonConverter(typeof(StringJsonConverter))]
        public long? Id { get; set; }


        [JsonConverter(typeof(StringJsonConverter))]
        public long? ParentId { get; set; }

        public string MenuName { get; set; }

        public string MenuIcon { get; set; }

        public string MenuUrl { get; set; }

        public string MenuTarget { get; set; }

        public int MenuSort { get; set; }

        public int MenuType { get; set; }

        public int MenuStatus { get; set; }

        public string Authorize { get; set; }

        public string Remark { get; set; }
    }
}