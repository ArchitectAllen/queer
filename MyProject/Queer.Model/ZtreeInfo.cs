﻿using Newtonsoft.Json;
using Queer.Util.Helper;

namespace Queer.Model
{

    public class ZtreeInfo
    {
        [JsonConverter(typeof(StringJsonConverter))]
        public long? id { get; set; }

        [JsonConverter(typeof(StringJsonConverter))]
        public long? pId { get; set; }

        public string name { get; set; }

        public bool open { get { return true; } }

        public bool @checked { set; get; }

        public string title { get { return name; } }

        public bool disabled { get; set; }

    }
}
