﻿namespace Queer.Model.Param.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-19 08:59
    /// 描 述：数据字典值实体查询类
    /// </summary>

    public class DataDictDetailListParam
    {
        public string DictType { get; set; }

        public int? DictKey { get; set; }

        public string DictValue { get; set; }

        public int? DictStatus { set; get; }

        public int? BaseIsDelete { set; get; }

        public string DictTypes { get; set; }

    }
}