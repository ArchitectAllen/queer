﻿using Newtonsoft.Json;
using Queer.Util.Helper;

namespace Queer.Model.Param.OrganizationManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 12:41
    /// 描 述：用户信息实体查询类
    /// </summary>

    public class UserListParam
    {
        /// <summary>
        /// 用户名
        /// </summary>
        /// <returns></returns>
        public string UserName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        /// <returns></returns>
        public string Mobile { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        /// <returns></returns>
        public int? UserStatus { get; set; }

        public long? QueryDepartmentId { get; set; }

    }

    /// <summary>
    /// 用户基本数据
    /// </summary>
    public class UserBaseDataModel
    {
        [JsonConverter(typeof(StringJsonConverter))]
        public long? Id { get; set; }
        /// <summary>
        /// 出生日期
        /// </summary>
        public string Birthday { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string Portrait { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// QQ
        /// </summary>
        public string QQ { get; set; }
        /// <summary>
        /// 微信
        /// </summary>
        public string WeChat { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        public string Tags { get; set; }

    }
}
