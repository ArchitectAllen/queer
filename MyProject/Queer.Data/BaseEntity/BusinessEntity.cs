﻿using Newtonsoft.Json;
using Queer.Util.Helper;
using System;
using System.ComponentModel;

namespace Queer.Data.BaseEntity
{
    /// <summary>
    /// 业务Entity基类
    /// </summary>
    public abstract class BusinessEntity : DefaultEntity
    {
        /// <summary>
        /// 删除标志 0：未删除 1：已删除
        /// </summary>
        [DefaultValue(false)]
        [FakeDelete(true)]
        public bool? IsDelete { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 创建人ID编号
        /// </summary>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? CreateUserId { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 删除人ID编号
        /// </summary>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? DeleteUserId { get; set; }
        /// <summary>
        /// 最后修改时间
        /// </summary>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 最后修改人ID编号
        /// </summary>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? LastModifyUserId { get; set; }
    }
}
