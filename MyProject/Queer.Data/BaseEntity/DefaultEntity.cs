﻿using Furion.DatabaseAccessor;
using Newtonsoft.Json;
using Queer.Util.Helper;

namespace Queer.Data.BaseEntity
{
    /// <summary>
    /// 自定义数据库 Entity基类
    /// </summary>
    public abstract class DefaultEntity : IEntity<MasterDbContextLocator>
    {
        [JsonConverter(typeof(StringJsonConverter))]
        public long? Id { get; set; }

    }

}
