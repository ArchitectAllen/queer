﻿using Furion;
using Furion.DatabaseAccessor;
using Microsoft.Extensions.DependencyInjection;
using Queer.Data.DbContexts;
using System;
using System.Configuration;

namespace Queer.Data
{
    [AppStartup(10)]
    public class DataAppStartup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            string dbType = App.Configuration["ConnectionStrings:DBType"];

            // 数据库访问注册
            services.AddDatabaseAccessor(options =>
            {
                // 注册数据库访问上下文
                switch (dbType.ToLower())
                {
                    case "sqlserver":
                        options.AddDbPool<DefaultDbContext>(DbProvider.SqlServer);
                        break;

                    case "mysql":
                        options.AddDbPool<DefaultDbContext>(DbProvider.MySqlOfficial);
                        break;

                    default:
                        throw new ConfigurationErrorsException($"数据库类型：“{dbType.ToLower()}”暂不支持");
                }
            });

        }

    }
}