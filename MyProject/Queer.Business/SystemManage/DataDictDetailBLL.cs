﻿using Furion.DependencyInjection;
using Queer.Entity;
using Queer.IBusiness.SystemManage;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Model.Result.SystemManage;
using Queer.Util;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.Business.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-19 08:59
    /// 描 述：数据字典值业务类
    /// </summary>

    public class DataDictDetailBLL : IDataDictDetailBLL, ITransient
    {
        private IDataDictDetailService _dataDictDetailService;

        public DataDictDetailBLL(IDataDictDetailService dataDictDetailService)
        {
            _dataDictDetailService = dataDictDetailService;
        }

        #region 获取数据

        public async Task<TData<List<DataDictDetailEntity>>> GetList(DataDictDetailListParam param)
        {
            TData<List<DataDictDetailEntity>> obj = new TData<List<DataDictDetailEntity>>();
            obj.Data = await _dataDictDetailService.GetList(param);
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<DataDictDetailEntity>>> GetPageList(DataDictDetailListParam param, Pagination pagination)
        {
            TData<List<DataDictDetailEntity>> obj = new TData<List<DataDictDetailEntity>>();
            obj.Data = await _dataDictDetailService.GetPageList(param, pagination);
            obj.Total = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<DataDictDetailEntity>> GetEntity(long id)
        {
            TData<DataDictDetailEntity> obj = new TData<DataDictDetailEntity>();
            obj.Data = await _dataDictDetailService.GetEntity(id);
            obj.Tag = 1;
            return obj;
        }

        public TData<List<DataDictDetailSelect>> GetListJsonForSelect(DataDictDetailListParam param)
        {
            TData<List<DataDictDetailSelect>> obj = new TData<List<DataDictDetailSelect>>();
            obj.Data = _dataDictDetailService.GetListJsonForSelect(param);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 提交数据

        public async Task<TData<string>> SaveForm(DataDictDetailEntity entity)
        {
            TData<string> obj = new TData<string>();
            await _dataDictDetailService.SaveForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await _dataDictDetailService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }

        #endregion
    }
}