﻿using Furion.DependencyInjection;
using Queer.Entity;
using Queer.IBusiness.SystemManage;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.Business.SystemManage
{
    /// <summary>
    /// 创 建：
    /// 日 期：2020-12-04 12:49
    /// 描 述：Api日志业务类
    /// </summary>

    public class LogApiBLL : ILogApiBLL, ITransient
    {
        private ILogApiService _logApiService;

        public LogApiBLL(ILogApiService logApiService)
        {
            _logApiService = logApiService;
        }

        #region 获取数据

        public async Task<TData<List<LogApiEntity>>> GetList(LogApiListParam param)
        {
            TData<List<LogApiEntity>> obj = new TData<List<LogApiEntity>>();
            obj.Data = await _logApiService.GetList(param);
            obj.Data.ForEach(a => a.IpLocation = IpLocationHelper.GetIpLocation(a.IpAddress));
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<LogApiEntity>>> GetPageList(LogApiListParam param, Pagination pagination)
        {
            TData<List<LogApiEntity>> obj = new TData<List<LogApiEntity>>();
            obj.Data = await _logApiService.GetPageList(param, pagination);

            obj.Data.ForEach(a => a.IpLocation = IpLocationHelper.GetIpLocation(a.IpAddress));

            obj.Total = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<LogApiEntity>> GetEntity(long id)
        {
            TData<LogApiEntity> obj = new TData<LogApiEntity>();
            obj.Data = await _logApiService.GetEntity(id);
            obj.Tag = 1;
            return obj;
        }

        #endregion

        #region 提交数据

        public async Task<TData<string>> SaveForm(LogApiEntity entity)
        {
            TData<string> obj = new TData<string>();
            await _logApiService.SaveForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await _logApiService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }

        #endregion
    }
}
