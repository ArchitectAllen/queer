﻿using Furion.DependencyInjection;
using Queer.Entity;
using Queer.IBusiness.SystemManage;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.Business.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-18 16:04
    /// 描 述：数据字典业务类
    /// </summary>

    public class DataDictBLL : IDataDictBLL, ITransient
    {
        private IDataDictService _dataDictService;

        public DataDictBLL(IDataDictService dataDictService)
        {
            _dataDictService = dataDictService;
        }

        #region 获取数据

        public async Task<TData<List<DataDictEntity>>> GetList(DataDictListParam param)
        {
            TData<List<DataDictEntity>> obj = new TData<List<DataDictEntity>>();
            obj.Data = await _dataDictService.GetList(param);
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<DataDictEntity>>> GetPageList(DataDictListParam param, Pagination pagination)
        {
            TData<List<DataDictEntity>> obj = new TData<List<DataDictEntity>>();
            obj.Data = await _dataDictService.GetPageList(param, pagination);
            obj.Total = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<DataDictEntity>> GetEntity(long id)
        {
            TData<DataDictEntity> obj = new TData<DataDictEntity>();
            obj.Data = await _dataDictService.GetEntity(id);
            obj.Tag = 1;
            return obj;
        }

        #endregion

        #region 提交数据

        public async Task<TData<string>> SaveForm(DataDictEntity entity)
        {
            TData<string> obj = new TData<string>();
            await _dataDictService.SaveForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await _dataDictService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }

        #endregion
    }
}