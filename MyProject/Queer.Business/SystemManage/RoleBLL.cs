﻿using Furion;
using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Queer.Cache;
using Queer.Entity;
using Queer.Enum.SystemManage;
using Queer.IBusiness.SystemManage;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Queer.Business.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-06 09:46
    /// 描 述：角色信息业务类
    /// </summary>

    public class RoleBLL : IRoleBLL, ITransient
    {
        private IRoleService _roleService;
        private IMenuAuthorizeService _menuAuthorizeService;
        private OperatorCache _operatorCache;

        public RoleBLL(IRoleService roleService, OperatorCache operatorCache, IMenuAuthorizeService menuAuthorizeService)
        {
            _roleService = roleService;
            _menuAuthorizeService = menuAuthorizeService;
            _operatorCache = operatorCache;
        }

        #region 获取数据

        public async Task<TData<List<RoleEntity>>> GetList(RoleListParam param)
        {
            TData<List<RoleEntity>> obj = new TData<List<RoleEntity>>();
            obj.Data = await _roleService.GetList(param);
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<RoleEntity>>> GetPageList(RoleListParam param, Pagination pagination)
        {
            TData<List<RoleEntity>> obj = new TData<List<RoleEntity>>();
            obj.Data = await _roleService.GetPageList(param, pagination);
            obj.Total = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<RoleEntity>> GetEntity(long id)
        {
            TData<RoleEntity> obj = new TData<RoleEntity>();
            obj.Data = await _roleService.GetEntity(id);

            List<long> menuIds = await _menuAuthorizeService.GetMenuIdList(new MenuListParam
            {
                AuthorizeId = id,
                AuthorizeType = AuthorizeTypeEnum.Role.ParseToInt()
            });

            // 获取角色对应的权限
            obj.Data.MenuIds = string.Join(",", menuIds);

            obj.Tag = 1;
            return obj;
        }

        /// <summary>
        /// 保存权限
        /// </summary>
        public async Task<TData> SaveRoleAuth(long roleId, string menuIds)
        {
            TData obj = new TData();
            await _roleService.SaveRoleAuth(roleId, menuIds);

            // 需要清除该角色对应用户的权限缓存
            var userTokens = await $"select a.apitoken from SysUser a join SysUserBelong b on a.id = b.UserId where b.BelongId = {roleId}".SqlQueryAsync<string>();
            foreach (var token in userTokens)
                _operatorCache.ClearCacheByToken(token);

            obj.Tag = 1;
            return obj;
        }

        #endregion

        #region 提交数据

        public async Task<TData<string>> SaveForm(RoleEntity entity)
        {
            TData<string> obj = new TData<string>();
            await _roleService.SaveForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await _roleService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }

        #endregion
    }
}
