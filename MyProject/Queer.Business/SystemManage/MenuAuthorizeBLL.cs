﻿using Furion.DependencyInjection;
using Queer.Cache;
using Queer.Entity;
using Queer.Enum;
using Queer.Enum.SystemManage;
using Queer.IBusiness.SystemManage;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Queer.Business.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 11:16
    /// 描 述：菜单权限业务类
    /// </summary>

    public class MenuAuthorizeBLL : IMenuAuthorizeBLL, ITransient
    {
        private IMenuAuthorizeService _menuAuthorizeService;
        private MenuCache _menuCache;

        public MenuAuthorizeBLL(IMenuAuthorizeService menuAuthorizeService, MenuCache menuCache)
        {
            _menuAuthorizeService = menuAuthorizeService;
            _menuCache = menuCache;
        }

        #region 获取数据

        public async Task<TData<List<MenuAuthorizeEntity>>> GetList(MenuAuthorizeListParam param)
        {
            TData<List<MenuAuthorizeEntity>> obj = new TData<List<MenuAuthorizeEntity>>();
            obj.Data = await _menuAuthorizeService.GetList(param);
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<MenuAuthorizeEntity>>> GetPageList(MenuAuthorizeListParam param, Pagination pagination)
        {
            TData<List<MenuAuthorizeEntity>> obj = new TData<List<MenuAuthorizeEntity>>();
            obj.Data = await _menuAuthorizeService.GetPageList(param, pagination);
            obj.Total = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<MenuAuthorizeEntity>> GetEntity(long id)
        {
            TData<MenuAuthorizeEntity> obj = new TData<MenuAuthorizeEntity>();
            obj.Data = await _menuAuthorizeService.GetEntity(id);
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<MenuAuthorizeInfo>>> GetAuthorizeList(OperatorInfo user)
        {
            TData<List<MenuAuthorizeInfo>> obj = new() { Data = new List<MenuAuthorizeInfo>(),Tag =1 };

            // 取出状态为启用中的菜单（页面、目录、按钮）
            var menuList = (await _menuCache.GetMenuCacheList()).Where(p => p.MenuStatus == (int)StatusEnum.Yes);

            // 超管账号直接无视
            if (user.IsSystem ==1)
            {
                obj.Data = menuList.Select(a => new MenuAuthorizeInfo()
                {
                    Authorize = a.Authorize,
                    MenuId = a.Id
                }).ToList();

                return obj;
            }

            // 取出该用户所有权限
            var menuAuthorizeCacheList = await _menuAuthorizeService.GetUserAuth(user.UserId.Value);

            // 对比，排除禁用的权限
            var enableMenuIdList = menuList.Select(p => p.Id);
            menuAuthorizeCacheList = menuAuthorizeCacheList.Where(p => enableMenuIdList.Contains(p.MenuId)).ToList();

            // 写入权限标识
            foreach (var authorize in menuAuthorizeCacheList)
            {
                obj.Data.Add(new MenuAuthorizeInfo
                {
                    MenuId = authorize.MenuId,
                    AuthorizeId = authorize.AuthorizeId,
                    AuthorizeType = authorize.AuthorizeType,
                    Authorize = menuList.Where(t => t.Id == authorize.MenuId).Select(t => t.Authorize).FirstOrDefault()
                });
            }

            return obj;
        }

        #endregion

        #region 提交数据

        public async Task<TData<string>> SaveForm(MenuAuthorizeEntity entity)
        {
            TData<string> obj = new TData<string>();
            await _menuAuthorizeService.SaveForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await _menuAuthorizeService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }


        #endregion
    }
}
