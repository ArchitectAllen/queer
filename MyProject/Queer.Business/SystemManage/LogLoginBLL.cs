﻿using Furion.DependencyInjection;
using Queer.Entity;
using Queer.IBusiness.SystemManage;
using Queer.IService.SystemManage;
using Queer.Model.Param.SystemManage;
using Queer.Util;
using Queer.Util.Helper;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.Business.SystemManage
{
    /// <summary>
    /// 创 建：song
    /// 日 期：2020-12-04 12:55
    /// 描 述：登陆日志业务类
    /// </summary>

    public class LogLoginBLL : ILogLoginBLL, ITransient
    {
        private ILogLoginService _logLoginService;

        public LogLoginBLL(ILogLoginService logLoginService)
        {
            _logLoginService = logLoginService;
        }

        #region 获取数据

        public async Task<TData<List<LogLoginEntity>>> GetList(LogLoginListParam param)
        {
            TData<List<LogLoginEntity>> obj = new TData<List<LogLoginEntity>>();
            obj.Data = await _logLoginService.GetList(param);
            obj.Data.ForEach(a => a.IpLocation = IpLocationHelper.GetIpLocation(a.IpAddress));
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<LogLoginEntity>>> GetPageList(LogLoginListParam param, Pagination pagination)
        {
            TData<List<LogLoginEntity>> obj = new TData<List<LogLoginEntity>>();
            obj.Data = await _logLoginService.GetPageList(param, pagination);
            obj.Total = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<LogLoginEntity>> GetEntity(long id)
        {
            TData<LogLoginEntity> obj = new TData<LogLoginEntity>();
            obj.Data = await _logLoginService.GetEntity(id);
            obj.Tag = 1;
            return obj;
        }

        #endregion

        #region 提交数据

        public async Task<TData<string>> SaveForm(LogLoginEntity entity)
        {
            TData<string> obj = new TData<string>();
            await _logLoginService.SaveForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await _logLoginService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }

        #endregion
    }
}
