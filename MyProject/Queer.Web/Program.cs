using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.IO;

namespace Queer.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) => Host
            .CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((hostingContext, config) =>
            {
                // 基础目录
                var basePath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "OtherSettings" + Path.DirectorySeparatorChar;
                // 加载自定义配置
                config.AddJsonFile(basePath + "dbsetting.json", optional: true, reloadOnChange: true);
                config.AddJsonFile(basePath + "mqttsetting.json", optional: true, reloadOnChange: true);
                config.AddJsonFile(basePath + "swaggersetting.json", optional: true, reloadOnChange: true);
                config.AddJsonFile(basePath + "systemsetting.json", optional: true, reloadOnChange: true);
            })
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.Inject().UseStartup<Startup>();
            });
    }
}
