using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Queer.Util.Helper;
using Queer.Util.Model;
using System.IO;

namespace Queer.Web
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // ��ȡ����
            services.AddConfigurableOptions<SystemConfig>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            DefaultFilesOptions defaultFilesOptions = new DefaultFilesOptions();
            defaultFilesOptions.DefaultFileNames.Clear();
            defaultFilesOptions.DefaultFileNames.Add("index.html");
            app.UseDefaultFiles(defaultFilesOptions);

            // ע��wwwroot��̬�ļ�����
            app.UseStaticFiles();

            // ע��Resource��̬�ļ�����
            string resource = Path.Combine(env.ContentRootPath, "Resource");
            FileHelper.CreateDirectory(resource);
            app.UseStaticFiles(new StaticFileOptions
            {
                RequestPath = "/Resource",
                FileProvider = new PhysicalFileProvider(resource),
            });
        }
    }
}