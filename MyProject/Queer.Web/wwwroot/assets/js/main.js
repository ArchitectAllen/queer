layui.config({
    version: '1',
    base: '/assets/module/'
}).extend({
    steps: 'steps/steps',
    notice: 'notice/notice',
    cascader: 'cascader/cascader',
    //dropdown: 'dropdown/dropdown',
    fileChoose: 'fileChoose/fileChoose',
    Split: 'Split/Split',
    Cropper: 'Cropper/Cropper',
    tagsInput: 'tagsInput/tagsInput',
    citypicker: 'city-picker/city-picker',
    introJs: 'introJs/introJs',
    zTree: 'zTree/zTree',
    opTable: 'opTable/opTable',
    autocomplete: 'autocomplete/autocomplete',
    xAce: 'xAce/xAce',
    iconPicker: 'iconPicker',
    iconExtend: 'iconExtend/iconExtend',
    nprogress: 'nprogress/nprogress',
    CountUp: 'CountUp',
    cron: "cron/cron"
}).use(['layer', 'setter', 'index', 'admin', 'setter', 'iconExtend'], function () {
    var setter = layui.setter;

    layui.iconExtend.loadProject('yq_icon');//页面初始化阿里图标库组件 ‘\module\iconExtend\iconfont\yq_icon’图标文件替换目录

    if (!setter.getToken()) {
        location.href = '/index.html';
        return;
    }
});