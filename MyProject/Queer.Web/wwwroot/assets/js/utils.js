﻿/**
 * 图片表格显示用
 * */
var LAY_INDEX = 0;

/**
 * 对layui表格进行全局设置
 * */
function layuiTableSet() {

    if (!layui.table) {
        layer.msg("代码错误，请先引入 table 模块", { icon: 2 });
        return;
    }

    var table_set = {
        headers: { 'Authorization': "Bearer " + layui.setter.getToken().JwtToken },
        request: {
            pageName: 'PageIndex' //页码的参数名称，默认：page
            , limitName: 'PageSize' //每页数据量的参数名，默认：limit
        },
        limit: 15,
        limits: [15, 30, 45, 60],
        response: {
            statusName: 'Tag' //规定数据状态的字段名称，默认：code
            , statusCode: 1 //规定成功的状态码，默认：0
            , msgName: 'Message' //规定状态信息的字段名称，默认：msg
            , countName: 'Total' //规定数据总数的字段名称，默认：count
            , dataName: 'Data' //规定数据列表的字段名称，默认：data
        },
        defaultToolbar: ['filter', {
            title: '导出' //标题
            , layEvent: 'export' //事件名，用于 toolbar 事件中使用
            , icon: 'layui-icon-export' //图标类名
        }],
        done: function () {
            // 校验权限，如果done被重写了，记得加这个方法
            layui.admin.renderPerm();
        }
    };

    // 根据高度判断表格应该显示多少
    var height = $(window).height();
    if (height > 900) {
        table_set.height = 'full-160';
        table_set.limit = 20;
        table_set.limits = [20, 30, 40, 50, 60, 70, 80, 90, 100];
    } else {
        table_set.height = 'full-133';
        table_set.limit = 15;
        table_set.limits = [15, 30, 45, 60];
    }

    layui.table.set(table_set);
}

/**
 * 表格导出
 * @param {Array} cols
 * @param {string} url
 * @param {string} excelName
 */
function layuiTableExport(cols, url, excelName) {

    if (!layui.tableX) {
        layer.msg("代码错误，请先引入 tableX 模块", { icon: 2 });
        return;
    }

    var loadIndex = layer.load(2);
    layui.admin.req(url, function (res) {
        layer.close(loadIndex);
        if (1 === res.Tag) {
            layui.tableX.exportData({
                cols: cols,
                data: res.Data,
                fileName: (excelName ? excelName : "导出表格")
            });
        } else {
            layer.msg(res.Message, { icon: 2 });
        }
    }, 'get');
}


/**
 * 获取get字段
 * @param {string} name
 */
function getRequestData(name) {
    var params = decodeURI(window.location.search);
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = params.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}

/**
 * 初始化 layui 复选框 form模块，div id名称，name 复选框name， arr数组：[{Key:1,Value:1}]
 * @param {any} form
 * @param {any} id
 * @param {any} name
 * @param {any} arr
 * @param {any} keyName
 * @param {any} valueName
 */
function setLayuiCheckbox(form, id, name, arr, keyName, valueName) {
    var h = [];
    for (var i = 0; i < arr.length; i++) {
        var a = arr[i];
        h.push('<input type="checkbox" name="');
        h.push(name);
        h.push('" title="');

        if (keyName) {
            h.push(a[keyName]);
        } else {
            h.push(a.Key);
        }

        h.push('" value="');

        if (valueName) {
            h.push(a[valueName]);
        } else {
            h.push(a.Value);
        }

        h.push('"  lay-filter="');
        h.push(name);
        h.push('" lay-skin="primary"> ');
    }

    var str = h.join('');
    if (id.substr(0, 1) == "#") {
        $(id).html(str);
    } else {
        $("#" + id).html(str);
    }

    form.render('checkbox'); //刷新select选择框渲染
}

/**
 * xmSelect序列化数据
 * @param {any} data
 */
function listToTree(data) {
    let arr = JSON.parse(JSON.stringify(data))
    const listChildren = (obj, filter) => {
        [arr, obj.children] = arr.reduce((res, val) => {
            if (filter(val))
                res[1].push(val)
            else
                res[0].push(val)
            return res
        }, [[], []])
        obj.children.forEach(val => {
            if (arr.length)
                listChildren(val, obj => obj.pId === val.id)
        })
    }
    const tree = {}
    listChildren(tree, val => arr.findIndex(i => i.id === val.pId) === -1)
    return tree.children
}

/**
 * 递归获取树形数组的值（键值为value）
 * @param {any} Arr
 * @param {any} ID
 */
function findTreeId(Arr, ID) {
    var _result = null;
    for (let i = 0; i < Arr.length; i++) {
        //console.log(Arr[i], Arr[i].id == ID)
        if (Arr[i].value == ID) return Arr[i];
        if (Arr[i].children) _result = findTreeId(Arr[i].children, ID)
        if (_result != null) return _result;
    }
    return _result
}