layui.define(['table'], function (exports) {
    var setter = {
        // 接口地址，实际项目请换成自己的接口
        baseServer: 'http://localhost:5005',
        postAjaxUseBody: true, // admin.req 的 post请求默认使用body传递
        pageTabs: true,   // 是否开启多标签
        cacheTab: false,  // 是否记忆Tab
        defaultTheme: '',  // 默认主题
        openTabCtxMenu: true,   // 是否开启Tab右键菜单
        maxTabNum: 20,  // 最多打开多少个tab
        viewPath: '/page', // 视图位置
        viewSuffix: '.html',  // 视图后缀
        reqPutToPost: false,  // req请求put方法变成post
        apiNoCache: true,  // ajax请求json数据不带版本号
        tableName: 'project',  // 存储表名
        getJwtHeader: function () {
            return { 'Authorization': "Bearer " + setter.getToken().JwtToken };
        },
        /* 获取缓存的token */
        getToken: function () {
            var cache = layui.data(setter.tableName);
            if (cache) return cache.token;
        },
        /* 清除token */
        removeToken: function () {
            layui.data(setter.tableName, { key: 'token', remove: true });
        },
        /* 缓存token */
        putToken: function (token) {
            layui.data(setter.tableName, { key: 'token', value: token });
        },
        /* 当前登录的用户 */
        getUser: function () {
            var cache = layui.data(setter.tableName);
            if (cache) return cache.loginUser;
        },
        /* 缓存user */
        putUser: function (user) {
            layui.data(setter.tableName, { key: 'loginUser', value: user });
        },
        /* 获取用户所有权限 */
        getUserAuths: function () {

            var auths = [];

            // 取后台返回的所有数据
            var allAuths = setter.getUser().MenuAuthorizes;

            // 去除无用数据
            for (var i = 0; i < allAuths.length; i++) {
                if (allAuths[i].Authorize && allAuths[i].Authorize.length > 0) {
                    auths.push(allAuths[i].Authorize);
                }
            }

            return auths;
        },
        /* ajax请求的header */
        getAjaxHeaders: function (url) {

            var headers = [];
            var token = setter.getToken();
            if (token)
                headers.push({ name: 'Authorization', value: "Bearer " + token.JwtToken });

            return headers;
        },
        /* ajax请求结束后的处理，返回false阻止代码执行 */
        ajaxSuccessBefore: function (res, url, obj) {

            if (res.code === 400) {  // 404 未找到
                layui.layer.msg('请求路径不存在', { icon: 2, anim: 6, time: 2000 });
                return false;
            }

            if (res.code === 401) {  // 登录过期退出到登录界面
                setter.removeToken();
                layui.layer.alert('登录过期', {
                    icon: 2, anim: 6, time: 2000, end: function () {
                        location.href = '/page/login.html';
                    }
                });
                return false;
            }

            if (res.code === 415) {  // 415 媒体类型不正确
                layui.layer.msg('参数格式不正确', { icon: 2, anim: 6, time: 2000 });
                return false;
            }

            return true;
        },
        /* 路由不存在处理 */
        routerNotFound: function (r) {
            layui.layer.alert('很抱歉，路径：<span class="text-danger">' + r.path.join('/') + '</span>不存在', {
                title: '提示', offset: '30px', skin: 'layui-layer-admin', btn: [], anim: 6, shadeClose: true
            });
        }
    };

    setter.base_server = setter.baseServer;  // 兼容旧版
    exports('setter', setter);
});
