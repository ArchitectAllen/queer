﻿function request(d) {
    for (var c = location.search.slice(1).split("&"), a = 0; a < c.length; a++) { var b = c[a].split("="); if (b[0] == d) if ("undefined" == unescape(b[1])) break; else return unescape(b[1]) } return ""
};

var objectId = '';
var selectIds = request('selectIds');
var companyId = 'fc84a4a7-f5fa-499b-90de-71fbd5666fbb';
var departmentId = request('departmentId');

var acceptClick;
var userlistselected = [];
var userlistselectedobj = {};
var bootstrap = function ($, learun) {
    "use strict";

    var userlist = {};

    // 渲染用户列表
    function renderUserlist(list) {
        var $warp = $('<div></div>');
        for (var i = 0, l = list.length; i < l; i++) {
            var item = list[i];
            var active = "";
            var photoName = "UserCard01.png";
            if (userlistselected.indexOf(item.Id) != -1) {
                active = "active";
            }
            if (item.Sex) {
                photoName = "UserCard02.png";
            }
            var _cardbox = "";
            _cardbox += '<div class="card-box ' + active + '" data-value="' + item.Id + '" >';
            _cardbox += '    <div class="card-box-img">';
            _cardbox += '        <img src="/Content/images/' + photoName + '" />';
            _cardbox += '    </div>';
            _cardbox += '    <div class="card-box-content">';
            _cardbox += '        <p>账户：' + item.RealName + '</p>';
            _cardbox += '        <p>姓名：' + item.RealName + '</p>';
            _cardbox += '        <p>部门：<span data-id="' + item.DepartmentID + '">' + item.RealName + '</span></p>';
            _cardbox += '    </div>';
            _cardbox += '</div>';
            var $cardbox = $(_cardbox);
            $cardbox[0].userinfo = item;
            $warp.append($cardbox);
        }
        $warp.find('.card-box').on('click', function () {
            var $this = $(this);
            var userid = $this.attr('data-value');
            if ($this.hasClass('active')) {
                $this.removeClass('active');
                removeUser(userid);
                userlistselected.splice(userlistselected.indexOf(userid), 1);
                delete userlistselectedobj[userid];
            }
            else {
                $this.addClass('active');
                userlistselectedobj[userid] = $this[0].userinfo;
                userlistselected.push(userid);
                addUser($this[0].userinfo);
            }
        });

        $('#user_list').html($warp);
    };

    function addUser(useritem) {
        var $warp = $('#selected_user_list');
        var _html = '<div class="user-selected-box" data-value="' + useritem.Id + '" >';
        _html += '<p><span data-id="' + useritem.OrganizationID + '">' + useritem.OrgName + '</span></p>';
        _html += '<p><span data-id="' + useritem.DepartmentID + '">' + useritem.DeptName + '</span>【' + useritem.TrueName + '】</p>';
        _html += '<span class="user-reomve" title="移除选中人员"></span>';
        _html += '</div>';
        $warp.append(_html);
    };
    function removeUser(userid) {
        var $warp = $('#selected_user_list');
        $warp.find('[data-value="' + userid + '"]').remove();
    };

    function removeObj(obj, objPropery, objValue) {
        return $.each(obj, function (v, o) {
            return v[objPropery] != objValue;
        });
    }

    var page = {
        init: function () {
            page.bind();
            page.initData();
        },
        bind: function () {
            // 用户
            $('#department_tree').lrtree({
                nodeClick: function (item) {
                    departmentId = item.id;
                    if (!!userlist[item.id]) {
                        renderUserlist(userlist[item.id]);
                    }
                    else {
                        admin.req('/OrganizationManage/Company/GetLearnTree', { DepartmentId: departmentId }, function (res) {
                            if (res.Tag == 1) {
                                userlist[item.id] = res.Data || [];
                                renderUserlist(userlist[item.id]);
                            }
                        }, 'get', { async: false });
                        //learun.httpAsync('GET', '/SysUser/GetLearnList', { companyId: companyId, departmentId: departmentId }, function (data) {
                        //    userlist[item.id] = data || [];
                        //    renderUserlist(userlist[item.id]);
                        //});
                    }
                }
            });
            // 部门
            $('#company_select').lrCompanySelect({ isLocal: true }).bind('change', function () {
                
                companyId = $(this).lrselectGet();
                $('#department_tree').lrtreeSet('refresh', {
                    url: '/OrganizationManage/Department/GetDepartmentLearnTreeModel',
                    // 访问数据接口参数
                    param: { companyId: companyId }
                });
            });
            // 已选人员按钮
            $('#user_selected_btn').on('click', function () {
                $('#form_warp_right').animate({ right: '0px' }, 300);
            });
            $('#user_selected_btn_close').on('click', function () {
                $('#form_warp_right').animate({ right: '-180px' }, 300);
            });

            //全选按钮
            $("#btnAllCheck").on('click', function () {
                var $warp = $('#selected_user_list');
                var userIds = "";
                $.each(userlist[departmentId], function (id, item) {
                    if (item) {
                        userlistselectedobj[item.Id] = item;
                        userIds += item.Id + ',';
                    }
                });
                userIds = userIds.substring(0, userIds.length - 1);
                var userList = userIds.split(',');
                for (var i = 0, l = userList.length; i < l; i++) {
                    var userId = userList[i];
                    var item = userlistselectedobj[userId];
                    if (!!item) {
                        if (userlistselected.indexOf(userId) == -1) {
                            userlistselected.push(userId);

                            var _html = '<div class="user-selected-box" data-value="' + item.Id + '" >';
                            _html += '<p><span data-id="' + item.OrganizationID + '">' + item.OrgName + '</span></p>';
                            _html += '<p><span data-id="' + item.DepartmentID+ '">' + item.DeptName + '</span>【' + item.TrueName + '】</p>';
                            _html += '<span class="user-reomve" title="移除选中人员"></span>';
                            _html += '</div>';
                            $warp.append($(_html));
                            $('#user_list').find('[data-value="' + item.Id + '"]').addClass('active');
                        }
                    }
                }
            });

            // 选中人员按钮点击事件
            $('#selected_user_list').on('click', function (e) {
                var et = e.target || e.srcElement;
                var $et = $(et);
                if ($et.hasClass('user-reomve')) {
                    var userid = $et.parent().attr('data-value');
                    removeUser(userid);
                    userlistselected.splice(userlistselected.indexOf(userid), 1);
                    delete userlistselectedobj[userid];
                    $('#user_list').find('[data-value="' + userid + '"]').removeClass('active');
                }
            });

            // 滚动条
            $('#user_list_warp').lrscroll();
            $('#selected_user_list_warp').lrscroll();
        },
        initData: function () {
            if (!!companyId) {
                $('#company_select').lrselectSet(companyId);
            }
            if (!!departmentId) {
                $('#department_tree').lrtreeSet('setValue', departmentId);
            }

            $.lrSetForm('/OrganizationManage/User/GetListJson?UserIds=' + selectIds, function (data) {
                var userInfoList = data.Data;
                if (selectIds == "") {
                    return false;
                }
                var $warp = $('#selected_user_list');
                $.each(userInfoList, function (id, item) {
                    if (item) {
                        userlistselectedobj[item.Id] = item;
                    }
                });
                var userList = selectIds.split(',');
                for (var i = 0, l = userList.length; i < l; i++) {
                    var userId = userList[i];
                    var item = userlistselectedobj[userId];
                    if (!!item) {
                        if (userlistselected.indexOf(userId) == -1) {
                            userlistselected.push(userId);
                        }

                        var _html = '<div class="user-selected-box" data-value="' + item.Id + '" >';
                        //_html += '<p><span data-id="' + item.OrganizationID + '">' + item.RealName + '</span></p>';
                        _html += '<p><span data-id="' + item.DepartmentId + '">' + item.RealName + '</span>【' + item.RealName + '】</p>';
                        _html += '<span class="user-reomve" title="移除选中人员"></span>';
                        _html += '</div>';
                        $warp.append($(_html));
                        $('#user_list').find('[data-value="' + item.Id + '"]').addClass('active');
                    }
                }
            });
        }
    };
    // 保存数据
    acceptClick = function () {
        layui.use(['layer'], function () {
            var layer = layui.layer;
            var loading = layer.load(2, {
                shade: [0.6, '#fff'] //0.1透明度的白色背景
            });
            alert(String(userlistselected));
        });

        return false;
    };
    page.init();
}