﻿using Queer.Entity;
using Queer.Model.Param.OrganizationManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.IBusiness.OrganizationManage
{
    public interface IUserBelongBLL
    {
        #region 获取数据

        Task<TData<List<UserBelongEntity>>> GetList(UserBelongListParam param);

        Task<TData<List<UserBelongEntity>>> GetPageList(UserBelongListParam param, Pagination pagination);

        Task<TData<UserBelongEntity>> GetEntity(long id);

        #endregion

        #region 提交数据

        Task<TData<string>> SaveForm(UserBelongEntity entity);

        Task<TData> DeleteForm(string ids);

        #endregion
    }
}
