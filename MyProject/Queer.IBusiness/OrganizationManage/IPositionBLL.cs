﻿using Queer.Entity;
using Queer.Model.Param.OrganizationManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.IBusiness.OrganizationManage
{
    public interface IPositionBLL
    {
        #region 获取数据

        Task<TData<List<PositionEntity>>> GetList(PositionListParam param);

        Task<TData<List<PositionEntity>>> GetPageList(PositionListParam param, Pagination pagination);

        Task<TData<PositionEntity>> GetEntity(long id);

        #endregion

        #region 提交数据

        Task<TData<string>> SaveForm(PositionEntity entity);

        Task<TData> DeleteForm(string ids);

        #endregion
    }
}
