﻿using Queer.Entity;
using Queer.Model.Param.OrganizationManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.IBusiness.OrganizationManage
{
    public interface IUserBLL
    {
        #region 获取数据

        Task<TData<List<UserEntity>>> GetList(UserListParam param);

        Task<TData<List<UserEntity>>> GetPageList(UserListParam param, Pagination pagination);

        Task<TData<UserEntity>> GetEntity(long id);

        #endregion

        #region 提交数据

        Task<TData<string>> SaveForm(UserEntity entity);

        Task<TData> DeleteForm(string ids);

        Task<TData<UserEntity>> CheckLogin(string userName, string password);

        Task<TData<object>> UserPageLoad();

        Task GetUserBelong(UserEntity user);

        Task<TData> ChangeUserState(UserEntity entity);

        Task<TData<long>> ResetPassword(UserEntity entity);

        #endregion
    }
}
