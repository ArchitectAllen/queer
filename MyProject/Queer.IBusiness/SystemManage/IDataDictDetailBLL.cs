﻿using Queer.Entity;
using Queer.Model.Param.SystemManage;
using Queer.Model.Result.SystemManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.IBusiness.SystemManage
{
    public interface IDataDictDetailBLL
    {
        #region 获取数据

        Task<TData<List<DataDictDetailEntity>>> GetList(DataDictDetailListParam param);

        Task<TData<List<DataDictDetailEntity>>> GetPageList(DataDictDetailListParam param, Pagination pagination);

        Task<TData<DataDictDetailEntity>> GetEntity(long id);

        TData<List<DataDictDetailSelect>> GetListJsonForSelect(DataDictDetailListParam param);
        #endregion

        #region 提交数据

        Task<TData<string>> SaveForm(DataDictDetailEntity entity);

        Task<TData> DeleteForm(string ids);

        #endregion
    }
}