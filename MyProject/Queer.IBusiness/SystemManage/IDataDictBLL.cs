﻿using Queer.Entity;
using Queer.Model.Param.SystemManage;
using Queer.Util.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Queer.IBusiness.SystemManage
{
    public interface IDataDictBLL
    {
        #region 获取数据

        Task<TData<List<DataDictEntity>>> GetList(DataDictListParam param);

        Task<TData<List<DataDictEntity>>> GetPageList(DataDictListParam param, Pagination pagination);

        Task<TData<DataDictEntity>> GetEntity(long id);

        #endregion

        #region 提交数据

        Task<TData<string>> SaveForm(DataDictEntity entity);

        Task<TData> DeleteForm(string ids);

        #endregion
    }
}