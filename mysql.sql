/*
 Navicat Premium Data Transfer

 Source Server         : nekopara.top
 Source Server Type    : MySQL
 Source Server Version : 80024
 Source Host           : nekopara.top:3306
 Source Schema         : queer

 Target Server Type    : MySQL
 Target Server Version : 80024
 File Encoding         : 65001

 Date: 20/08/2021 09:07:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sysapiauthorize
-- ----------------------------
DROP TABLE IF EXISTS `sysapiauthorize`;
CREATE TABLE `sysapiauthorize`  (
  `Id` bigint NULL DEFAULT NULL,
  `Url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Authorize` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysapiauthorize
-- ----------------------------
INSERT INTO `sysapiauthorize` VALUES (46049625945280512, '/SystemManage/LogApi/GetPageListJson', 'system:logapi:search');

-- ----------------------------
-- Table structure for sysautojob
-- ----------------------------
DROP TABLE IF EXISTS `sysautojob`;
CREATE TABLE `sysautojob`  (
  `Id` bigint NULL DEFAULT NULL,
  `JobGroupName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `JobName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `JobStatus` int NULL DEFAULT NULL,
  `CronExpression` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `StartTime` timestamp NULL DEFAULT NULL,
  `EndTime` timestamp NULL DEFAULT NULL,
  `NextStartTime` timestamp NULL DEFAULT NULL,
  `Remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL,
  `Count` bigint NULL DEFAULT NULL,
  `StartCount` bigint NULL DEFAULT NULL,
  `StartNow` tinyint(1) NULL DEFAULT NULL,
  `NeedLog` tinyint(1) NULL DEFAULT NULL,
  `ExecuteType` tinyint(1) NULL DEFAULT NULL,
  `JobCode` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysautojob
-- ----------------------------

-- ----------------------------
-- Table structure for sysautojoblog
-- ----------------------------
DROP TABLE IF EXISTS `sysautojoblog`;
CREATE TABLE `sysautojoblog`  (
  `Id` bigint NULL DEFAULT NULL,
  `JobGroupName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `JobName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LogStatus` int NULL DEFAULT NULL,
  `Remark` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `UseTime` int NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysautojoblog
-- ----------------------------
INSERT INTO `sysautojoblog` VALUES (348753030478303232, '系统任务', '数据库备份', 1, '备份路径：E:\\GIT\\queer\\MyProject\\Queer.Web\\Database', 54, '2021-08-20 09:00:02');
INSERT INTO `sysautojoblog` VALUES (348753517466357760, '系统任务', '数据库备份', 1, '备份路径：E:\\GIT\\queer\\MyProject\\Queer.Web\\Database', 53, '2021-08-20 09:01:58');

-- ----------------------------
-- Table structure for syscodetemplet
-- ----------------------------
DROP TABLE IF EXISTS `syscodetemplet`;
CREATE TABLE `syscodetemplet`  (
  `Id` bigint NULL DEFAULT NULL,
  `Code` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `Remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Flag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of syscodetemplet
-- ----------------------------
INSERT INTO `syscodetemplet` VALUES (45368218067537920, 'using Furion.DatabaseAccessor;\r\nusing Microsoft.EntityFrameworkCore;\r\nusing Microsoft.EntityFrameworkCore.Metadata.Builders;\r\nusing Newtonsoft.Json;\r\nusing System;\r\nusing System.Collections.Generic;\r\nusing System.ComponentModel.DataAnnotations.Schema;\r\nusing {项目名称}.Util.Helper;\r\nusing {项目名称}.Data.BaseEntity;\r\n\r\nnamespace {项目名称}.Entity\r\n{\r\n{描述}\r\n    [Table(\"{表名称}\")]\r\n    public partial class {类名} : {继承实体}\r\n    {\r\n{字段}\r\n    }\r\n}', '实体类', 'Entity', 'csharp');
INSERT INTO `syscodetemplet` VALUES (45368219082559488, 'using System;\r\nusing System.Collections.Generic;\r\nusing System.Linq;\r\nusing System.Text;\r\nusing System.Threading.Tasks;\r\nusing {项目名称}.Util.Helper;\r\nusing Newtonsoft.Json;\r\n\r\nnamespace {项目名称}.Model.Param.{命名空间}\r\n{\r\n{描述}\r\n    public class {类名}\r\n    {\r\n{字段}\r\n    }\r\n}', '查询类', 'ListParam', 'csharp');
INSERT INTO `syscodetemplet` VALUES (45368219468435456, 'using Furion.DatabaseAccessor;\nusing Furion.DependencyInjection;\nusing Furion.LinqBuilder;\nusing System;\nusing System.Collections.Generic;\nusing System.Linq;\nusing System.Text;\nusing System.Threading.Tasks;\nusing Microsoft.EntityFrameworkCore;\nusing {项目名称}.Entity;\nusing {项目名称}.IService.{命名空间};\nusing {项目名称}.Model.Param.{命名空间};\nusing {项目名称}.Util;\nusing {项目名称}.Util.Helper;\nusing {项目名称}.Util.Model;\n\nnamespace {项目名称}.Service.{命名空间}\n{\n{描述}\n    public class {类名} : I{类名}, ITransient\n    {\n        private readonly IRepository<{实体类名}> _{驼峰实体类名}DB;\n\n        public {类名}(IRepository<{实体类名}> {驼峰实体类名}DB)\n        {\n            _{驼峰实体类名}DB = {驼峰实体类名}DB;\n        }\n\n        #region 获取数据\n\n        /// <summary>\n        /// 带条件查询所有\n        /// </summary>\n        public async Task<List<{实体类名}>> GetList({查询类名} param)\n        {\n            IQueryable<{实体类名}> query =  ListFilter(param);\n            var data = await query.ToListAsync();\n            return data;\n        }\n\n        /// <summary>\n        /// 分页查询\n        /// </summary>\n        public async Task<List<{实体类名}>> GetPageList({查询类名} param, Pagination pagination)\n        {\n            IQueryable<{实体类名}> query =  ListFilter(param);\n            var data = await query.OrderByDescending(a => a.Id).ToPagedListAsync(pagination.PageIndex, pagination.PageSize);\n\n            // 分页参数赋值\n            pagination.TotalCount = data.TotalCount;\n            return data.Items.ToList();\n        }\n\n        /// <summary>\n        /// 根据ID获取对象\n        /// </summary>\n        public async Task<{实体类名}> GetEntity(long id)\n        {\n            var data =  await _{驼峰实体类名}DB.AsQueryable(p => p.Id == id).FirstOrDefaultAsync();\n            return data;\n        }\n\n        /// <summary>\n        /// 查询多个ID主键数据\n        /// </summary>\n        public async Task<List<{实体类名}>> GetListByIds(string ids)\n        {\n            if (ids.IsNullOrEmpty())\n                throw new Exception(\"参数不合法！\");\n\n            var idArr = TextHelper.SplitToArray<long>(ids, \',\').ToList();\n            var data = await _{驼峰实体类名}DB.AsQueryable(a => idArr.Contains(a.Id.GetValueOrDefault())).ToListAsync();\n\n            return data;\n        }\n\n        #endregion\n\n        #region 提交数据\n\n        public async Task SaveForm({实体类名} entity)\n        {\n            if (entity.Id.IsNullOrZero())\n            {\n                await _{驼峰实体类名}DB.InsertNowAsync(entity);\n            }\n            else\n            {\n                await _{驼峰实体类名}DB.UpdateNowAsync(entity, ignoreNullValues: true);\n            }\n        }\n\n        public async Task DeleteForm(string ids)\n        {\n            if(string.IsNullOrWhiteSpace(ids))\n                throw new BusinessException(\"参数不合法！\");\n\n            var _ids = ids.Split(\",\");\n            await _{驼峰实体类名}DB.{删除方法}(_ids);\n        }\n\n        #endregion\n\n        #region 私有方法\n        private IQueryable<{实体类名}> ListFilter({查询类名} param)\n        {\n            IQueryable<{实体类名}> query = _{驼峰实体类名}DB.AsQueryable(false);\n            if (param != null)\n            {\n{查询条件}\n            }\n{是否伪删除}\n            return query;\n        }\n\n        #endregion\n    }\n}', '数据服务类', 'Service', 'csharp');
INSERT INTO `syscodetemplet` VALUES (45368219850117120, 'using Furion.DependencyInjection;\r\nusing System;\r\nusing System.Collections.Generic;\r\nusing System.Linq;\r\nusing System.Text;\r\nusing System.Threading.Tasks;\r\nusing {项目名称}.Entity;\r\nusing {项目名称}.IBusiness.{命名空间};\r\nusing {项目名称}.IService.{命名空间};\r\nusing {项目名称}.Model.Param.{命名空间};\r\nusing {项目名称}.Util;\r\nusing {项目名称}.Util.Model;\r\n\r\nnamespace {项目名称}.Business.{命名空间}\r\n{\r\n{描述}\r\n    public class {类名}: I{类名}, ITransient\r\n    {\r\n        private readonly I{数据服务类名} _{驼峰数据服务类名};\r\n        \r\n        public {类名}(I{数据服务类名} {驼峰数据服务类名})\r\n        {\r\n            _{驼峰数据服务类名} = {驼峰数据服务类名};\r\n        }\r\n\r\n        #region 获取数据\r\n\r\n        public async Task<TData<List<{实体类名}>>> GetList({查询类名} param)\r\n        {\r\n            TData<List<{实体类名}>> obj = new TData<List<{实体类名}>>();\r\n            obj.Data = await _{驼峰数据服务类名}.GetList(param);\r\n            obj.Tag = 1;\r\n            return obj;\r\n        }\r\n\r\n        public async Task<TData<List<{实体类名}>>> GetPageList({查询类名} param, Pagination pagination)\r\n        {\r\n            TData<List<{实体类名}>> obj = new TData<List<{实体类名}>>();\r\n            obj.Data = await _{驼峰数据服务类名}.GetPageList(param, pagination);\r\n            obj.Total = pagination.TotalCount;\r\n            obj.Tag = 1;\r\n            return obj;\r\n        }\r\n\r\n        public async Task<TData<{实体类名}>> GetEntity(long id)\r\n        {\r\n            TData<{实体类名}> obj = new TData<{实体类名}>();\r\n            obj.Data = await _{驼峰数据服务类名}.GetEntity(id);\r\n            obj.Tag = 1;\r\n            return obj;\r\n        }\r\n\r\n        #endregion\r\n\r\n        #region 提交数据\r\n\r\n        public async Task<TData<string>> SaveForm({实体类名} entity)\r\n        {\r\n            TData<string> obj = new TData<string>();\r\n            await _{驼峰数据服务类名}.SaveForm(entity);\r\n            obj.Data = entity.Id.ParseToString();\r\n            obj.Tag = 1;\r\n            return obj;\r\n        }\r\n\r\n        public async Task<TData> DeleteForm(string ids)\r\n        {\r\n            TData obj = new TData();\r\n            await _{驼峰数据服务类名}.DeleteForm(ids);\r\n            obj.Tag = 1;\r\n            return obj;\r\n        }\r\n\r\n        #endregion\r\n    }\r\n}', '业务类', 'BLL', 'csharp');
INSERT INTO `syscodetemplet` VALUES (45368220223410176, 'using System;\r\nusing System.Collections.Generic;\r\nusing System.Linq;\r\nusing System.Text;\r\nusing System.Threading.Tasks;\r\nusing {项目名称}.Entity;\r\nusing {项目名称}.Model.Param.{命名空间};\r\nusing {项目名称}.Util;\r\nusing {项目名称}.Util.Model;\r\n\r\nnamespace {项目名称}.IService.{命名空间}\r\n{\r\n{描述}\r\n    public interface I{类名}\r\n    {\r\n        #region 获取数据\r\n\r\n        Task<List<{实体类名}>> GetList({查询类名} param);\r\n\r\n        Task<List<{实体类名}>> GetPageList({查询类名} param, Pagination pagination);\r\n\r\n        Task<{实体类名}> GetEntity(long id);\r\n\r\n        #endregion\r\n\r\n        #region 提交数据\r\n\r\n        Task SaveForm({实体类名} entity);\r\n\r\n        Task DeleteForm(string ids);\r\n\r\n        #endregion\r\n\r\n        #region 私有方法\r\n\r\n        #endregion\r\n    }\r\n}', '数据服务接口', 'IService', 'csharp');
INSERT INTO `syscodetemplet` VALUES (45368220596703232, 'using System;\r\nusing System.Collections.Generic;\r\nusing System.Linq;\r\nusing System.Text;\r\nusing System.Threading.Tasks;\r\nusing {项目名称}.Entity;\r\nusing {项目名称}.Model.Param.{命名空间};\r\nusing {项目名称}.Util.Model;\r\n\r\nnamespace {项目名称}.IBusiness.{命名空间}\r\n{\r\n    public interface I{类名}\r\n    {\r\n        #region 获取数据\r\n\r\n        Task<TData<List<{实体类名}>>> GetList({查询类名} param);\r\n\r\n        Task<TData<List<{实体类名}>>> GetPageList({查询类名} param, Pagination pagination);\r\n\r\n        Task<TData<{实体类名}>> GetEntity(long id);\r\n\r\n        #endregion\r\n\r\n        #region 提交数据\r\n\r\n        Task<TData<string>> SaveForm({实体类名} entity);\r\n\r\n        Task<TData> DeleteForm(string ids);\r\n\r\n        #endregion\r\n    }\r\n}', '业务接口', 'IBLL', 'csharp');
INSERT INTO `syscodetemplet` VALUES (45368220965801984, 'using Microsoft.AspNetCore.Mvc;\nusing System;\nusing System.Collections.Generic;\nusing System.Linq;\nusing System.Threading.Tasks;\nusing {项目名称}.Entity;\nusing {项目名称}.IBusiness.{命名空间};\nusing {项目名称}.Model.Param.{命名空间};\nusing {项目名称}.Util.Model;\nusing {项目名称}.WebApi.Areas;\n\nnamespace {项目名称}.WebApi.Areas.{模块}.{命名空间}\n{\n{描述}\n    [Route(\"{命名空间}/[controller]\")]\n    public class {类名} : BaseAdminController\n    {\n        private readonly I{业务类名} _{驼峰业务类名};\n\n        public {类名}(I{业务类名} {驼峰业务类名})\n        {\n            _{驼峰业务类名} = {驼峰业务类名};\n        }\n\n        #region 获取数据\n\n        /// <summary>\n        /// 条件查询\n        /// </summary>\n        [HttpGet]\n        public async Task<TData<List<{实体类名}>>> GetListJson([FromQuery] {查询类名} param)\n        {\n            TData<List<{实体类名}>> obj = await _{驼峰业务类名}.GetList(param);\n            return obj;\n        }\n\n        /// <summary>\n        /// 条件查询-分页\n        /// </summary>\n        [HttpGet]\n        public async Task<TData<List<{实体类名}>>> GetPageListJson([FromQuery] {查询类名} param, [FromQuery] Pagination pagination)\n        {\n            TData<List<{实体类名}>> obj = await _{驼峰业务类名}.GetPageList(param, pagination);\n            return obj;\n        }\n\n        /// <summary>\n        /// 根据ID查询\n        /// </summary>\n        [HttpGet]\n        public async Task<TData<{实体类名}>> GetFormJson([FromQuery] long id)\n        {\n            TData<{实体类名}> obj = await _{驼峰业务类名}.GetEntity(id);\n            return obj;\n        }\n\n        #endregion\n\n        #region 提交数据\n\n        /// <summary>\n        /// 新增/修改 数据\n        /// </summary>\n        [HttpPost]\n        public async Task<TData<string>> SaveFormJson([FromBody] {实体类名} entity)\n        {\n            TData<string> obj = await _{驼峰业务类名}.SaveForm(entity);\n            return obj;\n        }\n\n        /// <summary>\n        /// 删除数据\n        /// </summary>\n        [HttpPost]\n        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)\n        {\n            TData obj = await _{驼峰业务类名}.DeleteForm(model.ids);\n            return obj;\n        }\n\n        #endregion\n    }\n}', '控制器', 'Controller', 'csharp');
INSERT INTO `syscodetemplet` VALUES (45368221347483648, '<!DOCTYPE html>\n<html>\n<head>\n    <meta charset=\"utf-8\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">\n    <link rel=\"stylesheet\" href=\"/assets/libs/layui/css/layui.css\" />\n    <link rel=\"stylesheet\" href=\"/assets/module/admin.css\">\n    <!--[if lt IE 9]>\n    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>\n    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>\n    <![endif]-->\n    <style>\n        {启用搜索}\n    </style>\n</head>\n<body>\n    <!-- 主体部分 -->\n    <div class=\"layui-fluid\">\n        <div class=\"layui-card\">\n            <div class=\"layui-card-body\">\n                <!-- 表格工具栏 -->\n                <form class=\"layui-form toolbar searchbar\">\n                    <div class=\"layui-form-item\">\n\n{查询条件}\n\n                        <div class=\"layui-inline\">\n                             \n                            <button class=\"layui-btn icon-btn\" lay-filter=\"TbSearch\" lay-submit perm-show=\\\"{搜索按钮权限}\\\">\n                                <i class=\"layui-icon\"></i>搜索\n                            </button>\n                        </div>\n                    </div>\n                </form>\n                <!-- 数据表格 -->\n                <table id=\"gridTable\" lay-filter=\"gridTable\"></table>\n            </div>\n        </div>\n    </div>\n\n    <!-- 表格操作列 -->\n    <script type=\"text/html\" id=\"toolbar\">\n        {新增按钮}\n        {修改按钮}\n        {删除按钮}\n    </script>\n\n    <!-- js部分 -->\n    <script src=\"/assets/libs/jquery/jquery-3.2.1.min.js\"></script>\n    <script src=\"/assets/libs/layui/layui.js\"></script>\n    <script src=\"/assets/js/utils.js\"></script>\n    <script src=\"/assets/js/main.js\"></script>\n    <script>\n\n        // 全局变量\n        var table;\n\n        layui.use([\'layer\', \'form\', \'table\', \'util\', \'admin\', \'setter\',\'tableX\'], function () {\n            var $ = layui.jquery;\n            var layer = layui.layer;\n            var form = layui.form;\n            table = layui.table;\n            var util = layui.util;\n            var admin = layui.admin;\n            var setter = layui.setter;\n\n            // 表格初始化\n            layuiTableSet();\n\n            /* 渲染表格 */\n            var insTb = table.render({\n                elem: \'#gridTable\',\n                url: setter.baseServer + \'/{命名空间}/{类名前缀}/GetPageListJson\',\n                page: true,\n                toolbar: \"#toolbar\",\n                cols: [[\n                    { type: \'checkbox\' },\n                    { type: \'numbers\' },\n{表格列}\n                ]]\n            });\n\n            /* 表格搜索 */\n            form.on(\'submit(TbSearch)\', function (data) {\n                insTb.reload({ where: data.field, page: { curr: 1 } });\n                return false;\n            });\n\n            /* 表格头工具栏点击事件 */\n            table.on(\'toolbar(gridTable)\', function (obj) {\n                if (obj.event === \'add\') { // 添加\n                    showEditModel();\n                } else if (obj.event === \'edit\') { // 修改\n                    var checkRows = table.checkStatus(\'gridTable\');\n                    if (checkRows.data.length === 0 || checkRows.data.length > 1) {\n                        layer.msg(\'请选择且只选择一条数据\', { icon: 2 });\n                        return;\n                    }\n                    showEditModel(checkRows.data[0]);\n                } else if (obj.event === \'del\') { // 删除\n                    var checkRows = table.checkStatus(\'gridTable\');\n                    if (checkRows.data.length === 0) {\n                        layer.msg(\'请选择要删除的数据\', { icon: 2 });\n                        return;\n                    }\n                    var ids = checkRows.data.map(function (d) {\n                        return d.Id;\n                    });\n                    doDel({ ids: ids });\n                } else if (obj.event == \'export\') {\n                    // 导出\n                    var cols = [[\n{表格列}\n                    ]];\n                    layuiTableExport(cols, setter.baseServer + \'/{命名空间}/{类名前缀}/GetListJson\')\n                }\n            });\n\n            /* 显示表单弹窗 */\n            function showEditModel(mData) {\n                var id = 0;\n                if (mData != undefined && mData != null) { id = mData.Id; }\n                admin.open({\n                    type: 2,\n                    area: [\'768px\', \'600px\'],\n                    title: (mData ? \'修改\' : \'添加\'),\n                    content: \'{驼峰类名前缀}Form.html?id=\' + id,\n                });\n            }\n\n            /* 删除 */\n            function doDel(obj) {\n                layer.confirm(\'确定要删除选中数据吗？\', {\n                    skin: \'layui-layer-admin\',\n                    shade: .1\n                }, function (i) {\n                    layer.close(i);\n                    var loadIndex = layer.load(2);\n                    admin.req(\'/{命名空间}/{类名前缀}/DeleteFormJson\',  obj.ids.join(\',\') , function (res) {\n                        layer.close(loadIndex);\n                        if (1 === res.Tag) {\n                            layer.msg(\"操作成功\", { icon: 1 });\n                            insTb.reload({ page: { curr: 1 } });\n                        } else {\n                            layer.msg(res.Message, { icon: 2 });\n                        }\n                    }, \'post\');\n                });\n            }\n\n        });\n    </script>\n</body>\n</html>', '列表页面', 'Index', 'html');
INSERT INTO `syscodetemplet` VALUES (45368221750136832, '<!DOCTYPE html>\n<html>\n<head>\n    <meta charset=\"utf-8\" />\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">\n    <link rel=\"stylesheet\" href=\"/assets/libs/layui/css/layui.css\" />\n    <link rel=\"stylesheet\" href=\"/assets/module/admin.css?v=318\">\n    <!--[if lt IE 9]>\n    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>\n    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>\n    <![endif]-->\n    <style>\n        .form-div {\n            background-color: white;\n        }\n    </style>\n</head>\n<body>\n    <div class=\"layui-row form-div\">\n        <form id=\"dataform\" lay-filter=\"dataform\" class=\"layui-form model-form\">\n\n{表单控件}\n          \n            <div class=\"layui-form-item text-right\">\n                <button class=\"layui-btn\" lay-filter=\"formSubmit\" lay-submit>保存</button>\n                <button class=\"layui-btn layui-btn-primary\" type=\"button\"  ew-event=\"closeDialog\">取消</button>\n            </div>\n        </form>\n    </div>\n\n    <!-- 初始加载动画 -->\n    <div class=\"page-loading\">\n        <div class=\"signal-loader\">\n            <span></span><span></span><span></span><span></span>\n        </div>\n    </div>\n\n    <!-- js部分 -->\n    <script src=\"/assets/libs/jquery/jquery-3.2.1.min.js\"></script>\n    <script src=\"/assets/libs/layui/layui.js\"></script>\n    <script src=\"/assets/js/utils.js\"></script>\n    <script src=\"/assets/js/main.js\"></script>\n    <script type=\"text/javascript\">\n        var id = getRequestData(\"id\");\n\n        layui.use([\'layer\', \'form\', \'admin\', \'laydate\', \'formX\'], function () {\n            var layer = layui.layer;\n            var form = layui.form;\n            var admin = layui.admin;\n            var formX = layui.formX;\n            var laydate = layui.laydate;\n\n            /* // 时间弹窗模板\n            laydate.render({\n                elem: \'#StartTime\'\n                , type: \'datetime\'\n                , trigger: \'click\'\n            });\n            */\n\n            // 初始化\n            if (id > 0) {\n                admin.req(\'/{命名空间}/{类名前缀}/GetFormJson\', { id: id }, function (res) {\n                    admin.removeLoading();\n                    if (res.Tag == 1) {\n                        form.val(\'dataform\', res.Data);\n                    }\n                }, \'get\');\n            } else {\n                admin.removeLoading();\n            }\n\n            // 表单提交\n            form.on(\'submit(formSubmit)\', function (data) {\n                data.field.Id = id;\n                admin.showLoading(\'body\', 3, \'.8\');\n\n                admin.req(\'/{命名空间}/{类名前缀}/SaveFormJson\', data.field, function (res) {\n                    admin.removeLoading(\'body\', true, true);\n                    if (res.Tag == 1) {\n                        parent.layer.msg(\'操作成功\', { icon: 1, time: 1500 });\n                        parent.table.reload(\'gridTable\');\n                        parent.layer.close(parent.layer.getFrameIndex(window.name));\n                    }\n                    else {\n                        layer.msg(res.Message, { icon: 2 });\n                    }\n                }, \'post\');\n\n                return false;\n            });\n\n        });\n    </script>\n</body>\n</html>', '表单页面', 'Form', 'html');

-- ----------------------------
-- Table structure for sysdatadict
-- ----------------------------
DROP TABLE IF EXISTS `sysdatadict`;
CREATE TABLE `sysdatadict`  (
  `Id` bigint NULL DEFAULT NULL,
  `DictType` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DictSort` int NULL DEFAULT NULL,
  `Remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysdatadict
-- ----------------------------
INSERT INTO `sysdatadict` VALUES (297048410391842816, 'NewsType', 1, '新闻类别', 16508640061130151, NULL, NULL, '2021-03-30 16:44:20', 16508640061130151, 0, '2021-03-30 16:44:20');

-- ----------------------------
-- Table structure for sysdatadictdetail
-- ----------------------------
DROP TABLE IF EXISTS `sysdatadictdetail`;
CREATE TABLE `sysdatadictdetail`  (
  `Id` bigint NULL DEFAULT NULL,
  `DictType` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DictSort` int NULL DEFAULT NULL,
  `DictKey` int NULL DEFAULT NULL,
  `DictValue` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ListClass` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DictStatus` int NULL DEFAULT NULL,
  `IsDefault` int NULL DEFAULT NULL,
  `Remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysdatadictdetail
-- ----------------------------
INSERT INTO `sysdatadictdetail` VALUES (297289317041049600, 'NewsType', 1, 1, '每日趣闻', NULL, 1, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sysdepartment
-- ----------------------------
DROP TABLE IF EXISTS `sysdepartment`;
CREATE TABLE `sysdepartment`  (
  `Id` bigint NULL DEFAULT NULL,
  `ParentId` bigint NULL DEFAULT NULL,
  `DepartmentName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Telephone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Fax` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `PrincipalId` bigint NULL DEFAULT NULL,
  `DepartmentSort` int NULL DEFAULT NULL,
  `Remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Enable` tinyint(1) NULL DEFAULT NULL,
  `PrincipalPhone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DepartmentCode` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL,
  `DeptType` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysdepartment
-- ----------------------------
INSERT INTO `sysdepartment` VALUES (16508640061124402, NULL, '主公司', '0551-6666666', '0551-8888888', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sysdepartment` VALUES (16508640061124403, 16508640061124402, '一号子公司', '111', '11', '11', 16508640061130151, 1, '', 1, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sysdepartment` VALUES (16508640061124404, 16508640061124402, '二号子公司', '1', '', '', 16508640061130150, 1, '', 1, '', '', NULL, NULL, NULL, '2021-08-16 15:47:43', 16508640061130151, NULL, NULL, 0);
INSERT INTO `sysdepartment` VALUES (16508640061124405, 16508640061124403, '研发部', '3', '', '', 16508640061130153, 1, '专注前端与后端结合的开发模式', 1, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sysdepartment` VALUES (16508640061124406, 16508640061124403, '测试部', '1', '', '', 0, 3, '', 1, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sysdepartment` VALUES (16508640061124407, 16508640061124403, '前端设计部', '1', '', '', 16508640061130150, 2, '', 1, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sysdepartment` VALUES (16508640061124408, 16508640061124403, '财务部', '0551-87654321', '0551-12345678', 'wangxue@yishasoft.com', 0, 15, '2', 1, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sysdepartment` VALUES (16508640061124409, 16508640061124403, '市场部', '111', '111', '11', 16508640061130150, 7, '', 1, '', '', NULL, NULL, NULL, '2021-03-31 10:12:47', 16508640061130151, NULL, NULL, 0);
INSERT INTO `sysdepartment` VALUES (347405950350528512, 16508640061124403, '测试', '13123123123', '', 'asdada', 16508640061130150, 10, NULL, NULL, NULL, NULL, 16508640061130151, NULL, NULL, '2021-08-16 15:50:54', 16508640061130151, 0, '2021-08-16 15:47:13', NULL);

-- ----------------------------
-- Table structure for syslogapi
-- ----------------------------
DROP TABLE IF EXISTS `syslogapi`;
CREATE TABLE `syslogapi`  (
  `Id` bigint NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL,
  `CreatorId` bigint NULL DEFAULT NULL,
  `LogStatus` int NULL DEFAULT NULL,
  `Remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ExecuteUrl` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ExecuteParam` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ExecuteResult` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ExecuteTime` int NULL DEFAULT NULL,
  `IpAddress` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of syslogapi
-- ----------------------------
INSERT INTO `syslogapi` VALUES (348752993966886912, '2021-08-20 08:59:53', NULL, 1, NULL, '/HomeManage/Home/GetPageListAndUserInfo', '', NULL, 459, '192.168.10.120', 16508640061130151, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `syslogapi` VALUES (348753013021609984, '2021-08-20 08:59:58', NULL, 1, NULL, '/OrganizationManage/User/UserPageLoad', '', NULL, 95, '192.168.10.120', 16508640061130151, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `syslogapi` VALUES (348753013160022016, '2021-08-20 08:59:58', NULL, 1, NULL, '/OrganizationManage/User/GetPageListJson', '?PageIndex=1&PageSize=15', NULL, 96, '192.168.10.120', 16508640061130151, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `syslogapi` VALUES (348753048400564224, '2021-08-20 09:00:06', NULL, 1, NULL, '/OrganizationManage/Department/GetListJson', '', NULL, 7, '192.168.10.120', 16508640061130151, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `syslogapi` VALUES (348753054566191104, '2021-08-20 09:00:08', NULL, 1, NULL, '/OrganizationManage/Position/GetPageListJson', '?PageIndex=1&PageSize=15', NULL, 14, '192.168.10.120', 16508640061130151, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `syslogapi` VALUES (348753085117501440, '2021-08-20 09:00:15', NULL, 1, NULL, '/SystemManage/Role/GetPageListJson', '?PageIndex=1&PageSize=15', NULL, 14, '192.168.10.120', 16508640061130151, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `syslogapi` VALUES (348753088959483904, '2021-08-20 09:00:16', NULL, 1, NULL, '/SystemManage/Menu/GetListJson', '', NULL, 4, '192.168.10.120', 16508640061130151, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `syslogapi` VALUES (348753185172623360, '2021-08-20 09:00:39', NULL, 1, NULL, '/SystemManage/Database/GetTablePageListJson', '?PageIndex=1&PageSize=15', NULL, 160, '192.168.10.120', 16508640061130151, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `syslogapi` VALUES (348753480887832576, '2021-08-20 09:01:49', NULL, 1, NULL, '/HomeManage/Home/GetPageListAndUserInfo', '', NULL, 466, '192.168.10.120', 16508640061130151, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `syslogapi` VALUES (348753564908130304, '2021-08-20 09:02:09', NULL, 1, NULL, '/OrganizationManage/Department/GetListJson', '', NULL, 41, '192.168.10.120', 16508640061130151, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sysloglogin
-- ----------------------------
DROP TABLE IF EXISTS `sysloglogin`;
CREATE TABLE `sysloglogin`  (
  `Id` bigint NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL,
  `CreatorId` bigint NULL DEFAULT NULL,
  `LogStatus` int NULL DEFAULT NULL,
  `IpAddress` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `IpLocation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `OS` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ExtraRemark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysloglogin
-- ----------------------------

-- ----------------------------
-- Table structure for sysmenu
-- ----------------------------
DROP TABLE IF EXISTS `sysmenu`;
CREATE TABLE `sysmenu`  (
  `Id` bigint NULL DEFAULT NULL,
  `ParentId` bigint NULL DEFAULT NULL,
  `MenuName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `MenuIcon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `MenuUrl` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `MenuTarget` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `MenuSort` int NULL DEFAULT NULL,
  `MenuType` int NULL DEFAULT NULL,
  `MenuStatus` int NULL DEFAULT NULL,
  `Authorize` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysmenu
-- ----------------------------
INSERT INTO `sysmenu` VALUES (16508640061130069, 0, '单位组织', 'layui-icon layui-icon-user', '', '', 100, 1, 1, '', '', NULL, NULL, NULL, '2021-04-12 14:22:56', 16508640061130151, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130070, 0, '系统管理', 'layui-icon layui-icon-set', '', '', 101, 1, 1, '', '', NULL, NULL, NULL, '2021-04-12 14:23:02', 16508640061130151, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130071, 0, '系统工具', 'layui-icon layui-icon-util', '', '', 102, 1, 1, '', '', NULL, NULL, NULL, '2021-04-12 14:23:07', 16508640061130151, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130072, 16508640061130069, '员工管理', 'layui-icon layui-icon-set', '#/organization/user/userIndex', '', 1, 2, 1, 'organization:user:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130073, 16508640061130069, '部门管理', '', '#/organization/department/departmentIndex', '', 2, 2, 1, 'organization:department:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130074, 16508640061130070, '系统角色', 'layui-icon ', '#/system/role/roleIndex', '', 1, 2, 1, 'system:role:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130075, 16508640061130070, '系统菜单', 'layui-icon ', '#/system/menu/menuIndex', '', 2, 2, 1, 'system:menu:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130076, 16508640061130070, '系统日志', 'layui-icon layui-icon-align-left', NULL, '', 10, 1, 1, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130077, 16508640061130070, '通用字典', NULL, '#/system/datadict/datadictIndex', '', 5, 2, 1, 'system:datadict:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130079, 16508640061130070, '数据表管理', NULL, '#/system/database/databaseIndex', '', 14, 2, 1, 'system:datatable:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130080, 16508640061130071, '代码生成', '', '#/tool/codeGenerator/codeGeneratorIndex', '', 1, 2, 1, 'tool:codegenerator:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130082, 16508640061130076, '登录日志', NULL, '#/system/logLogin/logLoginIndex', '', 1, 2, 1, 'system:loglogin:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130083, 16508640061130069, '职位管理', '', '#/organization/position/positionIndex', '', 3, 2, 1, 'organization:position:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130084, 16508640061130072, '员工查询', '', '', '', 1, 3, 1, 'organization:user:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130085, 16508640061130072, '员工新增', '', '', '', 2, 3, 1, 'organization:user:add', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130086, 16508640061130072, '员工修改', '', '', '', 3, 3, 1, 'organization:user:edit', '', NULL, NULL, NULL, '2021-08-19 13:00:06', 16508640061130151, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130087, 16508640061130072, '员工删除', '', '', '', 4, 3, 1, 'organization:user:delete', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130089, 16508640061130072, '重置密码', '', '', '', 6, 3, 1, 'organization:user:resetpwd', '', NULL, NULL, NULL, '2021-08-19 13:00:56', 16508640061130151, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130090, 16508640061130073, '部门查询', '', '', '', 1, 3, 1, 'organization:department:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130091, 16508640061130073, '部门新增', '', '', '', 2, 3, 1, 'organization:department:add', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130092, 16508640061130073, '部门修改', '', '', '', 3, 3, 1, 'organization:department:edit', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130093, 16508640061130073, '部门删除', '', '', '', 4, 3, 1, 'organization:department:delete', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130094, 16508640061130083, '职位查询', '', '', '', 1, 3, 1, 'organization:position:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130095, 16508640061130083, '职位新增', '', '', '', 2, 3, 1, 'organization:position:add', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130096, 16508640061130083, '职位修改', '', '', '', 3, 3, 1, 'organization:position:edit', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130097, 16508640061130083, '职位删除', '', '', '', 4, 3, 1, 'organization:position:delete', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130098, 16508640061130074, '角色查询', '', '', '', 1, 3, 1, 'system:role:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130099, 16508640061130074, '角色新增', '', '', '', 2, 3, 1, 'system:role:add', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130100, 16508640061130074, '角色修改', '', '', '', 3, 3, 1, 'system:role:edit', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130101, 16508640061130074, '角色删除', '', '', '', 4, 3, 1, 'system:role:delete', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130102, 16508640061130075, '菜单查询', '', '', '', 1, 3, 1, 'system:menu:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130103, 16508640061130075, '菜单新增', '', '', '', 2, 3, 1, 'system:menu:add', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130104, 16508640061130075, '菜单修改', '', '', '', 3, 3, 1, 'system:menu:edit', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130105, 16508640061130075, '菜单删除', '', '', '', 4, 3, 1, 'system:menu:delete', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130106, 16508640061130077, '字典查询', '', '', '', 1, 3, 1, 'system:datadict:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130107, 16508640061130077, '字典新增', '', '', '', 2, 3, 1, 'system:datadict:add', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130108, 16508640061130077, '字典修改', '', '', '', 3, 3, 1, 'system:datadict:edit', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130109, 16508640061130077, '字典删除', '', '', '', 4, 3, 1, 'system:datadict:delete', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130114, 16508640061130082, '登录日志查询', '', '', '', 1, 3, 1, 'system:loglogin:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130118, 16508640061130079, '数据表查询', '', '', '', 1, 3, 1, 'system:datatable:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130119, 16508640061130080, '代码生成', '', '', '', 2, 3, 1, 'tool:codegenerator:add', '', NULL, NULL, NULL, '2021-08-19 14:59:36', 16508640061130151, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130120, 16508640061130080, '代码生成查询', '', '', '', 1, 3, 1, 'tool:codegenerator:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130121, 16508640061130071, '服务器信息', NULL, '#/tool/server/serverIndex', '', 15, 2, 1, 'tool:server:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130122, 16508640061130070, '定时任务', NULL, '#/system/autoJob/autoJobIndex', '', 12, 2, 1, 'system:autojob:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130123, 16508640061130122, '定时任务查询', '', '', '', 1, 3, 1, 'system:autojob:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130124, 16508640061130122, '定时任务新增', '', '', '', 2, 3, 1, 'system:autojob:add', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130125, 16508640061130122, '定时任务修改', '', '', '', 3, 3, 1, 'system:autojob:edit', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130126, 16508640061130122, '定时任务删除', '', '', '', 4, 3, 1, 'system:autojob:delete', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130128, 16508640061130122, '定时任务日志查看', '', '', '', 5, 3, 1, 'system:autojob:logview', '', NULL, NULL, NULL, '2021-08-19 13:24:52', 16508640061130151, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130134, 16508640061130070, '系统api', 'layui-icon ', '#/api', '', 13, 2, 1, 'system:api:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130135, 16508640061130076, 'Api日志', NULL, '#/system/logApi/logApiIndex', '', 3, 2, 1, 'system:logapi:view', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130136, 16508640061130135, 'Api日志查询', '', '', '', 1, 3, 1, 'system:logapi:search', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (16508640061130137, 16508640061130135, 'Api日志详情', '', '', '', 2, 3, 1, 'system:logapi:detail', '', NULL, NULL, NULL, '2021-08-19 13:21:34', 16508640061130151, NULL, NULL);
INSERT INTO `sysmenu` VALUES (260867164880244736, 16508640061130070, '缓存管理', NULL, '#/system/cache/cacheIndex', NULL, 14, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (261771075572994048, 16508640061130070, 'MQTT主题订阅', NULL, '#/system/mqttTheme/mqttThemeIndex', NULL, 16, 2, 1, 'system:mqtttheme:view', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (261771076357328896, 261771075572994048, 'MQTT主题订阅搜索', NULL, NULL, NULL, 17, 3, 1, 'system:mqtttheme:search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (261771077133275136, 261771075572994048, 'MQTT主题订阅新增', NULL, NULL, NULL, 18, 3, 1, 'system:mqtttheme:add', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (261771077850501120, 261771075572994048, 'MQTT主题订阅修改', NULL, NULL, NULL, 19, 3, 1, 'system:mqtttheme:edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (261771078534172672, 261771075572994048, 'MQTT主题订阅删除', NULL, NULL, NULL, 20, 3, 1, 'system:mqtttheme:delete', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysmenu` VALUES (348453219346681856, 16508640061130074, '权限分配', '', '', NULL, 5, 3, 1, 'system:role:auths', NULL, 16508640061130151, NULL, NULL, '2021-08-19 13:08:41', 16508640061130151, 0, '2021-08-19 13:08:41');
INSERT INTO `sysmenu` VALUES (348454208413896704, 16508640061130075, '接口权限', '', '', NULL, 5, 3, 1, 'system:menu:btnauths', NULL, 16508640061130151, NULL, NULL, '2021-08-19 13:12:37', 16508640061130151, 0, '2021-08-19 13:12:37');
INSERT INTO `sysmenu` VALUES (348455016329121792, 16508640061130077, '字典值新增', '', '', NULL, 5, 3, 1, 'system:datadicdetail:add', NULL, 16508640061130151, NULL, NULL, '2021-08-19 13:18:22', 16508640061130151, 0, '2021-08-19 13:15:50');
INSERT INTO `sysmenu` VALUES (348455591577915392, 16508640061130077, '字典值修改', '', '', NULL, 6, 3, 1, 'system:datadicdetail:edit', NULL, 16508640061130151, NULL, NULL, '2021-08-19 13:18:31', 16508640061130151, 0, '2021-08-19 13:18:07');
INSERT INTO `sysmenu` VALUES (348455821002149888, 16508640061130077, '字典值删除', '', '', NULL, 7, 3, 1, 'system:datadictdetail:delete', NULL, 16508640061130151, NULL, NULL, '2021-08-19 13:19:02', 16508640061130151, 0, '2021-08-19 13:19:02');
INSERT INTO `sysmenu` VALUES (348472333524013056, 260867164880244736, '缓存查询', '', '', NULL, 1, 3, 1, 'system:cache:search', NULL, 16508640061130151, NULL, NULL, '2021-08-19 14:25:10', 16508640061130151, 0, '2021-08-19 14:24:39');
INSERT INTO `sysmenu` VALUES (348472428696965120, 260867164880244736, '缓存新增', '', '', NULL, 2, 3, 1, 'system:cache:add', NULL, 16508640061130151, NULL, NULL, '2021-08-19 14:25:20', 16508640061130151, 0, '2021-08-19 14:25:01');
INSERT INTO `sysmenu` VALUES (348472752279130112, 260867164880244736, '缓存修改', '', '', NULL, 3, 3, 1, 'system:cache:edit', NULL, 16508640061130151, NULL, NULL, '2021-08-19 14:26:18', 16508640061130151, 0, '2021-08-19 14:26:18');
INSERT INTO `sysmenu` VALUES (348472837276700672, 260867164880244736, '缓存删除', '', '', NULL, 4, 3, 1, 'system:cache:delete', NULL, 16508640061130151, NULL, NULL, '2021-08-19 14:26:39', 16508640061130151, 0, '2021-08-19 14:26:39');
INSERT INTO `sysmenu` VALUES (348481239650406400, 16508640061130080, '代码生成模板', '', '', NULL, 3, 3, 1, 'tool:codegenerator:templet', NULL, 16508640061130151, NULL, NULL, '2021-08-19 15:00:02', 16508640061130151, 0, '2021-08-19 15:00:02');

-- ----------------------------
-- Table structure for sysmenuauthorize
-- ----------------------------
DROP TABLE IF EXISTS `sysmenuauthorize`;
CREATE TABLE `sysmenuauthorize`  (
  `Id` bigint NULL DEFAULT NULL,
  `MenuId` bigint NULL DEFAULT NULL,
  `AuthorizeId` bigint NULL DEFAULT NULL,
  `AuthorizeType` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysmenuauthorize
-- ----------------------------
INSERT INTO `sysmenuauthorize` VALUES (46081509060382720, 16508640061130069, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081509404315648, 16508640061130072, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081509710499840, 16508640061130084, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081510016684032, 16508640061130085, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081510314479616, 16508640061130086, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081510620663808, 16508640061130087, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081510918459392, 16508640061130088, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081511224643584, 16508640061130089, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081511535022080, 16508640061130073, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081511837011968, 16508640061130090, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081512143196160, 16508640061130091, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081512453574656, 16508640061130092, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081512755564544, 16508640061130093, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081513057554432, 16508640061130083, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081513359544320, 16508640061130094, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081513665728512, 16508640061130095, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081513967718400, 16508640061130096, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46081514273902592, 16508640061130097, 253512862180315136, 1);
INSERT INTO `sysmenuauthorize` VALUES (46123151389757440, 16508640061130069, 16508640061130147, 1);
INSERT INTO `sysmenuauthorize` VALUES (46123151687553024, 16508640061130072, 16508640061130147, 1);
INSERT INTO `sysmenuauthorize` VALUES (46123151972765696, 16508640061130084, 16508640061130147, 1);
INSERT INTO `sysmenuauthorize` VALUES (46123152245395456, 16508640061130085, 16508640061130147, 1);
INSERT INTO `sysmenuauthorize` VALUES (46123152513830912, 16508640061130086, 16508640061130147, 1);
INSERT INTO `sysmenuauthorize` VALUES (46123152786460672, 16508640061130070, 16508640061130147, 1);
INSERT INTO `sysmenuauthorize` VALUES (46123153059090432, 16508640061130076, 16508640061130147, 1);
INSERT INTO `sysmenuauthorize` VALUES (46123153327525888, 16508640061130135, 16508640061130147, 1);
INSERT INTO `sysmenuauthorize` VALUES (46123153591767040, 16508640061130136, 16508640061130147, 1);
INSERT INTO `sysmenuauthorize` VALUES (46123153868591104, 16508640061130137, 16508640061130147, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241783234560, 16508640061130069, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241787428864, 16508640061130072, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241791623168, 16508640061130084, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241800011776, 16508640061130085, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241804206080, 16508640061130086, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241808400384, 16508640061130087, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241812594688, 16508640061130088, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241820983296, 16508640061130089, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241825177600, 16508640061130073, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241829371904, 16508640061130090, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241837760512, 16508640061130091, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241841954816, 16508640061130092, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241850343424, 16508640061130093, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241854537728, 16508640061130083, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241862926336, 16508640061130094, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241871314944, 16508640061130095, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241875509248, 16508640061130096, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241883897856, 16508640061130097, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241892286464, 16508640061130129, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241900675072, 16508640061130130, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241909063680, 16508640061130131, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241913257984, 16508640061130132, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (103195241921646592, 16508640061130133, 103195209390624768, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092413965762560, 16508640061130069, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092414716542976, 16508640061130072, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092414955618304, 16508640061130084, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092415169527808, 16508640061130085, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092415383437312, 16508640061130086, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092415593152512, 16508640061130087, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092415798673408, 16508640061130088, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092416008388608, 16508640061130089, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092416222298112, 16508640061130073, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092416427819008, 16508640061130090, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092416629145600, 16508640061130091, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092416834666496, 16508640061130092, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092417044381696, 16508640061130093, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092417249902592, 16508640061130083, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092417455423488, 16508640061130094, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092417669332992, 16508640061130095, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092417874853888, 16508640061130096, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092418088763392, 16508640061130097, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092418294284288, 16508640061130070, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092418503999488, 16508640061130074, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092418722103296, 16508640061130098, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092418927624192, 16508640061130099, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092419133145088, 16508640061130100, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092419351248896, 16508640061130101, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092419569352704, 16508640061130075, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092419783262208, 16508640061130102, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092419992977408, 16508640061130103, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092420219469824, 16508640061130104, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092420500488192, 16508640061130105, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092420714397696, 16508640061130076, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092420919918592, 16508640061130082, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092421125439488, 16508640061130114, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092421326766080, 16508640061130115, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092421528092672, 16508640061130135, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092421729419264, 16508640061130136, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092421939134464, 16508640061130137, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092422144655360, 16508640061130077, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092422350176256, 16508640061130106, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092422551502848, 16508640061130107, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092422761218048, 16508640061130108, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092422962544640, 16508640061130109, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092423163871232, 16508640061130078, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092423365197824, 16508640061130110, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092423566524416, 16508640061130111, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092423772045312, 16508640061130112, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092423977566208, 16508640061130113, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092424183087104, 16508640061130079, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092424388608000, 16508640061130118, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092424606711808, 16508640061130122, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092424829009920, 16508640061130123, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092425038725120, 16508640061130124, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092425252634624, 16508640061130125, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092425458155520, 16508640061130126, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092425655287808, 16508640061130127, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092425860808704, 16508640061130128, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092426062135296, 16508640061130134, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092426276044800, 16508640061130071, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092426485760000, 16508640061130080, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092426695475200, 16508640061130119, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092426900996096, 16508640061130120, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092427106516992, 16508640061130121, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092427307843584, 16508640061131999, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092427513364480, 16508640061132000, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (252092427727273984, 16508640061132001, 16508640061130146, 1);
INSERT INTO `sysmenuauthorize` VALUES (258565056382373888, 16508640061130069, 258201886530736128, 1);
INSERT INTO `sysmenuauthorize` VALUES (258565056466259968, 16508640061130072, 258201886530736128, 1);
INSERT INTO `sysmenuauthorize` VALUES (258565056483037184, 16508640061130087, 258201886530736128, 1);
INSERT INTO `sysmenuauthorize` VALUES (258565056491425792, 16508640061130088, 258201886530736128, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114459746304, 16508640061130069, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114522660864, 16508640061130072, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114535243776, 16508640061130084, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114552020992, 16508640061130085, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114568798208, 16508640061130086, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114581381120, 16508640061130087, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114598158336, 16508640061130088, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114610741248, 16508640061130089, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114627518464, 16508640061130073, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114640101376, 16508640061130090, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114652684288, 16508640061130091, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114669461504, 16508640061130092, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114686238720, 16508640061130093, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114703015936, 16508640061130083, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114715598848, 16508640061130094, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114732376064, 16508640061130095, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114744958976, 16508640061130096, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114761736192, 16508640061130097, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (286975114778513408, 286447559788990464, 286619174082449408, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872531423232, 16508640061130069, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872548200448, 16508640061130072, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872560783360, 16508640061130084, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872573366272, 16508640061130085, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872585949184, 16508640061130086, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872598532096, 16508640061130088, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872611115008, 16508640061130089, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872623697920, 16508640061130073, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872636280832, 16508640061130090, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872648863744, 16508640061130091, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872661446656, 16508640061130092, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872674029568, 16508640061130093, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872686612480, 16508640061130083, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872699195392, 16508640061130094, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872711778304, 16508640061130095, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872724361216, 16508640061130096, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872736944128, 16508640061130097, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872749527040, 16508640061130070, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872762109952, 16508640061130074, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872778887168, 16508640061130098, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872791470080, 16508640061130099, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872804052992, 16508640061130100, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872816635904, 16508640061130101, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872829218816, 16508640061130075, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872845996032, 16508640061130102, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872858578944, 16508640061130103, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872871161856, 16508640061130104, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872887939072, 16508640061130105, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872900521984, 16508640061130076, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872913104896, 16508640061130082, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872934076416, 16508640061130114, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872946659328, 16508640061130115, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872959242240, 16508640061130135, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872976019456, 16508640061130136, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682872988602368, 16508640061130137, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873005379584, 16508640061130077, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873017962496, 16508640061130106, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873034739712, 16508640061130107, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873047322624, 16508640061130108, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873064099840, 16508640061130109, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873080877056, 16508640061130078, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873093459968, 16508640061130110, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873110237184, 16508640061130111, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873122820096, 16508640061130112, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873139597312, 16508640061130113, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873156374528, 16508640061130079, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873168957440, 16508640061130118, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873185734656, 16508640061130122, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682873890377728, 16508640061130123, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682874037178368, 16508640061130124, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682874192367616, 16508640061130125, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682874326585344, 16508640061130126, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682874410471424, 16508640061130127, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682874464997376, 16508640061130128, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682874553077760, 16508640061130134, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682875299663872, 260867164880244736, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682875467436032, 261771075572994048, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682875551322112, 261771076357328896, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682875622625280, 261771077133275136, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682876423737344, 261771077850501120, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682876549566464, 261771078534172672, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682876616675328, 16508640061130071, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682876637646848, 16508640061130080, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682876658618368, 16508640061130119, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682876679589888, 16508640061130120, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682876700561408, 16508640061130121, 277766285150916608, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682888981483520, 16508640061130069, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682888998260736, 16508640061130072, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889010843648, 16508640061130084, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889027620864, 16508640061130085, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889040203776, 16508640061130086, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889056980992, 16508640061130088, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889069563904, 16508640061130089, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889086341120, 16508640061130073, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889098924032, 16508640061130090, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889115701248, 16508640061130091, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889128284160, 16508640061130092, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889145061376, 16508640061130093, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889157644288, 16508640061130083, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889174421504, 16508640061130094, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889191198720, 16508640061130095, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889207975936, 16508640061130096, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889220558848, 16508640061130097, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889237336064, 16508640061130070, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889249918976, 16508640061130076, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889266696192, 16508640061130082, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889283473408, 16508640061130114, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889296056320, 16508640061130115, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889312833536, 16508640061130135, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889325416448, 16508640061130136, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (289682889342193664, 16508640061130137, 287185473351192576, 1);
INSERT INTO `sysmenuauthorize` VALUES (290558951165005824, 16508640061130069, 289429554030710784, 1);
INSERT INTO `sysmenuauthorize` VALUES (290558951248891904, 16508640061130072, 289429554030710784, 1);
INSERT INTO `sysmenuauthorize` VALUES (290558951265669120, 16508640061130086, 289429554030710784, 1);
INSERT INTO `sysmenuauthorize` VALUES (290558951282446336, 16508640061130070, 289429554030710784, 1);
INSERT INTO `sysmenuauthorize` VALUES (290558951295029248, 16508640061130074, 289429554030710784, 1);
INSERT INTO `sysmenuauthorize` VALUES (290558951307612160, 16508640061130100, 289429554030710784, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899490996260864, 16508640061130069, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491013038080, 16508640061130072, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491029815296, 16508640061130084, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491042398208, 16508640061130085, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491054981120, 16508640061130086, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491067564032, 16508640061130087, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491080146944, 16508640061130088, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491096924160, 16508640061130089, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491109507072, 16508640061130073, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491122089984, 16508640061130090, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491151450112, 16508640061130091, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491164033024, 16508640061130092, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491176615936, 16508640061130093, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491193393152, 16508640061130083, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491205976064, 16508640061130094, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491277279232, 16508640061130095, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491298250752, 16508640061130096, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (293899491310833664, 16508640061130097, 292826047173365760, 1);
INSERT INTO `sysmenuauthorize` VALUES (295136574889267200, 16508640061130069, 289712787762974720, 1);
INSERT INTO `sysmenuauthorize` VALUES (295136575036067840, 16508640061130072, 289712787762974720, 1);
INSERT INTO `sysmenuauthorize` VALUES (295136575065427968, 16508640061130084, 289712787762974720, 1);
INSERT INTO `sysmenuauthorize` VALUES (295136575090593792, 16508640061130086, 289712787762974720, 1);
INSERT INTO `sysmenuauthorize` VALUES (295136575119953920, 16508640061130088, 289712787762974720, 1);
INSERT INTO `sysmenuauthorize` VALUES (296695508938788864, 16508640061130069, 296691626556788736, 1);
INSERT INTO `sysmenuauthorize` VALUES (296695508963954688, 16508640061130072, 296691626556788736, 1);
INSERT INTO `sysmenuauthorize` VALUES (296695508993314816, 16508640061130084, 296691626556788736, 1);
INSERT INTO `sysmenuauthorize` VALUES (296695509018480640, 16508640061130070, 296691626556788736, 1);
INSERT INTO `sysmenuauthorize` VALUES (296695509047840768, 16508640061130075, 296691626556788736, 1);
INSERT INTO `sysmenuauthorize` VALUES (296695509073006592, 16508640061130103, 296691626556788736, 1);
INSERT INTO `sysmenuauthorize` VALUES (296695509102366720, 16508640061130104, 296691626556788736, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471093665792, 16508640061130069, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471114637312, 16508640061130072, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471127220224, 16508640061130084, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471139803136, 16508640061130085, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471152386048, 16508640061130086, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471169163264, 16508640061130087, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471181746176, 16508640061130089, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471194329088, 16508640061130073, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471206912000, 16508640061130090, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471223689216, 16508640061130091, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471236272128, 16508640061130092, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471253049344, 16508640061130093, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471265632256, 16508640061130083, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471278215168, 16508640061130094, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471294992384, 16508640061130095, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471311769600, 16508640061130096, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471324352512, 16508640061130097, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471336935424, 16508640061130070, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471353712640, 16508640061130074, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471366295552, 16508640061130098, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471383072768, 16508640061130099, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471399849984, 16508640061130100, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471412432896, 16508640061130101, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471429210112, 16508640061130075, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471445987328, 16508640061130102, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471462764544, 16508640061130103, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471479541760, 16508640061130104, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471496318976, 16508640061130105, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471513096192, 16508640061130077, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471529873408, 16508640061130106, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471546650624, 16508640061130107, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471563427840, 16508640061130108, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471580205056, 16508640061130109, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471596982272, 16508640061130076, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471613759488, 16508640061130082, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471630536704, 16508640061130114, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471651508224, 16508640061130115, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471672479744, 16508640061130135, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471689256960, 16508640061130136, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471710228480, 16508640061130137, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471727005696, 16508640061130122, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471747977216, 16508640061130123, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471764754432, 16508640061130124, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471785725952, 16508640061130125, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471802503168, 16508640061130126, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471819280384, 16508640061130127, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471840251904, 16508640061130128, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471857029120, 16508640061130134, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471878000640, 260867164880244736, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471894777856, 16508640061130079, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471911555072, 16508640061130118, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471932526592, 261771075572994048, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471953498112, 261771076357328896, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471970275328, 261771077133275136, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451471991246848, 261771077850501120, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451472012218368, 261771078534172672, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451472028995584, 16508640061130071, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451472049967104, 16508640061130080, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451472070938624, 16508640061130120, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451472091910144, 16508640061130119, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348451472108687360, 16508640061130121, 258606017569361920, 1);
INSERT INTO `sysmenuauthorize` VALUES (348508668603404288, 16508640061130069, 348410673211904000, 1);
INSERT INTO `sysmenuauthorize` VALUES (348508668611792896, 16508640061130072, 348410673211904000, 1);
INSERT INTO `sysmenuauthorize` VALUES (348508668620181504, 16508640061130084, 348410673211904000, 1);
INSERT INTO `sysmenuauthorize` VALUES (348508668632764416, 16508640061130085, 348410673211904000, 1);
INSERT INTO `sysmenuauthorize` VALUES (348508668641153024, 16508640061130086, 348410673211904000, 1);
INSERT INTO `sysmenuauthorize` VALUES (348508668649541632, 16508640061130087, 348410673211904000, 1);
INSERT INTO `sysmenuauthorize` VALUES (348508668662124544, 16508640061130089, 348410673211904000, 1);

-- ----------------------------
-- Table structure for sysmqttmsg
-- ----------------------------
DROP TABLE IF EXISTS `sysmqttmsg`;
CREATE TABLE `sysmqttmsg`  (
  `Id` bigint NULL DEFAULT NULL,
  `ThemeName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Msg` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `Time` timestamp NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysmqttmsg
-- ----------------------------

-- ----------------------------
-- Table structure for sysmqtttheme
-- ----------------------------
DROP TABLE IF EXISTS `sysmqtttheme`;
CREATE TABLE `sysmqtttheme`  (
  `Id` bigint NULL DEFAULT NULL,
  `ThemeName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `IsSubscribe` tinyint(1) NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysmqtttheme
-- ----------------------------

-- ----------------------------
-- Table structure for sysposition
-- ----------------------------
DROP TABLE IF EXISTS `sysposition`;
CREATE TABLE `sysposition`  (
  `Id` bigint NULL DEFAULT NULL,
  `PositionName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `PositionSort` int NULL DEFAULT NULL,
  `Remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysposition
-- ----------------------------
INSERT INTO `sysposition` VALUES (16508640061130141, '项目经理', 3, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysposition` VALUES (16508640061130142, '测试经理', 4, '111', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysposition` VALUES (347390645939212288, '测试', 123, '123', 16508640061130151, NULL, NULL, '2021-08-16 14:46:24', 16508640061130151, 0, '2021-08-16 14:46:24');

-- ----------------------------
-- Table structure for sysrole
-- ----------------------------
DROP TABLE IF EXISTS `sysrole`;
CREATE TABLE `sysrole`  (
  `Id` bigint NULL DEFAULT NULL,
  `RoleName` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RoleSort` int NULL DEFAULT NULL,
  `RoleStatus` int NULL DEFAULT NULL,
  `Remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysrole
-- ----------------------------
INSERT INTO `sysrole` VALUES (258606017569361920, '管理', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (348410673211904000, '测试角色A', 2, 1, NULL, 16508640061130151, NULL, NULL, '2021-08-19 10:19:38', 16508640061130151, 0, '2021-08-19 10:19:38');

-- ----------------------------
-- Table structure for systest
-- ----------------------------
DROP TABLE IF EXISTS `systest`;
CREATE TABLE `systest`  (
  `Id` bigint NULL DEFAULT NULL,
  `MyName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `SearchName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of systest
-- ----------------------------

-- ----------------------------
-- Table structure for sysuser
-- ----------------------------
DROP TABLE IF EXISTS `sysuser`;
CREATE TABLE `sysuser`  (
  `Id` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL,
  `UserName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Salt` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `RealName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DepartmentId` bigint NULL DEFAULT NULL,
  `Gender` int NULL DEFAULT NULL,
  `Birthday` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Portrait` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Mobile` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `QQ` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `WeChat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LoginCount` int NULL DEFAULT NULL,
  `UserStatus` int NULL DEFAULT NULL,
  `IsSystem` int NULL DEFAULT NULL,
  `IsOnline` int NULL DEFAULT NULL,
  `FirstVisit` timestamp NULL DEFAULT NULL,
  `PreviousVisit` timestamp NULL DEFAULT NULL,
  `LastVisit` timestamp NULL DEFAULT NULL,
  `Remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Picture` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ApiToken` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL,
  `Tags` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LoginUserType` int NULL DEFAULT NULL,
  `LoginUserId` bigint NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysuser
-- ----------------------------
INSERT INTO `sysuser` VALUES (16508640061130150, 0, 'wangxue', 'ae51fabc8d8217a7242a58bb76ca62c1', '10184', '王雪', 16508640061124408, 1, '1993-10-06', '', '', '15612345678', '', '', 1, 1, 0, 1, '2019-09-21 10:48:03', '2019-09-21 10:48:03', '2019-09-21 10:48:03', '', NULL, NULL, NULL, NULL, NULL, '2021-08-19 17:31:16', 16508640061130151, NULL, NULL, NULL, NULL);
INSERT INTO `sysuser` VALUES (16508640061130151, 0, 'admin', 'b4e24a62fb8b61b0af033b0a30b20df1', '98648', '管理员', 16508640061124402, 1, '2019-01-01', '/Resource/Portrait/2021/05/19/1621388220355.jpg', 'admin@163.com', '15766666666', '810938177', 's810938177', 46785, 1, 1, 1, '2018-12-12 16:00:10', '2021-08-20 08:32:57', '2021-08-20 08:32:57', '123123', NULL, 'ff93ff74ac084221a036aa9db505ed43', NULL, NULL, NULL, '2021-08-20 08:32:58', 0, NULL, '超管', NULL, NULL);
INSERT INTO `sysuser` VALUES (347386923079176192, 0, 'ces', '27d8cd7ace1823f0fabb026c76328427', '45689', '123', 16508640061124403, 1, '2021-08-16', NULL, NULL, '123', NULL, NULL, 9, 1, NULL, 1, NULL, '2021-08-19 17:23:31', '2021-08-19 17:23:31', '', NULL, '7ebf0c65c5064ef4b0ee311d1e33b901', 16508640061130151, NULL, NULL, '2021-08-19 17:23:31', 0, '2021-08-16 14:31:37', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sysuserbelong
-- ----------------------------
DROP TABLE IF EXISTS `sysuserbelong`;
CREATE TABLE `sysuserbelong`  (
  `Id` bigint NULL DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT NULL,
  `UserId` bigint NULL DEFAULT NULL,
  `BelongId` bigint NULL DEFAULT NULL,
  `BelongType` int NULL DEFAULT NULL,
  `CreateUserId` bigint NULL DEFAULT NULL,
  `DeleteTime` timestamp NULL DEFAULT NULL,
  `DeleteUserId` bigint NULL DEFAULT NULL,
  `LastModifyTime` timestamp NULL DEFAULT NULL,
  `LastModifyUserId` bigint NULL DEFAULT NULL,
  `IsDelete` tinyint(1) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysuserbelong
-- ----------------------------
INSERT INTO `sysuserbelong` VALUES (259363197956395008, '2020-12-16 16:56:45', 259345304376053760, 258606017569361920, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (259363198329688064, '2020-12-16 16:56:45', 259345304376053760, 16508640061130140, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (259363198677815296, '2020-12-16 16:56:45', 259345304376053760, 16508640061130141, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (259426846125330432, '2020-12-16 21:09:40', 16508640061130151, 258606017569361920, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (259426846565732352, '2020-12-16 21:09:40', 16508640061130151, 16508640061130140, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (259432564475826176, '2020-12-16 21:32:23', 16508640061130152, 258606017569361920, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (259432564895256576, '2020-12-16 21:32:23', 16508640061130152, 16508640061130141, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (259432565268549632, '2020-12-16 21:32:24', 16508640061130152, 16508640061130140, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (283461040950218752, '2021-02-21 04:52:59', 16508640061130150, 258606017569361920, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (283461041021521920, '2021-02-21 04:52:59', 16508640061130150, 277766285150916608, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (283461041080242176, '2021-02-21 04:52:59', 16508640061130150, 16508640061130140, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (286770553761566720, '2021-03-02 08:03:48', 286770553635737600, 277766285150916608, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (286770553816092672, '2021-03-02 08:03:48', 286770553635737600, 286619174082449408, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (286770553853841408, '2021-03-02 08:03:48', 286770553635737600, 16508640061130139, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (286770553883201536, '2021-03-02 08:03:48', 286770553635737600, 16508640061130140, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (286770553912561664, '2021-03-02 08:03:48', 286770553635737600, 16508640061130141, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (286770553941921792, '2021-03-02 08:03:48', 286770553635737600, 16508640061130143, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (286770553971281920, '2021-03-02 08:03:48', 286770553635737600, 16508640061130142, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (286770554000642048, '2021-03-02 08:03:48', 286770553635737600, 16508640061130144, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (286770554025807872, '2021-03-02 08:03:48', 286770553635737600, 16508640061130145, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (287262428117340160, '2021-03-03 16:38:20', 287262427932790784, 258606017569361920, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (287262428171866112, '2021-03-03 16:38:20', 287262427932790784, 277766285150916608, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (287262428205420544, '2021-03-03 16:38:20', 287262427932790784, 16508640061130139, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (297014870753153024, '2021-03-30 14:31:03', 297014869461307392, 258606017569361920, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (297014871122251776, '2021-03-30 14:31:04', 297014869461307392, 16508640061130142, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (297016001441042432, '2021-03-30 14:35:33', 297016000946114560, 258606017569361920, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (297016001784975360, '2021-03-30 14:35:33', 297016000946114560, 16508640061130141, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sysuserbelong` VALUES (297376920335552512, '2021-03-31 14:29:43', 297308940515938304, 258606017569361920, 2, 16508640061130151, NULL, NULL, '2021-03-31 14:29:43', 16508640061130151, 0);
INSERT INTO `sysuserbelong` VALUES (297376920574627840, '2021-03-31 14:29:43', 297308940515938304, 16508640061130142, 1, 16508640061130151, NULL, NULL, '2021-03-31 14:29:43', 16508640061130151, 0);
INSERT INTO `sysuserbelong` VALUES (297377008873115648, '2021-03-31 14:30:04', 297377008554348544, 258606017569361920, 2, 16508640061130151, NULL, NULL, '2021-03-31 14:30:04', 16508640061130151, 0);
INSERT INTO `sysuserbelong` VALUES (297377009095413760, '2021-03-31 14:30:04', 297377008554348544, 16508640061130141, 1, 16508640061130151, NULL, NULL, '2021-03-31 14:30:04', 16508640061130151, 0);
INSERT INTO `sysuserbelong` VALUES (313344285162344448, '2021-05-14 15:58:19', 313344284575141888, 258606017569361920, 2, 16508640061130151, NULL, NULL, '2021-05-14 15:58:19', 16508640061130151, 0);
INSERT INTO `sysuserbelong` VALUES (313344285439168512, '2021-05-14 15:58:19', 313344284575141888, 16508640061130142, 1, 16508640061130151, NULL, NULL, '2021-05-14 15:58:19', 16508640061130151, 0);
INSERT INTO `sysuserbelong` VALUES (347383742655893504, '2021-08-16 14:18:58', 347383742500704256, 258606017569361920, 2, 16508640061130151, NULL, NULL, '2021-08-16 14:18:58', 16508640061130151, 0);
INSERT INTO `sysuserbelong` VALUES (347383742676865024, '2021-08-16 14:18:58', 347383742500704256, 16508640061130142, 1, 16508640061130151, NULL, NULL, '2021-08-16 14:18:58', 16508640061130151, 0);
INSERT INTO `sysuserbelong` VALUES (348412980892798976, '2021-08-19 10:28:48', 347386923079176192, 348410673211904000, 2, 16508640061130151, NULL, NULL, '2021-08-19 10:28:48', 16508640061130151, 0);
INSERT INTO `sysuserbelong` VALUES (348412980917964800, '2021-08-19 10:28:48', 347386923079176192, 16508640061130142, 1, 16508640061130151, NULL, NULL, '2021-08-19 10:28:48', 16508640061130151, 0);

SET FOREIGN_KEY_CHECKS = 1;
