 **准备条件：1.服务器需要安装宝塔面板（不然得自己配置nginx）** 

### 1. 安装.net5 环境

    -> 参考文档：https://docs.microsoft.com/zh-cn/dotnet/core/install/linux-centos

    - CentOS 8
        安装 SDK
        `sudo dnf install dotnet-sdk-5.0`
        安装运行时
        `sudo dnf install aspnetcore-runtime-5.0`

    - CentOS 7
        将 Microsoft 包签名密钥添加到受信任密钥列表，并添加 Microsoft 包存储库
        `sudo rpm -Uvh https://packages.microsoft.com/config/centos/7/packages-microsoft-prod.rpm`
        安装 SDK
        `sudo yum install dotnet-sdk-5.0`
        安装运行时
        `sudo yum install aspnetcore-runtime-5.0`

### 2. 因使用‘System.Drawing’，所以需要执行如下命令先

       `yum install libgdiplus-devel`

### 3. 使用宝塔面板，创建一个站点，并配置反向代理

![输入图片说明](https://images.gitee.com/uploads/images/2021/0602/165439_8c88cb28_5074431.png "截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0602/165531_04c3dc44_5074431.png "截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0602/165646_ea90bb32_5074431.png "截图.png")

     **特别注意 localhost:5000 这个端口号，.net core 项目启动默认是5000，这里如果不填5000的话 得记一下** 
    
### 4. 发布项目，选择‘文件系统’，将发布好的文件上传到宝塔面板创建项目时的文件夹中。
    - 通过cd命令进入该文件夹
    - 执行启动命令 `dotnet Smbxfdbz.dll `
        端口号自定义命令：`dotnet Smbxfdbz.dll --urls="http://*:5005"` 5005即你自定义的端口号，要和NGINX的反向代理一致


(验证码报错，需安装)
sudo yum install https://download-ib01.fedoraproject.org/pub/epel/8/Everything/x86_64/Packages/l/libgdiplus-6.0.4-3.el8.x86_64.rpm
        