# queer

#### 介绍

1. 基于.net5 和 furion 的后台管理系统（前后端分离+代码生成）
2. 项目尚未完善，仅用于自己的学习，不喜互喷
3. 已完成功能：权限验证、用户管理、角色管理、代码生成、菜单管理、日志记录、代码模板、缓存管理、MQTT通讯、定时任务
4. 注意前端采用基于layui的第三方框架，easyweb，项目中已移除部分该框架的核心代码，具体请至 [Easyweb官网](https://eleadmin.com/index#guide) 查看
5. 前端功能截图（具体可进入下方提供的demo查看演示）：

    - 项目 DEMO 预览 <a href='http://demo.nekopara.top'> 点此跳转登陆 </a>，账号 admin 密码 123456
    - 项目 DEMO Swagger地址 <a href='http://demo.nekopara.top/api'> 点此跳转Swagger </a>，这个项目重点是后端啊....

![系统菜单](https://gitee.com/songzhidan/queer/raw/master/MdImages/3.png "系统菜单")
![API日志](https://gitee.com/songzhidan/queer/raw/master/MdImages/4.png "API日志")
![Swagger文档](https://gitee.com/songzhidan/queer/raw/master/MdImages/5.png "Swagger文档")
![数据表管理](https://gitee.com/songzhidan/queer/raw/master/MdImages/6.png "数据表管理")
![代码生成](https://gitee.com/songzhidan/queer/raw/master/MdImages/7.png "代码生成")
![服务器信息](https://gitee.com/songzhidan/queer/raw/master/MdImages/8.png "服务器信息")
![缓存管理](https://gitee.com/songzhidan/queer/raw/master/MdImages/9.png "缓存管理")

#### 软件架构

1. 项目结构、工具类参考： <a href='https://gitee.com/liukuo362573/YiShaAdmin' > YiShaAdmin </a>
2. 底层使用框架： <a href='https://monksoul.gitee.io/furion/docs/'> furion </a>
3. 项目结构：

    - 01 Framework 基础设施层
        - CodeGenerator   代码生成模块
        - Util            工具类模块

    - 02 DataAccess 数据库核心层
        - Data            数据库仓储定义

    - 03 Entity 实体层
        - Entity          数据库实体类
        - Enum            枚举类型
        - Model           DTO模型
    
    - 04 Service 数据服务层
        - Cache           缓存服务
        - EntityListener  实体类表监听服务
        - IService        数据库服务接口
        - Service         数据库服务

    - 05 Business 业务层
        - Business        业务层
        - IBusiness       业务接口

    - 06 Web
        - Web             WEB服务
        - WebApi          API接口服务


4. 主要依赖包：

    - ** _Furion_ **                                    
        - furion核心包，详见<a href='https://monksoul.gitee.io/furion/docs/'> 文档 </a>
    - ** _Furion.Extras.Authentication.JwtBearer_ **    
        - furion的JWT鉴权包，详见<a href='https://monksoul.gitee.io/furion/docs/'> 文档 </a>
    - ** _Microsoft.EntityFrameworkCore.SqlServer_ **   
        - SqlServer操作依赖包
    - ** _IP2Region_ **                                 
        - IP定位工具，MyProject.Web 下DB文件夹是依赖，部署项目时需要拷贝
    - ** _NLog.Web.AspNetCore_ **                       
        - NLOG日志工具
    - ** _StackExchange.Redis_ **                       
        - redis缓存工具
    - ** _System.Drawing.Common_ **                     
        - 图像工具，这里仅用于验证码的生成
    - ** _Microsoft.AspNetCore.Mvc.NewtonsoftJson_ **   
        - json序列化使用


2. 项目仅供参考.... 待完善功能和数据结构