/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : SQL Server
 Source Server Version : 15002000
 Source Host           : localhost:1433
 Source Catalog        : queer
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 15002000
 File Encoding         : 65001

 Date: 15/10/2021 09:59:45
*/


-- ----------------------------
-- Table structure for SysApiAuthorize
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysApiAuthorize]') AND type IN ('U'))
	DROP TABLE [dbo].[SysApiAuthorize]
GO

CREATE TABLE [dbo].[SysApiAuthorize] (
  [Id] bigint  NOT NULL,
  [Url] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [Authorize] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SysApiAuthorize] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N' ',
'SCHEMA', N'dbo',
'TABLE', N'SysApiAuthorize',
'COLUMN', N'Id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'请求接口',
'SCHEMA', N'dbo',
'TABLE', N'SysApiAuthorize',
'COLUMN', N'Url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'菜单权限标识',
'SCHEMA', N'dbo',
'TABLE', N'SysApiAuthorize',
'COLUMN', N'Authorize'
GO

EXEC sp_addextendedproperty
'MS_Description', N'接口权限',
'SCHEMA', N'dbo',
'TABLE', N'SysApiAuthorize'
GO


-- ----------------------------
-- Records of SysApiAuthorize
-- ----------------------------
INSERT INTO [dbo].[SysApiAuthorize] ([Id], [Url], [Authorize]) VALUES (N'46049625945280512', N'/SystemManage/LogApi/GetPageListJson', N'system:logapi:search')
GO


-- ----------------------------
-- Table structure for SysAutoJob
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysAutoJob]') AND type IN ('U'))
	DROP TABLE [dbo].[SysAutoJob]
GO

CREATE TABLE [dbo].[SysAutoJob] (
  [Id] bigint  NOT NULL,
  [JobGroupName] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [JobName] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [JobStatus] int  NOT NULL,
  [CronExpression] varchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [StartTime] datetime  NOT NULL,
  [EndTime] datetime  NULL,
  [NextStartTime] datetime  NOT NULL,
  [Remark] nvarchar(500) COLLATE Chinese_PRC_CI_AS  NULL,
  [CreateUserId] bigint  NULL,
  [DeleteTime] datetime  NULL,
  [DeleteUserId] bigint  NULL,
  [LastModifyTime] datetime  NULL,
  [LastModifyUserId] bigint  NULL,
  [IsDelete] bit  NOT NULL,
  [CreateTime] datetime  NULL,
  [Count] bigint  NULL,
  [StartCount] bigint  NULL,
  [StartNow] bit  NULL,
  [NeedLog] bit  NULL,
  [ExecuteType] bit  NULL,
  [JobCode] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SysAutoJob] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'任务组名称',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'JobGroupName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'任务名称',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'JobName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'任务状态(0禁用 1启用)',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'JobStatus'
GO

EXEC sp_addextendedproperty
'MS_Description', N'cron表达式',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'CronExpression'
GO

EXEC sp_addextendedproperty
'MS_Description', N'运行开始时间',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'StartTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'运行结束时间',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'EndTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'下次执行时间',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'NextStartTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'CreateUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除时间',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'DeleteTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'DeleteUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改时间',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'LastModifyTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'LastModifyUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'已执行次数',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'Count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'项目启动后的执行次数',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'StartCount'
GO

EXEC sp_addextendedproperty
'MS_Description', N'项目启动后是否立即执行',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'StartNow'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否需要记录日志',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'NeedLog'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否需要串行（等待任务结束后运行）',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'ExecuteType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'任务配置代码（按需）',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJob',
'COLUMN', N'JobCode'
GO


-- ----------------------------
-- Records of SysAutoJob
-- ----------------------------
INSERT INTO [dbo].[SysAutoJob] ([Id], [JobGroupName], [JobName], [JobStatus], [CronExpression], [StartTime], [EndTime], [NextStartTime], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [Count], [StartCount], [StartNow], [NeedLog], [ExecuteType], [JobCode]) VALUES (N'348046766836289536', N'系统任务', N'数据库备份', N'1', N'0 0 0 * * ?', N'2021-08-18 10:12:40.000', N'9999-12-31 00:00:00.000', N'2021-08-18 10:12:40.000', N'数据库备份', N'16508640061130151', NULL, NULL, N'2021-10-15 09:45:42.550', N'0', N'0', N'2021-08-18 10:13:35.610', N'121', N'1', N'1', N'1', N'0', N'')
GO

INSERT INTO [dbo].[SysAutoJob] ([Id], [JobGroupName], [JobName], [JobStatus], [CronExpression], [StartTime], [EndTime], [NextStartTime], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [Count], [StartCount], [StartNow], [NeedLog], [ExecuteType], [JobCode]) VALUES (N'348095683238039552', N'测试', N'测试八位字符可以', N'1', N'0/5 * * * * ?', N'2021-08-18 13:27:32.000', N'9999-12-31 00:00:00.000', N'2021-08-18 13:34:15.000', N'', N'16508640061130151', N'2021-08-18 13:40:32.373', N'16508640061130151', N'2021-08-18 13:40:32.373', N'16508640061130151', N'1', N'2021-08-18 13:27:58.190', N'26', N'2', N'0', N'1', N'1', N'')
GO

INSERT INTO [dbo].[SysAutoJob] ([Id], [JobGroupName], [JobName], [JobStatus], [CronExpression], [StartTime], [EndTime], [NextStartTime], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [Count], [StartCount], [StartNow], [NeedLog], [ExecuteType], [JobCode]) VALUES (N'348102127807107072', N'测试', N'测试', N'1', N'0/5 * * * * ?', N'2021-08-18 13:57:20.000', N'2022-08-18 13:57:23.000', N'2021-08-18 13:57:45.000', N'', N'16508640061130151', N'2021-08-18 13:57:58.533', N'16508640061130151', N'2021-08-18 13:57:58.533', N'16508640061130151', N'1', N'2021-08-18 13:53:34.693', N'2', N'11', N'1', N'1', N'0', N'')
GO


-- ----------------------------
-- Table structure for SysAutoJobLog
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysAutoJobLog]') AND type IN ('U'))
	DROP TABLE [dbo].[SysAutoJobLog]
GO

CREATE TABLE [dbo].[SysAutoJobLog] (
  [Id] bigint  NOT NULL,
  [JobGroupName] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [JobName] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [LogStatus] int  NOT NULL,
  [Remark] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [UseTime] int  NULL,
  [CreateTime] datetime  NULL
)
GO

ALTER TABLE [dbo].[SysAutoJobLog] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'任务组名称',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJobLog',
'COLUMN', N'JobGroupName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'任务名称',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJobLog',
'COLUMN', N'JobName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'执行状态(0失败 1成功)',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJobLog',
'COLUMN', N'LogStatus'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJobLog',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'执行耗时',
'SCHEMA', N'dbo',
'TABLE', N'SysAutoJobLog',
'COLUMN', N'UseTime'
GO


-- ----------------------------
-- Records of SysAutoJobLog
-- ----------------------------
INSERT INTO [dbo].[SysAutoJobLog] ([Id], [JobGroupName], [JobName], [LogStatus], [Remark], [UseTime], [CreateTime]) VALUES (N'369048796593655808', N'系统任务', N'数据库备份', N'1', N'备份路径：E:\GIT\queer\MyProject\Queer.Web\Database', N'79', NULL)
GO

INSERT INTO [dbo].[SysAutoJobLog] ([Id], [JobGroupName], [JobName], [LogStatus], [Remark], [UseTime], [CreateTime]) VALUES (N'369049192145883136', N'系统任务', N'数据库备份', N'1', N'备份路径：E:\GIT\queer\MyProject\Queer.Web\Database', N'67', NULL)
GO

INSERT INTO [dbo].[SysAutoJobLog] ([Id], [JobGroupName], [JobName], [LogStatus], [Remark], [UseTime], [CreateTime]) VALUES (N'369049345028263936', N'系统任务', N'数据库备份', N'1', N'备份路径：E:\GIT\queer\MyProject\Queer.Web\Database', N'53', NULL)
GO

INSERT INTO [dbo].[SysAutoJobLog] ([Id], [JobGroupName], [JobName], [LogStatus], [Remark], [UseTime], [CreateTime]) VALUES (N'369049647643103232', N'系统任务', N'数据库备份', N'1', N'备份路径：E:\GIT\queer\MyProject\Queer.Web\Database', N'87', NULL)
GO

INSERT INTO [dbo].[SysAutoJobLog] ([Id], [JobGroupName], [JobName], [LogStatus], [Remark], [UseTime], [CreateTime]) VALUES (N'369049790215884800', N'系统任务', N'数据库备份', N'1', N'备份路径：E:\GIT\queer\MyProject\Queer.Web\Database', N'45', NULL)
GO

INSERT INTO [dbo].[SysAutoJobLog] ([Id], [JobGroupName], [JobName], [LogStatus], [Remark], [UseTime], [CreateTime]) VALUES (N'369052934236803072', N'系统任务', N'数据库备份', N'1', N'备份路径：E:\GIT\queer\MyProject\Queer.Web\Database', N'50', N'2021-10-15 09:24:36.200')
GO

INSERT INTO [dbo].[SysAutoJobLog] ([Id], [JobGroupName], [JobName], [LogStatus], [Remark], [UseTime], [CreateTime]) VALUES (N'369055785528856576', N'系统任务', N'数据库备份', N'1', N'备份路径：E:\GIT\queer\MyProject\Queer.Web\Database', N'52', N'2021-10-15 09:35:56.003')
GO

INSERT INTO [dbo].[SysAutoJobLog] ([Id], [JobGroupName], [JobName], [LogStatus], [Remark], [UseTime], [CreateTime]) VALUES (N'369057357931483136', N'系统任务', N'数据库备份', N'1', N'备份路径：E:\GIT\queer\MyProject\Queer.Web\Database', N'47', N'2021-10-15 09:42:10.893')
GO

INSERT INTO [dbo].[SysAutoJobLog] ([Id], [JobGroupName], [JobName], [LogStatus], [Remark], [UseTime], [CreateTime]) VALUES (N'369058024888733696', N'系统任务', N'数据库备份', N'1', N'备份路径：E:\GIT\queer\MyProject\Queer.Web\Database', N'64', N'2021-10-15 09:44:49.903')
GO

INSERT INTO [dbo].[SysAutoJobLog] ([Id], [JobGroupName], [JobName], [LogStatus], [Remark], [UseTime], [CreateTime]) VALUES (N'369058246276681728', N'系统任务', N'数据库备份', N'1', N'备份路径：E:\GIT\queer\MyProject\Queer.Web\Database', N'51', N'2021-10-15 09:45:42.687')
GO


-- ----------------------------
-- Table structure for SysCodeTemplet
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysCodeTemplet]') AND type IN ('U'))
	DROP TABLE [dbo].[SysCodeTemplet]
GO

CREATE TABLE [dbo].[SysCodeTemplet] (
  [Id] bigint  NOT NULL,
  [Code] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [Remark] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [Flag] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [Type] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SysCodeTemplet] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'代码',
'SCHEMA', N'dbo',
'TABLE', N'SysCodeTemplet',
'COLUMN', N'Code'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysCodeTemplet',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'标识',
'SCHEMA', N'dbo',
'TABLE', N'SysCodeTemplet',
'COLUMN', N'Flag'
GO

EXEC sp_addextendedproperty
'MS_Description', N'代码类型（cs java html 等）',
'SCHEMA', N'dbo',
'TABLE', N'SysCodeTemplet',
'COLUMN', N'Type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'代码生成模板',
'SCHEMA', N'dbo',
'TABLE', N'SysCodeTemplet'
GO


-- ----------------------------
-- Records of SysCodeTemplet
-- ----------------------------
INSERT INTO [dbo].[SysCodeTemplet] ([Id], [Code], [Remark], [Flag], [Type]) VALUES (N'45368218067537920', N'using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using {项目名称}.Util.Helper;
using {项目名称}.Data.BaseEntity;

namespace {项目名称}.Entity
{
{描述}
    [Table("{表名称}")]
    public partial class {类名} : {继承实体}
    {
{字段}
    }
}', N'实体类', N'Entity', N'csharp')
GO

INSERT INTO [dbo].[SysCodeTemplet] ([Id], [Code], [Remark], [Flag], [Type]) VALUES (N'45368219082559488', N'using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using {项目名称}.Util.Helper;
using Newtonsoft.Json;

namespace {项目名称}.Model.Param.{命名空间}
{
{描述}
    public class {类名}
    {
{字段}
    }
}', N'查询类', N'ListParam', N'csharp')
GO

INSERT INTO [dbo].[SysCodeTemplet] ([Id], [Code], [Remark], [Flag], [Type]) VALUES (N'45368219468435456', N'using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.LinqBuilder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using {项目名称}.Entity;
using {项目名称}.IService.{命名空间};
using {项目名称}.Model.Param.{命名空间};
using {项目名称}.Util;
using {项目名称}.Util.Helper;
using {项目名称}.Util.Model;

namespace {项目名称}.Service.{命名空间}
{
{描述}
    public class {类名} : I{类名}, ITransient
    {
        private readonly IRepository<{实体类名}> _{驼峰实体类名}DB;

        public {类名}(IRepository<{实体类名}> {驼峰实体类名}DB)
        {
            _{驼峰实体类名}DB = {驼峰实体类名}DB;
        }

        #region 获取数据

        /// <summary>
        /// 带条件查询所有
        /// </summary>
        public async Task<List<{实体类名}>> GetList({查询类名} param)
        {
            IQueryable<{实体类名}> query =  ListFilter(param);
            var data = await query.ToListAsync();
            return data;
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        public async Task<List<{实体类名}>> GetPageList({查询类名} param, Pagination pagination)
        {
            IQueryable<{实体类名}> query =  ListFilter(param);
            var data = await query.OrderByDescending(a => a.Id).ToPagedListAsync(pagination.PageIndex, pagination.PageSize);

            // 分页参数赋值
            pagination.TotalCount = data.TotalCount;
            return data.Items.ToList();
        }

        /// <summary>
        /// 根据ID获取对象
        /// </summary>
        public async Task<{实体类名}> GetEntity(long id)
        {
            var data =  await _{驼峰实体类名}DB.AsQueryable(p => p.Id == id).FirstOrDefaultAsync();
            return data;
        }

        /// <summary>
        /// 查询多个ID主键数据
        /// </summary>
        public async Task<List<{实体类名}>> GetListByIds(string ids)
        {
            if (ids.IsNullOrEmpty())
                throw new Exception("参数不合法！");

            var idArr = TextHelper.SplitToArray<long>(ids, '','').ToList();
            var data = await _{驼峰实体类名}DB.AsQueryable(a => idArr.Contains(a.Id.GetValueOrDefault())).ToListAsync();

            return data;
        }

        #endregion

        #region 提交数据

        public async Task SaveForm({实体类名} entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await _{驼峰实体类名}DB.InsertNowAsync(entity);
            }
            else
            {
                await _{驼峰实体类名}DB.UpdateNowAsync(entity, ignoreNullValues: true);
            }
        }

        public async Task DeleteForm(string ids)
        {
            if(string.IsNullOrWhiteSpace(ids))
                throw new BusinessException("参数不合法！");

            var _ids = ids.Split(",");
            await _{驼峰实体类名}DB.{删除方法}(_ids);
        }

        #endregion

        #region 私有方法
        private IQueryable<{实体类名}> ListFilter({查询类名} param)
        {
            IQueryable<{实体类名}> query = _{驼峰实体类名}DB.AsQueryable(false);
            if (param != null)
            {
{查询条件}
            }
{是否伪删除}
            return query;
        }

        #endregion
    }
}', N'数据服务类', N'Service', N'csharp')
GO

INSERT INTO [dbo].[SysCodeTemplet] ([Id], [Code], [Remark], [Flag], [Type]) VALUES (N'45368219850117120', N'using Furion.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using {项目名称}.Entity;
using {项目名称}.IBusiness.{命名空间};
using {项目名称}.IService.{命名空间};
using {项目名称}.Model.Param.{命名空间};
using {项目名称}.Util;
using {项目名称}.Util.Model;

namespace {项目名称}.Business.{命名空间}
{
{描述}
    public class {类名}: I{类名}, ITransient
    {
        private readonly I{数据服务类名} _{驼峰数据服务类名};
        
        public {类名}(I{数据服务类名} {驼峰数据服务类名})
        {
            _{驼峰数据服务类名} = {驼峰数据服务类名};
        }

        #region 获取数据

        public async Task<TData<List<{实体类名}>>> GetList({查询类名} param)
        {
            TData<List<{实体类名}>> obj = new TData<List<{实体类名}>>();
            obj.Data = await _{驼峰数据服务类名}.GetList(param);
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<{实体类名}>>> GetPageList({查询类名} param, Pagination pagination)
        {
            TData<List<{实体类名}>> obj = new TData<List<{实体类名}>>();
            obj.Data = await _{驼峰数据服务类名}.GetPageList(param, pagination);
            obj.Total = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<{实体类名}>> GetEntity(long id)
        {
            TData<{实体类名}> obj = new TData<{实体类名}>();
            obj.Data = await _{驼峰数据服务类名}.GetEntity(id);
            obj.Tag = 1;
            return obj;
        }

        #endregion

        #region 提交数据

        public async Task<TData<string>> SaveForm({实体类名} entity)
        {
            TData<string> obj = new TData<string>();
            await _{驼峰数据服务类名}.SaveForm(entity);
            obj.Data = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await _{驼峰数据服务类名}.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }

        #endregion
    }
}', N'业务类', N'BLL', N'csharp')
GO

INSERT INTO [dbo].[SysCodeTemplet] ([Id], [Code], [Remark], [Flag], [Type]) VALUES (N'45368220223410176', N'using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using {项目名称}.Entity;
using {项目名称}.Model.Param.{命名空间};
using {项目名称}.Util;
using {项目名称}.Util.Model;

namespace {项目名称}.IService.{命名空间}
{
{描述}
    public interface I{类名}
    {
        #region 获取数据

        Task<List<{实体类名}>> GetList({查询类名} param);

        Task<List<{实体类名}>> GetPageList({查询类名} param, Pagination pagination);

        Task<{实体类名}> GetEntity(long id);

        #endregion

        #region 提交数据

        Task SaveForm({实体类名} entity);

        Task DeleteForm(string ids);

        #endregion

        #region 私有方法

        #endregion
    }
}', N'数据服务接口', N'IService', N'csharp')
GO

INSERT INTO [dbo].[SysCodeTemplet] ([Id], [Code], [Remark], [Flag], [Type]) VALUES (N'45368220596703232', N'using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using {项目名称}.Entity;
using {项目名称}.Model.Param.{命名空间};
using {项目名称}.Util.Model;

namespace {项目名称}.IBusiness.{命名空间}
{
    public interface I{类名}
    {
        #region 获取数据

        Task<TData<List<{实体类名}>>> GetList({查询类名} param);

        Task<TData<List<{实体类名}>>> GetPageList({查询类名} param, Pagination pagination);

        Task<TData<{实体类名}>> GetEntity(long id);

        #endregion

        #region 提交数据

        Task<TData<string>> SaveForm({实体类名} entity);

        Task<TData> DeleteForm(string ids);

        #endregion
    }
}', N'业务接口', N'IBLL', N'csharp')
GO

INSERT INTO [dbo].[SysCodeTemplet] ([Id], [Code], [Remark], [Flag], [Type]) VALUES (N'45368220965801984', N'using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using {项目名称}.Entity;
using {项目名称}.IBusiness.{命名空间};
using {项目名称}.Model.Param.{命名空间};
using {项目名称}.Util.Model;
using {项目名称}.WebApi.Areas;

namespace {项目名称}.WebApi.Areas.{模块}.{命名空间}
{
{描述}
    [Route("{命名空间}/[controller]")]
    public class {类名} : BaseAdminController
    {
        private readonly I{业务类名} _{驼峰业务类名};

        public {类名}(I{业务类名} {驼峰业务类名})
        {
            _{驼峰业务类名} = {驼峰业务类名};
        }

        #region 获取数据

        /// <summary>
        /// 条件查询
        /// </summary>
        [HttpGet]
        public async Task<TData<List<{实体类名}>>> GetListJson([FromQuery] {查询类名} param)
        {
            TData<List<{实体类名}>> obj = await _{驼峰业务类名}.GetList(param);
            return obj;
        }

        /// <summary>
        /// 条件查询-分页
        /// </summary>
        [HttpGet]
        public async Task<TData<List<{实体类名}>>> GetPageListJson([FromQuery] {查询类名} param, [FromQuery] Pagination pagination)
        {
            TData<List<{实体类名}>> obj = await _{驼峰业务类名}.GetPageList(param, pagination);
            return obj;
        }

        /// <summary>
        /// 根据ID查询
        /// </summary>
        [HttpGet]
        public async Task<TData<{实体类名}>> GetFormJson([FromQuery] long id)
        {
            TData<{实体类名}> obj = await _{驼峰业务类名}.GetEntity(id);
            return obj;
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 新增/修改 数据
        /// </summary>
        [HttpPost]
        public async Task<TData<string>> SaveFormJson([FromBody] {实体类名} entity)
        {
            TData<string> obj = await _{驼峰业务类名}.SaveForm(entity);
            return obj;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        [HttpPost]
        public async Task<TData> DeleteFormJson([FromBody] ParamModel model)
        {
            TData obj = await _{驼峰业务类名}.DeleteForm(model.ids);
            return obj;
        }

        #endregion
    }
}', N'控制器', N'Controller', N'csharp')
GO

INSERT INTO [dbo].[SysCodeTemplet] ([Id], [Code], [Remark], [Flag], [Type]) VALUES (N'45368221347483648', N'<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/assets/libs/layui/css/layui.css" />
    <link rel="stylesheet" href="/assets/module/admin.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        {启用搜索}
    </style>
</head>
<body>
    <!-- 主体部分 -->
    <div class="layui-fluid">
        <div class="layui-card">
            <div class="layui-card-body">
                <!-- 表格工具栏 -->
                <form class="layui-form toolbar searchbar">
                    <div class="layui-form-item">

{查询条件}

                        <div class="layui-inline">
                             
                            <button class="layui-btn icon-btn" lay-filter="TbSearch" lay-submit perm-show=\"{搜索按钮权限}\">
                                <i class="layui-icon"></i>搜索
                            </button>
                        </div>
                    </div>
                </form>
                <!-- 数据表格 -->
                <table id="gridTable" lay-filter="gridTable"></table>
            </div>
        </div>
    </div>

    <!-- 表格操作列 -->
    <script type="text/html" id="toolbar">
        {新增按钮}
        {修改按钮}
        {删除按钮}
    </script>

    <!-- js部分 -->
    <script src="/assets/libs/jquery/jquery-3.2.1.min.js"></script>
    <script src="/assets/libs/layui/layui.js"></script>
    <script src="/assets/js/utils.js"></script>
    <script src="/assets/js/main.js"></script>
    <script>

        // 全局变量
        var table;

        layui.use([''layer'', ''form'', ''table'', ''util'', ''admin'', ''setter'',''tableX''], function () {
            var $ = layui.jquery;
            var layer = layui.layer;
            var form = layui.form;
            table = layui.table;
            var util = layui.util;
            var admin = layui.admin;
            var setter = layui.setter;

            // 表格初始化
            layuiTableSet();

            /* 渲染表格 */
            var insTb = table.render({
                elem: ''#gridTable'',
                url: setter.baseServer + ''/{命名空间}/{类名前缀}/GetPageListJson'',
                page: true,
                toolbar: "#toolbar",
                cols: [[
                    { type: ''checkbox'' },
                    { type: ''numbers'' },
{表格列}
                ]]
            });

            /* 表格搜索 */
            form.on(''submit(TbSearch)'', function (data) {
                insTb.reload({ where: data.field, page: { curr: 1 } });
                return false;
            });

            /* 表格头工具栏点击事件 */
            table.on(''toolbar(gridTable)'', function (obj) {
                if (obj.event === ''add'') { // 添加
                    showEditModel();
                } else if (obj.event === ''edit'') { // 修改
                    var checkRows = table.checkStatus(''gridTable'');
                    if (checkRows.data.length === 0 || checkRows.data.length > 1) {
                        layer.msg(''请选择且只选择一条数据'', { icon: 2 });
                        return;
                    }
                    showEditModel(checkRows.data[0]);
                } else if (obj.event === ''del'') { // 删除
                    var checkRows = table.checkStatus(''gridTable'');
                    if (checkRows.data.length === 0) {
                        layer.msg(''请选择要删除的数据'', { icon: 2 });
                        return;
                    }
                    var ids = checkRows.data.map(function (d) {
                        return d.Id;
                    });
                    doDel({ ids: ids });
                } else if (obj.event == ''export'') {
                    // 导出
                    var cols = [[
{表格列}
                    ]];
                    layuiTableExport(cols, setter.baseServer + ''/{命名空间}/{类名前缀}/GetListJson'')
                }
            });

            /* 显示表单弹窗 */
            function showEditModel(mData) {
                var id = 0;
                if (mData != undefined && mData != null) { id = mData.Id; }
                admin.open({
                    type: 2,
                    area: [''768px'', ''600px''],
                    title: (mData ? ''修改'' : ''添加''),
                    content: ''{驼峰类名前缀}Form.html?id='' + id,
                });
            }

            /* 删除 */
            function doDel(obj) {
                layer.confirm(''确定要删除选中数据吗？'', {
                    skin: ''layui-layer-admin'',
                    shade: .1
                }, function (i) {
                    layer.close(i);
                    var loadIndex = layer.load(2);
                    admin.req(''/{命名空间}/{类名前缀}/DeleteFormJson'',  obj.ids.join('','') , function (res) {
                        layer.close(loadIndex);
                        if (1 === res.Tag) {
                            layer.msg("操作成功", { icon: 1 });
                            insTb.reload({ page: { curr: 1 } });
                        } else {
                            layer.msg(res.Message, { icon: 2 });
                        }
                    }, ''post'');
                });
            }

        });
    </script>
</body>
</html>', N'列表页面', N'Index', N'html')
GO

INSERT INTO [dbo].[SysCodeTemplet] ([Id], [Code], [Remark], [Flag], [Type]) VALUES (N'45368221750136832', N'<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/assets/libs/layui/css/layui.css" />
    <link rel="stylesheet" href="/assets/module/admin.css?v=318">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .form-div {
            background-color: white;
        }
    </style>
</head>
<body>
    <div class="layui-row form-div">
        <form id="dataform" lay-filter="dataform" class="layui-form model-form">

{表单控件}
          
            <div class="layui-form-item text-right">
                <button class="layui-btn" lay-filter="formSubmit" lay-submit>保存</button>
                <button class="layui-btn layui-btn-primary" type="button"  ew-event="closeDialog">取消</button>
            </div>
        </form>
    </div>

    <!-- 初始加载动画 -->
    <div class="page-loading">
        <div class="signal-loader">
            <span></span><span></span><span></span><span></span>
        </div>
    </div>

    <!-- js部分 -->
    <script src="/assets/libs/jquery/jquery-3.2.1.min.js"></script>
    <script src="/assets/libs/layui/layui.js"></script>
    <script src="/assets/js/utils.js"></script>
    <script src="/assets/js/main.js"></script>
    <script type="text/javascript">
        var id = getRequestData("id");

        layui.use([''layer'', ''form'', ''admin'', ''laydate'', ''formX''], function () {
            var layer = layui.layer;
            var form = layui.form;
            var admin = layui.admin;
            var formX = layui.formX;
            var laydate = layui.laydate;

            /* // 时间弹窗模板
            laydate.render({
                elem: ''#StartTime''
                , type: ''datetime''
                , trigger: ''click''
            });
            */

            // 初始化
            if (id > 0) {
                admin.req(''/{命名空间}/{类名前缀}/GetFormJson'', { id: id }, function (res) {
                    admin.removeLoading();
                    if (res.Tag == 1) {
                        form.val(''dataform'', res.Data);
                    }
                }, ''get'');
            } else {
                admin.removeLoading();
            }

            // 表单提交
            form.on(''submit(formSubmit)'', function (data) {
                data.field.Id = id;
                admin.showLoading(''body'', 3, ''.8'');

                admin.req(''/{命名空间}/{类名前缀}/SaveFormJson'', data.field, function (res) {
                    admin.removeLoading(''body'', true, true);
                    if (res.Tag == 1) {
                        parent.layer.msg(''操作成功'', { icon: 1, time: 1500 });
                        parent.table.reload(''gridTable'');
                        parent.layer.close(parent.layer.getFrameIndex(window.name));
                    }
                    else {
                        layer.msg(res.Message, { icon: 2 });
                    }
                }, ''post'');

                return false;
            });

        });
    </script>
</body>
</html>', N'表单页面', N'Form', N'html')
GO


-- ----------------------------
-- Table structure for SysDataDict
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysDataDict]') AND type IN ('U'))
	DROP TABLE [dbo].[SysDataDict]
GO

CREATE TABLE [dbo].[SysDataDict] (
  [Id] bigint  NOT NULL,
  [DictType] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [DictSort] int  NULL,
  [Remark] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [CreateUserId] bigint  NULL,
  [DeleteTime] datetime  NULL,
  [DeleteUserId] bigint  NULL,
  [LastModifyTime] datetime  NULL,
  [LastModifyUserId] bigint  NULL,
  [IsDelete] bit  NULL,
  [CreateTime] datetime  NULL
)
GO

ALTER TABLE [dbo].[SysDataDict] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典类型',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDict',
'COLUMN', N'DictType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典排序',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDict',
'COLUMN', N'DictSort'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDict',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDict',
'COLUMN', N'CreateUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除时间',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDict',
'COLUMN', N'DeleteTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDict',
'COLUMN', N'DeleteUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改时间',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDict',
'COLUMN', N'LastModifyTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDict',
'COLUMN', N'LastModifyUserId'
GO


-- ----------------------------
-- Records of SysDataDict
-- ----------------------------
INSERT INTO [dbo].[SysDataDict] ([Id], [DictType], [DictSort], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'297048410391842816', N'NewsType', N'1', N'新闻类别', N'16508640061130151', NULL, NULL, N'2021-03-30 16:44:19.943', N'16508640061130151', N'0', N'2021-03-30 16:44:19.943')
GO


-- ----------------------------
-- Table structure for SysDataDictDetail
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysDataDictDetail]') AND type IN ('U'))
	DROP TABLE [dbo].[SysDataDictDetail]
GO

CREATE TABLE [dbo].[SysDataDictDetail] (
  [Id] bigint  NOT NULL,
  [DictType] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [DictSort] int  NULL,
  [DictKey] int  NULL,
  [DictValue] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [ListClass] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [DictStatus] int  NULL,
  [IsDefault] int  NULL,
  [Remark] nvarchar(500) COLLATE Chinese_PRC_CI_AS  NULL,
  [CreateUserId] bigint  NULL,
  [DeleteTime] datetime  NULL,
  [DeleteUserId] bigint  NULL,
  [LastModifyTime] datetime  NULL,
  [LastModifyUserId] bigint  NULL,
  [IsDelete] bit  NULL,
  [CreateTime] datetime  NULL
)
GO

ALTER TABLE [dbo].[SysDataDictDetail] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典类型(外键)',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'DictType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典排序',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'DictSort'
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典键(一般从1开始)',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'DictKey'
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典值',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'DictValue'
GO

EXEC sp_addextendedproperty
'MS_Description', N'显示样式(default primary success info warning danger)',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'ListClass'
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典状态(0禁用 1启用)',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'DictStatus'
GO

EXEC sp_addextendedproperty
'MS_Description', N'默认选中(0不是 1是)',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'IsDefault'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'CreateUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除时间',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'DeleteTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'DeleteUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改时间',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'LastModifyTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysDataDictDetail',
'COLUMN', N'LastModifyUserId'
GO


-- ----------------------------
-- Records of SysDataDictDetail
-- ----------------------------
INSERT INTO [dbo].[SysDataDictDetail] ([Id], [DictType], [DictSort], [DictKey], [DictValue], [ListClass], [DictStatus], [IsDefault], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'297289317041049600', N'NewsType', N'1', N'1', N'每日趣闻', NULL, N'1', N'0', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO


-- ----------------------------
-- Table structure for SysDepartment
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysDepartment]') AND type IN ('U'))
	DROP TABLE [dbo].[SysDepartment]
GO

CREATE TABLE [dbo].[SysDepartment] (
  [Id] bigint  NOT NULL,
  [ParentId] bigint  NULL,
  [DepartmentName] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [Telephone] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [Fax] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [Email] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [PrincipalId] bigint  NULL,
  [DepartmentSort] int  NULL,
  [Remark] nvarchar(500) COLLATE Chinese_PRC_CI_AS  NULL,
  [Enable] bit DEFAULT 1 NULL,
  [PrincipalPhone] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [DepartmentCode] varchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [CreateUserId] bigint  NULL,
  [DeleteTime] datetime  NULL,
  [DeleteUserId] bigint  NULL,
  [LastModifyTime] datetime  NULL,
  [LastModifyUserId] bigint  NULL,
  [IsDelete] bit  NULL,
  [CreateTime] datetime  NULL,
  [DeptType] int  NULL
)
GO

ALTER TABLE [dbo].[SysDepartment] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'父部门Id(0表示是根部门)',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'ParentId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门名称',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'DepartmentName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门电话',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'Telephone'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门传真',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'Fax'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门Email',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'Email'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门负责人Id',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'PrincipalId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门排序',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'DepartmentSort'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否启用',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'Enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'负责人电话',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'PrincipalPhone'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门编号',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'DepartmentCode'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'CreateUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除时间',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'DeleteTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'DeleteUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改时间',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'LastModifyTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'LastModifyUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门类型（DepartmentType枚举值）',
'SCHEMA', N'dbo',
'TABLE', N'SysDepartment',
'COLUMN', N'DeptType'
GO


-- ----------------------------
-- Records of SysDepartment
-- ----------------------------
INSERT INTO [dbo].[SysDepartment] ([Id], [ParentId], [DepartmentName], [Telephone], [Fax], [Email], [PrincipalId], [DepartmentSort], [Remark], [Enable], [PrincipalPhone], [DepartmentCode], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [DeptType]) VALUES (N'16508640061124402', NULL, N'主公司', N'0551-6666666', N'0551-8888888', NULL, NULL, N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0')
GO

INSERT INTO [dbo].[SysDepartment] ([Id], [ParentId], [DepartmentName], [Telephone], [Fax], [Email], [PrincipalId], [DepartmentSort], [Remark], [Enable], [PrincipalPhone], [DepartmentCode], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [DeptType]) VALUES (N'16508640061124403', N'16508640061124402', N'一号子公司', N'111', N'11', N'11', N'16508640061130151', N'1', N'', N'1', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0')
GO

INSERT INTO [dbo].[SysDepartment] ([Id], [ParentId], [DepartmentName], [Telephone], [Fax], [Email], [PrincipalId], [DepartmentSort], [Remark], [Enable], [PrincipalPhone], [DepartmentCode], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [DeptType]) VALUES (N'16508640061124404', N'16508640061124402', N'二号子公司', N'1', N'', N'', N'16508640061130150', N'1', N'', N'1', N'', N'', NULL, NULL, NULL, N'2021-08-16 15:47:43.460', N'16508640061130151', NULL, NULL, N'0')
GO

INSERT INTO [dbo].[SysDepartment] ([Id], [ParentId], [DepartmentName], [Telephone], [Fax], [Email], [PrincipalId], [DepartmentSort], [Remark], [Enable], [PrincipalPhone], [DepartmentCode], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [DeptType]) VALUES (N'16508640061124405', N'16508640061124403', N'研发部', N'3', N'', N'', N'16508640061130153', N'1', N'专注前端与后端结合的开发模式', N'1', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0')
GO

INSERT INTO [dbo].[SysDepartment] ([Id], [ParentId], [DepartmentName], [Telephone], [Fax], [Email], [PrincipalId], [DepartmentSort], [Remark], [Enable], [PrincipalPhone], [DepartmentCode], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [DeptType]) VALUES (N'16508640061124406', N'16508640061124403', N'测试部', N'1', N'', N'', N'0', N'3', N'', N'1', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0')
GO

INSERT INTO [dbo].[SysDepartment] ([Id], [ParentId], [DepartmentName], [Telephone], [Fax], [Email], [PrincipalId], [DepartmentSort], [Remark], [Enable], [PrincipalPhone], [DepartmentCode], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [DeptType]) VALUES (N'16508640061124407', N'16508640061124403', N'前端设计部', N'1', N'', N'', N'16508640061130150', N'2', N'', N'1', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0')
GO

INSERT INTO [dbo].[SysDepartment] ([Id], [ParentId], [DepartmentName], [Telephone], [Fax], [Email], [PrincipalId], [DepartmentSort], [Remark], [Enable], [PrincipalPhone], [DepartmentCode], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [DeptType]) VALUES (N'16508640061124408', N'16508640061124403', N'财务部', N'0551-87654321', N'0551-12345678', N'wangxue@yishasoft.com', N'0', N'15', N'2', N'1', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0')
GO

INSERT INTO [dbo].[SysDepartment] ([Id], [ParentId], [DepartmentName], [Telephone], [Fax], [Email], [PrincipalId], [DepartmentSort], [Remark], [Enable], [PrincipalPhone], [DepartmentCode], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [DeptType]) VALUES (N'16508640061124409', N'16508640061124403', N'市场部', N'111', N'111', N'11', N'16508640061130150', N'7', N'', N'1', N'', N'', NULL, NULL, NULL, N'2021-03-31 10:12:46.727', N'16508640061130151', NULL, NULL, N'0')
GO

INSERT INTO [dbo].[SysDepartment] ([Id], [ParentId], [DepartmentName], [Telephone], [Fax], [Email], [PrincipalId], [DepartmentSort], [Remark], [Enable], [PrincipalPhone], [DepartmentCode], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime], [DeptType]) VALUES (N'347405950350528512', N'16508640061124403', N'测试', N'13123123123', N'', N'asdada', N'16508640061130150', N'10', NULL, NULL, NULL, NULL, N'16508640061130151', NULL, NULL, N'2021-08-16 15:50:53.873', N'16508640061130151', N'0', N'2021-08-16 15:47:13.063', NULL)
GO


-- ----------------------------
-- Table structure for SysLogApi
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysLogApi]') AND type IN ('U'))
	DROP TABLE [dbo].[SysLogApi]
GO

CREATE TABLE [dbo].[SysLogApi] (
  [Id] bigint  NOT NULL,
  [CreateTime] datetime  NULL,
  [CreateUserId] bigint  NULL,
  [LogStatus] int  NULL,
  [Remark] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [ExecuteUrl] varchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [ExecuteParam] nvarchar(4000) COLLATE Chinese_PRC_CI_AS  NULL,
  [ExecuteResult] nvarchar(4000) COLLATE Chinese_PRC_CI_AS  NULL,
  [ExecuteTime] int  NULL,
  [IpAddress] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SysLogApi] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'执行状态(0失败 1成功)',
'SCHEMA', N'dbo',
'TABLE', N'SysLogApi',
'COLUMN', N'LogStatus'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysLogApi',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'接口地址',
'SCHEMA', N'dbo',
'TABLE', N'SysLogApi',
'COLUMN', N'ExecuteUrl'
GO

EXEC sp_addextendedproperty
'MS_Description', N'请求参数',
'SCHEMA', N'dbo',
'TABLE', N'SysLogApi',
'COLUMN', N'ExecuteParam'
GO

EXEC sp_addextendedproperty
'MS_Description', N'请求结果',
'SCHEMA', N'dbo',
'TABLE', N'SysLogApi',
'COLUMN', N'ExecuteResult'
GO

EXEC sp_addextendedproperty
'MS_Description', N'执行时间',
'SCHEMA', N'dbo',
'TABLE', N'SysLogApi',
'COLUMN', N'ExecuteTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'IP地址',
'SCHEMA', N'dbo',
'TABLE', N'SysLogApi',
'COLUMN', N'IpAddress'
GO


-- ----------------------------
-- Records of SysLogApi
-- ----------------------------
INSERT INTO [dbo].[SysLogApi] ([Id], [CreateTime], [CreateUserId], [LogStatus], [Remark], [ExecuteUrl], [ExecuteParam], [ExecuteResult], [ExecuteTime], [IpAddress]) VALUES (N'369061711413514240', N'2021-10-15 09:59:28.597', N'0', N'1', NULL, N'/HomeManage/Home/GetPageListAndUserInfo', N'', NULL, N'499', N'192.168.66.119')
GO

INSERT INTO [dbo].[SysLogApi] ([Id], [CreateTime], [CreateUserId], [LogStatus], [Remark], [ExecuteUrl], [ExecuteParam], [ExecuteResult], [ExecuteTime], [IpAddress]) VALUES (N'369061719676293120', N'2021-10-15 09:59:30.787', N'0', N'1', NULL, N'/OrganizationManage/Department/GetListJson', N'', NULL, N'49', N'192.168.66.119')
GO

INSERT INTO [dbo].[SysLogApi] ([Id], [CreateTime], [CreateUserId], [LogStatus], [Remark], [ExecuteUrl], [ExecuteParam], [ExecuteResult], [ExecuteTime], [IpAddress]) VALUES (N'369061729591627776', N'2021-10-15 09:59:33.153', N'0', N'1', NULL, N'/SystemManage/LogApi/GetPageListJson', N'?PageIndex=1&PageSize=15', NULL, N'63', N'192.168.66.119')
GO

INSERT INTO [dbo].[SysLogApi] ([Id], [CreateTime], [CreateUserId], [LogStatus], [Remark], [ExecuteUrl], [ExecuteParam], [ExecuteResult], [ExecuteTime], [IpAddress]) VALUES (N'369061732225650688', N'2021-10-15 09:59:33.777', N'0', N'1', NULL, N'/SystemManage/LogLogin/GetPageListJson', N'?PageIndex=1&PageSize=15', NULL, N'17', N'192.168.66.119')
GO


-- ----------------------------
-- Table structure for SysLogLogin
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysLogLogin]') AND type IN ('U'))
	DROP TABLE [dbo].[SysLogLogin]
GO

CREATE TABLE [dbo].[SysLogLogin] (
  [Id] bigint  NOT NULL,
  [CreateTime] datetime  NOT NULL,
  [CreateUserId] bigint  NULL,
  [LogStatus] int  NOT NULL,
  [IpAddress] varchar(20) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [IpLocation] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [Browser] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [OS] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [Remark] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [ExtraRemark] nvarchar(500) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SysLogLogin] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'执行状态(0失败 1成功)',
'SCHEMA', N'dbo',
'TABLE', N'SysLogLogin',
'COLUMN', N'LogStatus'
GO

EXEC sp_addextendedproperty
'MS_Description', N'ip地址',
'SCHEMA', N'dbo',
'TABLE', N'SysLogLogin',
'COLUMN', N'IpAddress'
GO

EXEC sp_addextendedproperty
'MS_Description', N'ip位置',
'SCHEMA', N'dbo',
'TABLE', N'SysLogLogin',
'COLUMN', N'IpLocation'
GO

EXEC sp_addextendedproperty
'MS_Description', N'浏览器',
'SCHEMA', N'dbo',
'TABLE', N'SysLogLogin',
'COLUMN', N'Browser'
GO

EXEC sp_addextendedproperty
'MS_Description', N'操作系统',
'SCHEMA', N'dbo',
'TABLE', N'SysLogLogin',
'COLUMN', N'OS'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysLogLogin',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'额外备注',
'SCHEMA', N'dbo',
'TABLE', N'SysLogLogin',
'COLUMN', N'ExtraRemark'
GO


-- ----------------------------
-- Records of SysLogLogin
-- ----------------------------

-- ----------------------------
-- Table structure for SysMenu
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysMenu]') AND type IN ('U'))
	DROP TABLE [dbo].[SysMenu]
GO

CREATE TABLE [dbo].[SysMenu] (
  [Id] bigint  NOT NULL,
  [ParentId] bigint  NULL,
  [MenuName] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [MenuIcon] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [MenuUrl] varchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [MenuTarget] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [MenuSort] int  NULL,
  [MenuType] int  NULL,
  [MenuStatus] int  NULL,
  [Authorize] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [Remark] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [CreateUserId] bigint  NULL,
  [DeleteTime] datetime  NULL,
  [DeleteUserId] bigint  NULL,
  [LastModifyTime] datetime  NULL,
  [LastModifyUserId] bigint  NULL,
  [IsDelete] bit  NULL,
  [CreateTime] datetime  NULL
)
GO

ALTER TABLE [dbo].[SysMenu] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'父菜单Id(0表示是根菜单)',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'ParentId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'菜单名称',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'MenuName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'菜单图标',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'MenuIcon'
GO

EXEC sp_addextendedproperty
'MS_Description', N'菜单Url',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'MenuUrl'
GO

EXEC sp_addextendedproperty
'MS_Description', N'链接打开方式',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'MenuTarget'
GO

EXEC sp_addextendedproperty
'MS_Description', N'菜单排序',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'MenuSort'
GO

EXEC sp_addextendedproperty
'MS_Description', N'菜单类型(1目录 2页面 3按钮)',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'MenuType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'菜单状态(0禁用 1启用)',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'MenuStatus'
GO

EXEC sp_addextendedproperty
'MS_Description', N'菜单权限标识',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'Authorize'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'CreateUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除时间',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'DeleteTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'DeleteUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改时间',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'LastModifyTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysMenu',
'COLUMN', N'LastModifyUserId'
GO


-- ----------------------------
-- Records of SysMenu
-- ----------------------------
INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130069', N'0', N'单位组织', N'layui-icon layui-icon-user', N'', N'', N'100', N'1', N'1', N'', N'', NULL, NULL, NULL, N'2021-04-12 14:22:56.287', N'16508640061130151', NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130070', N'0', N'系统管理', N'layui-icon layui-icon-set', N'', N'', N'101', N'1', N'1', N'', N'', NULL, NULL, NULL, N'2021-04-12 14:23:01.983', N'16508640061130151', NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130071', N'0', N'系统工具', N'layui-icon layui-icon-util', N'', N'', N'102', N'1', N'1', N'', N'', NULL, NULL, NULL, N'2021-04-12 14:23:07.380', N'16508640061130151', NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130072', N'16508640061130069', N'员工管理', N'layui-icon layui-icon-set', N'#/organization/user/userIndex', N'', N'1', N'2', N'1', N'organization:user:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130073', N'16508640061130069', N'部门管理', N'', N'#/organization/department/departmentIndex', N'', N'2', N'2', N'1', N'organization:department:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130074', N'16508640061130070', N'系统角色', N'layui-icon ', N'#/system/role/roleIndex', N'', N'1', N'2', N'1', N'system:role:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130075', N'16508640061130070', N'系统菜单', N'layui-icon ', N'#/system/menu/menuIndex', N'', N'2', N'2', N'1', N'system:menu:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130076', N'16508640061130070', N'系统日志', N'layui-icon layui-icon-align-left', NULL, N'', N'10', N'1', N'1', NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130077', N'16508640061130070', N'通用字典', NULL, N'#/system/datadict/datadictIndex', N'', N'5', N'2', N'1', N'system:datadict:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130079', N'16508640061130070', N'数据表管理', NULL, N'#/system/database/databaseIndex', N'', N'14', N'2', N'1', N'system:datatable:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130080', N'16508640061130071', N'代码生成', N'', N'#/tool/codeGenerator/codeGeneratorIndex', N'', N'1', N'2', N'1', N'tool:codegenerator:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130082', N'16508640061130076', N'登录日志', NULL, N'#/system/logLogin/logLoginIndex', N'', N'1', N'2', N'1', N'system:loglogin:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130083', N'16508640061130069', N'职位管理', N'', N'#/organization/position/positionIndex', N'', N'3', N'2', N'1', N'organization:position:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130084', N'16508640061130072', N'员工查询', N'', N'', N'', N'1', N'3', N'1', N'organization:user:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130085', N'16508640061130072', N'员工新增', N'', N'', N'', N'2', N'3', N'1', N'organization:user:add', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130086', N'16508640061130072', N'员工修改', N'', N'', N'', N'3', N'3', N'1', N'organization:user:edit', N'', NULL, NULL, NULL, N'2021-08-19 13:00:06.367', N'16508640061130151', NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130087', N'16508640061130072', N'员工删除', N'', N'', N'', N'4', N'3', N'1', N'organization:user:delete', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130089', N'16508640061130072', N'重置密码', N'', N'', N'', N'6', N'3', N'1', N'organization:user:resetpwd', N'', NULL, NULL, NULL, N'2021-08-19 13:00:55.697', N'16508640061130151', NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130090', N'16508640061130073', N'部门查询', N'', N'', N'', N'1', N'3', N'1', N'organization:department:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130091', N'16508640061130073', N'部门新增', N'', N'', N'', N'2', N'3', N'1', N'organization:department:add', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130092', N'16508640061130073', N'部门修改', N'', N'', N'', N'3', N'3', N'1', N'organization:department:edit', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130093', N'16508640061130073', N'部门删除', N'', N'', N'', N'4', N'3', N'1', N'organization:department:delete', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130094', N'16508640061130083', N'职位查询', N'', N'', N'', N'1', N'3', N'1', N'organization:position:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130095', N'16508640061130083', N'职位新增', N'', N'', N'', N'2', N'3', N'1', N'organization:position:add', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130096', N'16508640061130083', N'职位修改', N'', N'', N'', N'3', N'3', N'1', N'organization:position:edit', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130097', N'16508640061130083', N'职位删除', N'', N'', N'', N'4', N'3', N'1', N'organization:position:delete', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130098', N'16508640061130074', N'角色查询', N'', N'', N'', N'1', N'3', N'1', N'system:role:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130099', N'16508640061130074', N'角色新增', N'', N'', N'', N'2', N'3', N'1', N'system:role:add', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130100', N'16508640061130074', N'角色修改', N'', N'', N'', N'3', N'3', N'1', N'system:role:edit', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130101', N'16508640061130074', N'角色删除', N'', N'', N'', N'4', N'3', N'1', N'system:role:delete', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130102', N'16508640061130075', N'菜单查询', N'', N'', N'', N'1', N'3', N'1', N'system:menu:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130103', N'16508640061130075', N'菜单新增', N'', N'', N'', N'2', N'3', N'1', N'system:menu:add', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130104', N'16508640061130075', N'菜单修改', N'', N'', N'', N'3', N'3', N'1', N'system:menu:edit', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130105', N'16508640061130075', N'菜单删除', N'', N'', N'', N'4', N'3', N'1', N'system:menu:delete', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130106', N'16508640061130077', N'字典查询', N'', N'', N'', N'1', N'3', N'1', N'system:datadict:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130107', N'16508640061130077', N'字典新增', N'', N'', N'', N'2', N'3', N'1', N'system:datadict:add', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130108', N'16508640061130077', N'字典修改', N'', N'', N'', N'3', N'3', N'1', N'system:datadict:edit', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130109', N'16508640061130077', N'字典删除', N'', N'', N'', N'4', N'3', N'1', N'system:datadict:delete', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130114', N'16508640061130082', N'登录日志查询', N'', N'', N'', N'1', N'3', N'1', N'system:loglogin:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130118', N'16508640061130079', N'数据表查询', N'', N'', N'', N'1', N'3', N'1', N'system:datatable:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130119', N'16508640061130080', N'代码生成', N'', N'', N'', N'2', N'3', N'1', N'tool:codegenerator:add', N'', NULL, NULL, NULL, N'2021-08-19 14:59:36.230', N'16508640061130151', NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130120', N'16508640061130080', N'代码生成查询', N'', N'', N'', N'1', N'3', N'1', N'tool:codegenerator:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130121', N'16508640061130071', N'服务器信息', NULL, N'#/tool/server/serverIndex', N'', N'15', N'2', N'1', N'tool:server:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130122', N'16508640061130070', N'定时任务', N'', N'#/system/autoJob/autoJobIndex', N'', N'12', N'2', N'2', N'system:autojob:view', N'', NULL, NULL, NULL, N'2021-10-15 09:46:15.243', N'16508640061130151', NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130123', N'16508640061130122', N'定时任务查询', N'', N'', N'', N'1', N'3', N'1', N'system:autojob:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130124', N'16508640061130122', N'定时任务新增', N'', N'', N'', N'2', N'3', N'1', N'system:autojob:add', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130125', N'16508640061130122', N'定时任务修改', N'', N'', N'', N'3', N'3', N'1', N'system:autojob:edit', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130126', N'16508640061130122', N'定时任务删除', N'', N'', N'', N'4', N'3', N'1', N'system:autojob:delete', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130128', N'16508640061130122', N'定时任务日志查看', N'', N'', N'', N'5', N'3', N'1', N'system:autojob:logview', N'', NULL, NULL, NULL, N'2021-08-19 13:24:52.407', N'16508640061130151', NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130134', N'16508640061130070', N'系统api', N'layui-icon ', N'#/api', N'', N'13', N'2', N'1', N'system:api:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130135', N'16508640061130076', N'Api日志', NULL, N'#/system/logApi/logApiIndex', N'', N'3', N'2', N'1', N'system:logapi:view', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130136', N'16508640061130135', N'Api日志查询', N'', N'', N'', N'1', N'3', N'1', N'system:logapi:search', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130137', N'16508640061130135', N'Api日志详情', N'', N'', N'', N'2', N'3', N'1', N'system:logapi:detail', N'', NULL, NULL, NULL, N'2021-08-19 13:21:34.240', N'16508640061130151', NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'260867164880244736', N'16508640061130070', N'缓存管理', NULL, N'#/system/cache/cacheIndex', NULL, N'14', N'1', N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348453219346681856', N'16508640061130074', N'权限分配', N'', N'', NULL, N'5', N'3', N'1', N'system:role:auths', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 13:08:41.440', N'16508640061130151', N'0', N'2021-08-19 13:08:41.440')
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348454208413896704', N'16508640061130075', N'接口权限', N'', N'', NULL, N'5', N'3', N'1', N'system:menu:btnauths', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 13:12:37.253', N'16508640061130151', N'0', N'2021-08-19 13:12:37.253')
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348455016329121792', N'16508640061130077', N'字典值新增', N'', N'', NULL, N'5', N'3', N'1', N'system:datadicdetail:add', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 13:18:21.643', N'16508640061130151', N'0', N'2021-08-19 13:15:49.873')
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348455591577915392', N'16508640061130077', N'字典值修改', N'', N'', NULL, N'6', N'3', N'1', N'system:datadicdetail:edit', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 13:18:31.247', N'16508640061130151', N'0', N'2021-08-19 13:18:07.023')
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348455821002149888', N'16508640061130077', N'字典值删除', N'', N'', NULL, N'7', N'3', N'1', N'system:datadictdetail:delete', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 13:19:01.723', N'16508640061130151', N'0', N'2021-08-19 13:19:01.723')
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348472333524013056', N'260867164880244736', N'缓存查询', N'', N'', NULL, N'1', N'3', N'1', N'system:cache:search', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 14:25:10.093', N'16508640061130151', N'0', N'2021-08-19 14:24:38.617')
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348472428696965120', N'260867164880244736', N'缓存新增', N'', N'', NULL, N'2', N'3', N'1', N'system:cache:add', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 14:25:19.593', N'16508640061130151', N'0', N'2021-08-19 14:25:01.307')
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348472752279130112', N'260867164880244736', N'缓存修改', N'', N'', NULL, N'3', N'3', N'1', N'system:cache:edit', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 14:26:18.453', N'16508640061130151', N'0', N'2021-08-19 14:26:18.453')
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348472837276700672', N'260867164880244736', N'缓存删除', N'', N'', NULL, N'4', N'3', N'1', N'system:cache:delete', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 14:26:38.720', N'16508640061130151', N'0', N'2021-08-19 14:26:38.720')
GO

INSERT INTO [dbo].[SysMenu] ([Id], [ParentId], [MenuName], [MenuIcon], [MenuUrl], [MenuTarget], [MenuSort], [MenuType], [MenuStatus], [Authorize], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348481239650406400', N'16508640061130080', N'代码生成模板', N'', N'', NULL, N'3', N'3', N'1', N'tool:codegenerator:templet', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 15:00:02.003', N'16508640061130151', N'0', N'2021-08-19 15:00:02.003')
GO


-- ----------------------------
-- Table structure for SysMenuAuthorize
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysMenuAuthorize]') AND type IN ('U'))
	DROP TABLE [dbo].[SysMenuAuthorize]
GO

CREATE TABLE [dbo].[SysMenuAuthorize] (
  [Id] bigint  NOT NULL,
  [MenuId] bigint  NOT NULL,
  [AuthorizeId] bigint  NOT NULL,
  [AuthorizeType] int  NOT NULL
)
GO

ALTER TABLE [dbo].[SysMenuAuthorize] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'菜单Id',
'SCHEMA', N'dbo',
'TABLE', N'SysMenuAuthorize',
'COLUMN', N'MenuId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'授权Id(角色Id或者用户Id)',
'SCHEMA', N'dbo',
'TABLE', N'SysMenuAuthorize',
'COLUMN', N'AuthorizeId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'授权类型(1角色 2用户)',
'SCHEMA', N'dbo',
'TABLE', N'SysMenuAuthorize',
'COLUMN', N'AuthorizeType'
GO


-- ----------------------------
-- Records of SysMenuAuthorize
-- ----------------------------
INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081509060382720', N'16508640061130069', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081509404315648', N'16508640061130072', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081509710499840', N'16508640061130084', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081510016684032', N'16508640061130085', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081510314479616', N'16508640061130086', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081510620663808', N'16508640061130087', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081510918459392', N'16508640061130088', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081511224643584', N'16508640061130089', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081511535022080', N'16508640061130073', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081511837011968', N'16508640061130090', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081512143196160', N'16508640061130091', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081512453574656', N'16508640061130092', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081512755564544', N'16508640061130093', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081513057554432', N'16508640061130083', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081513359544320', N'16508640061130094', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081513665728512', N'16508640061130095', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081513967718400', N'16508640061130096', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46081514273902592', N'16508640061130097', N'253512862180315136', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46123151389757440', N'16508640061130069', N'16508640061130147', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46123151687553024', N'16508640061130072', N'16508640061130147', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46123151972765696', N'16508640061130084', N'16508640061130147', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46123152245395456', N'16508640061130085', N'16508640061130147', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46123152513830912', N'16508640061130086', N'16508640061130147', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46123152786460672', N'16508640061130070', N'16508640061130147', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46123153059090432', N'16508640061130076', N'16508640061130147', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46123153327525888', N'16508640061130135', N'16508640061130147', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46123153591767040', N'16508640061130136', N'16508640061130147', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'46123153868591104', N'16508640061130137', N'16508640061130147', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241783234560', N'16508640061130069', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241787428864', N'16508640061130072', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241791623168', N'16508640061130084', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241800011776', N'16508640061130085', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241804206080', N'16508640061130086', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241808400384', N'16508640061130087', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241812594688', N'16508640061130088', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241820983296', N'16508640061130089', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241825177600', N'16508640061130073', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241829371904', N'16508640061130090', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241837760512', N'16508640061130091', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241841954816', N'16508640061130092', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241850343424', N'16508640061130093', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241854537728', N'16508640061130083', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241862926336', N'16508640061130094', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241871314944', N'16508640061130095', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241875509248', N'16508640061130096', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241883897856', N'16508640061130097', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241892286464', N'16508640061130129', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241900675072', N'16508640061130130', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241909063680', N'16508640061130131', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241913257984', N'16508640061130132', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'103195241921646592', N'16508640061130133', N'103195209390624768', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092413965762560', N'16508640061130069', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092414716542976', N'16508640061130072', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092414955618304', N'16508640061130084', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092415169527808', N'16508640061130085', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092415383437312', N'16508640061130086', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092415593152512', N'16508640061130087', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092415798673408', N'16508640061130088', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092416008388608', N'16508640061130089', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092416222298112', N'16508640061130073', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092416427819008', N'16508640061130090', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092416629145600', N'16508640061130091', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092416834666496', N'16508640061130092', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092417044381696', N'16508640061130093', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092417249902592', N'16508640061130083', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092417455423488', N'16508640061130094', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092417669332992', N'16508640061130095', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092417874853888', N'16508640061130096', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092418088763392', N'16508640061130097', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092418294284288', N'16508640061130070', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092418503999488', N'16508640061130074', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092418722103296', N'16508640061130098', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092418927624192', N'16508640061130099', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092419133145088', N'16508640061130100', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092419351248896', N'16508640061130101', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092419569352704', N'16508640061130075', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092419783262208', N'16508640061130102', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092419992977408', N'16508640061130103', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092420219469824', N'16508640061130104', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092420500488192', N'16508640061130105', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092420714397696', N'16508640061130076', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092420919918592', N'16508640061130082', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092421125439488', N'16508640061130114', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092421326766080', N'16508640061130115', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092421528092672', N'16508640061130135', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092421729419264', N'16508640061130136', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092421939134464', N'16508640061130137', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092422144655360', N'16508640061130077', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092422350176256', N'16508640061130106', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092422551502848', N'16508640061130107', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092422761218048', N'16508640061130108', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092422962544640', N'16508640061130109', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092423163871232', N'16508640061130078', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092423365197824', N'16508640061130110', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092423566524416', N'16508640061130111', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092423772045312', N'16508640061130112', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092423977566208', N'16508640061130113', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092424183087104', N'16508640061130079', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092424388608000', N'16508640061130118', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092424606711808', N'16508640061130122', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092424829009920', N'16508640061130123', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092425038725120', N'16508640061130124', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092425252634624', N'16508640061130125', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092425458155520', N'16508640061130126', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092425655287808', N'16508640061130127', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092425860808704', N'16508640061130128', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092426062135296', N'16508640061130134', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092426276044800', N'16508640061130071', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092426485760000', N'16508640061130080', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092426695475200', N'16508640061130119', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092426900996096', N'16508640061130120', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092427106516992', N'16508640061130121', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092427307843584', N'16508640061131999', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092427513364480', N'16508640061132000', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'252092427727273984', N'16508640061132001', N'16508640061130146', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'258565056382373888', N'16508640061130069', N'258201886530736128', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'258565056466259968', N'16508640061130072', N'258201886530736128', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'258565056483037184', N'16508640061130087', N'258201886530736128', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'258565056491425792', N'16508640061130088', N'258201886530736128', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114459746304', N'16508640061130069', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114522660864', N'16508640061130072', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114535243776', N'16508640061130084', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114552020992', N'16508640061130085', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114568798208', N'16508640061130086', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114581381120', N'16508640061130087', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114598158336', N'16508640061130088', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114610741248', N'16508640061130089', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114627518464', N'16508640061130073', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114640101376', N'16508640061130090', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114652684288', N'16508640061130091', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114669461504', N'16508640061130092', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114686238720', N'16508640061130093', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114703015936', N'16508640061130083', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114715598848', N'16508640061130094', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114732376064', N'16508640061130095', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114744958976', N'16508640061130096', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114761736192', N'16508640061130097', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'286975114778513408', N'286447559788990464', N'286619174082449408', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872531423232', N'16508640061130069', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872548200448', N'16508640061130072', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872560783360', N'16508640061130084', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872573366272', N'16508640061130085', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872585949184', N'16508640061130086', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872598532096', N'16508640061130088', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872611115008', N'16508640061130089', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872623697920', N'16508640061130073', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872636280832', N'16508640061130090', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872648863744', N'16508640061130091', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872661446656', N'16508640061130092', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872674029568', N'16508640061130093', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872686612480', N'16508640061130083', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872699195392', N'16508640061130094', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872711778304', N'16508640061130095', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872724361216', N'16508640061130096', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872736944128', N'16508640061130097', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872749527040', N'16508640061130070', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872762109952', N'16508640061130074', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872778887168', N'16508640061130098', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872791470080', N'16508640061130099', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872804052992', N'16508640061130100', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872816635904', N'16508640061130101', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872829218816', N'16508640061130075', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872845996032', N'16508640061130102', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872858578944', N'16508640061130103', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872871161856', N'16508640061130104', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872887939072', N'16508640061130105', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872900521984', N'16508640061130076', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872913104896', N'16508640061130082', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872934076416', N'16508640061130114', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872946659328', N'16508640061130115', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872959242240', N'16508640061130135', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872976019456', N'16508640061130136', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682872988602368', N'16508640061130137', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873005379584', N'16508640061130077', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873017962496', N'16508640061130106', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873034739712', N'16508640061130107', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873047322624', N'16508640061130108', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873064099840', N'16508640061130109', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873080877056', N'16508640061130078', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873093459968', N'16508640061130110', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873110237184', N'16508640061130111', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873122820096', N'16508640061130112', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873139597312', N'16508640061130113', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873156374528', N'16508640061130079', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873168957440', N'16508640061130118', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873185734656', N'16508640061130122', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682873890377728', N'16508640061130123', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682874037178368', N'16508640061130124', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682874192367616', N'16508640061130125', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682874326585344', N'16508640061130126', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682874410471424', N'16508640061130127', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682874464997376', N'16508640061130128', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682874553077760', N'16508640061130134', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682875299663872', N'260867164880244736', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682875467436032', N'261771075572994048', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682875551322112', N'261771076357328896', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682875622625280', N'261771077133275136', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682876423737344', N'261771077850501120', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682876549566464', N'261771078534172672', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682876616675328', N'16508640061130071', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682876637646848', N'16508640061130080', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682876658618368', N'16508640061130119', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682876679589888', N'16508640061130120', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682876700561408', N'16508640061130121', N'277766285150916608', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682888981483520', N'16508640061130069', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682888998260736', N'16508640061130072', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889010843648', N'16508640061130084', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889027620864', N'16508640061130085', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889040203776', N'16508640061130086', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889056980992', N'16508640061130088', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889069563904', N'16508640061130089', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889086341120', N'16508640061130073', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889098924032', N'16508640061130090', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889115701248', N'16508640061130091', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889128284160', N'16508640061130092', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889145061376', N'16508640061130093', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889157644288', N'16508640061130083', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889174421504', N'16508640061130094', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889191198720', N'16508640061130095', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889207975936', N'16508640061130096', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889220558848', N'16508640061130097', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889237336064', N'16508640061130070', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889249918976', N'16508640061130076', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889266696192', N'16508640061130082', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889283473408', N'16508640061130114', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889296056320', N'16508640061130115', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889312833536', N'16508640061130135', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889325416448', N'16508640061130136', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'289682889342193664', N'16508640061130137', N'287185473351192576', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'290558951165005824', N'16508640061130069', N'289429554030710784', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'290558951248891904', N'16508640061130072', N'289429554030710784', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'290558951265669120', N'16508640061130086', N'289429554030710784', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'290558951282446336', N'16508640061130070', N'289429554030710784', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'290558951295029248', N'16508640061130074', N'289429554030710784', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'290558951307612160', N'16508640061130100', N'289429554030710784', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899490996260864', N'16508640061130069', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491013038080', N'16508640061130072', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491029815296', N'16508640061130084', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491042398208', N'16508640061130085', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491054981120', N'16508640061130086', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491067564032', N'16508640061130087', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491080146944', N'16508640061130088', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491096924160', N'16508640061130089', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491109507072', N'16508640061130073', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491122089984', N'16508640061130090', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491151450112', N'16508640061130091', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491164033024', N'16508640061130092', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491176615936', N'16508640061130093', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491193393152', N'16508640061130083', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491205976064', N'16508640061130094', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491277279232', N'16508640061130095', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491298250752', N'16508640061130096', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'293899491310833664', N'16508640061130097', N'292826047173365760', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'295136574889267200', N'16508640061130069', N'289712787762974720', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'295136575036067840', N'16508640061130072', N'289712787762974720', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'295136575065427968', N'16508640061130084', N'289712787762974720', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'295136575090593792', N'16508640061130086', N'289712787762974720', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'295136575119953920', N'16508640061130088', N'289712787762974720', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'296695508938788864', N'16508640061130069', N'296691626556788736', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'296695508963954688', N'16508640061130072', N'296691626556788736', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'296695508993314816', N'16508640061130084', N'296691626556788736', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'296695509018480640', N'16508640061130070', N'296691626556788736', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'296695509047840768', N'16508640061130075', N'296691626556788736', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'296695509073006592', N'16508640061130103', N'296691626556788736', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'296695509102366720', N'16508640061130104', N'296691626556788736', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471093665792', N'16508640061130069', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471114637312', N'16508640061130072', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471127220224', N'16508640061130084', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471139803136', N'16508640061130085', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471152386048', N'16508640061130086', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471169163264', N'16508640061130087', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471181746176', N'16508640061130089', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471194329088', N'16508640061130073', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471206912000', N'16508640061130090', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471223689216', N'16508640061130091', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471236272128', N'16508640061130092', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471253049344', N'16508640061130093', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471265632256', N'16508640061130083', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471278215168', N'16508640061130094', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471294992384', N'16508640061130095', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471311769600', N'16508640061130096', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471324352512', N'16508640061130097', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471336935424', N'16508640061130070', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471353712640', N'16508640061130074', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471366295552', N'16508640061130098', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471383072768', N'16508640061130099', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471399849984', N'16508640061130100', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471412432896', N'16508640061130101', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471429210112', N'16508640061130075', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471445987328', N'16508640061130102', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471462764544', N'16508640061130103', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471479541760', N'16508640061130104', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471496318976', N'16508640061130105', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471513096192', N'16508640061130077', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471529873408', N'16508640061130106', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471546650624', N'16508640061130107', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471563427840', N'16508640061130108', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471580205056', N'16508640061130109', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471596982272', N'16508640061130076', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471613759488', N'16508640061130082', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471630536704', N'16508640061130114', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471651508224', N'16508640061130115', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471672479744', N'16508640061130135', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471689256960', N'16508640061130136', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471710228480', N'16508640061130137', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471727005696', N'16508640061130122', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471747977216', N'16508640061130123', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471764754432', N'16508640061130124', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471785725952', N'16508640061130125', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471802503168', N'16508640061130126', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471819280384', N'16508640061130127', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471840251904', N'16508640061130128', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471857029120', N'16508640061130134', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471878000640', N'260867164880244736', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471894777856', N'16508640061130079', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471911555072', N'16508640061130118', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471932526592', N'261771075572994048', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471953498112', N'261771076357328896', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471970275328', N'261771077133275136', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451471991246848', N'261771077850501120', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451472012218368', N'261771078534172672', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451472028995584', N'16508640061130071', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451472049967104', N'16508640061130080', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451472070938624', N'16508640061130120', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451472091910144', N'16508640061130119', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348451472108687360', N'16508640061130121', N'258606017569361920', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348508668603404288', N'16508640061130069', N'348410673211904000', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348508668611792896', N'16508640061130072', N'348410673211904000', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348508668620181504', N'16508640061130084', N'348410673211904000', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348508668632764416', N'16508640061130085', N'348410673211904000', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348508668641153024', N'16508640061130086', N'348410673211904000', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348508668649541632', N'16508640061130087', N'348410673211904000', N'1')
GO

INSERT INTO [dbo].[SysMenuAuthorize] ([Id], [MenuId], [AuthorizeId], [AuthorizeType]) VALUES (N'348508668662124544', N'16508640061130089', N'348410673211904000', N'1')
GO


-- ----------------------------
-- Table structure for SysPosition
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysPosition]') AND type IN ('U'))
	DROP TABLE [dbo].[SysPosition]
GO

CREATE TABLE [dbo].[SysPosition] (
  [Id] bigint  NOT NULL,
  [PositionName] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [PositionSort] int  NOT NULL,
  [Remark] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [CreateUserId] bigint  NULL,
  [DeleteTime] datetime  NULL,
  [DeleteUserId] bigint  NULL,
  [LastModifyTime] datetime  NULL,
  [LastModifyUserId] bigint  NULL,
  [IsDelete] bit  NULL,
  [CreateTime] datetime  NULL
)
GO

ALTER TABLE [dbo].[SysPosition] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'职位名称',
'SCHEMA', N'dbo',
'TABLE', N'SysPosition',
'COLUMN', N'PositionName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'职位排序',
'SCHEMA', N'dbo',
'TABLE', N'SysPosition',
'COLUMN', N'PositionSort'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysPosition',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysPosition',
'COLUMN', N'CreateUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除时间',
'SCHEMA', N'dbo',
'TABLE', N'SysPosition',
'COLUMN', N'DeleteTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysPosition',
'COLUMN', N'DeleteUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改时间',
'SCHEMA', N'dbo',
'TABLE', N'SysPosition',
'COLUMN', N'LastModifyTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysPosition',
'COLUMN', N'LastModifyUserId'
GO


-- ----------------------------
-- Records of SysPosition
-- ----------------------------
INSERT INTO [dbo].[SysPosition] ([Id], [PositionName], [PositionSort], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130141', N'项目经理', N'3', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysPosition] ([Id], [PositionName], [PositionSort], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'16508640061130142', N'测试经理', N'4', N'111', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysPosition] ([Id], [PositionName], [PositionSort], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'347390645939212288', N'测试', N'123', N'123', N'16508640061130151', NULL, NULL, N'2021-08-16 14:46:24.210', N'16508640061130151', N'0', N'2021-08-16 14:46:24.207')
GO


-- ----------------------------
-- Table structure for SysRole
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysRole]') AND type IN ('U'))
	DROP TABLE [dbo].[SysRole]
GO

CREATE TABLE [dbo].[SysRole] (
  [Id] bigint  NOT NULL,
  [RoleName] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [RoleSort] int  NOT NULL,
  [RoleStatus] int  NOT NULL,
  [Remark] nvarchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [CreateUserId] bigint  NULL,
  [DeleteTime] datetime  NULL,
  [DeleteUserId] bigint  NULL,
  [LastModifyTime] datetime  NULL,
  [LastModifyUserId] bigint  NULL,
  [IsDelete] bit  NULL,
  [CreateTime] datetime  NULL
)
GO

ALTER TABLE [dbo].[SysRole] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'角色名称',
'SCHEMA', N'dbo',
'TABLE', N'SysRole',
'COLUMN', N'RoleName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'角色排序',
'SCHEMA', N'dbo',
'TABLE', N'SysRole',
'COLUMN', N'RoleSort'
GO

EXEC sp_addextendedproperty
'MS_Description', N'角色状态(0禁用 1启用)',
'SCHEMA', N'dbo',
'TABLE', N'SysRole',
'COLUMN', N'RoleStatus'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysRole',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysRole',
'COLUMN', N'CreateUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除时间',
'SCHEMA', N'dbo',
'TABLE', N'SysRole',
'COLUMN', N'DeleteTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysRole',
'COLUMN', N'DeleteUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改时间',
'SCHEMA', N'dbo',
'TABLE', N'SysRole',
'COLUMN', N'LastModifyTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysRole',
'COLUMN', N'LastModifyUserId'
GO


-- ----------------------------
-- Records of SysRole
-- ----------------------------
INSERT INTO [dbo].[SysRole] ([Id], [RoleName], [RoleSort], [RoleStatus], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'258606017569361920', N'管理', N'1', N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysRole] ([Id], [RoleName], [RoleSort], [RoleStatus], [Remark], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete], [CreateTime]) VALUES (N'348410673211904000', N'测试角色A', N'2', N'1', NULL, N'16508640061130151', NULL, NULL, N'2021-08-19 10:19:37.653', N'16508640061130151', N'0', N'2021-08-19 10:19:37.653')
GO


-- ----------------------------
-- Table structure for SysTest
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysTest]') AND type IN ('U'))
	DROP TABLE [dbo].[SysTest]
GO

CREATE TABLE [dbo].[SysTest] (
  [Id] bigint  NOT NULL,
  [MyName] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [SearchName] varchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[SysTest] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of SysTest
-- ----------------------------

-- ----------------------------
-- Table structure for SysUser
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysUser]') AND type IN ('U'))
	DROP TABLE [dbo].[SysUser]
GO

CREATE TABLE [dbo].[SysUser] (
  [Id] bigint  NOT NULL,
  [IsDelete] bit  NOT NULL,
  [UserName] nvarchar(20) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [Password] varchar(32) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [Salt] varchar(5) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [RealName] nvarchar(20) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [DepartmentId] bigint  NOT NULL,
  [Gender] int  NOT NULL,
  [Birthday] varchar(10) COLLATE Chinese_PRC_CI_AS  NULL,
  [Portrait] varchar(200) COLLATE Chinese_PRC_CI_AS  NULL,
  [Email] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [Mobile] varchar(11) COLLATE Chinese_PRC_CI_AS  NULL,
  [QQ] varchar(20) COLLATE Chinese_PRC_CI_AS  NULL,
  [WeChat] varchar(20) COLLATE Chinese_PRC_CI_AS  NULL,
  [LoginCount] int  NULL,
  [UserStatus] int  NOT NULL,
  [IsSystem] int  NULL,
  [IsOnline] int  NULL,
  [FirstVisit] datetime  NULL,
  [PreviousVisit] datetime  NULL,
  [LastVisit] datetime  NULL,
  [Remark] nvarchar(200) COLLATE Chinese_PRC_CI_AS  NULL,
  [Picture] varchar(200) COLLATE Chinese_PRC_CI_AS  NULL,
  [ApiToken] varchar(200) COLLATE Chinese_PRC_CI_AS  NULL,
  [CreateUserId] bigint  NULL,
  [DeleteTime] datetime  NULL,
  [DeleteUserId] bigint  NULL,
  [LastModifyTime] datetime  NULL,
  [LastModifyUserId] bigint  NULL,
  [CreateTime] datetime  NULL,
  [Tags] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [LoginUserType] int DEFAULT 0 NULL,
  [LoginUserId] bigint  NULL
)
GO

ALTER TABLE [dbo].[SysUser] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户名',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'UserName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'密码',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'Password'
GO

EXEC sp_addextendedproperty
'MS_Description', N'密码盐值',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'Salt'
GO

EXEC sp_addextendedproperty
'MS_Description', N'姓名',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'RealName'
GO

EXEC sp_addextendedproperty
'MS_Description', N'所属部门Id',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'DepartmentId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'性别(0未知 1男 2女)',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'Gender'
GO

EXEC sp_addextendedproperty
'MS_Description', N'出生日期',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'Birthday'
GO

EXEC sp_addextendedproperty
'MS_Description', N'头像',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'Portrait'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Email',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'Email'
GO

EXEC sp_addextendedproperty
'MS_Description', N'手机',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'Mobile'
GO

EXEC sp_addextendedproperty
'MS_Description', N'QQ',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'QQ'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'WeChat'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录次数',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'LoginCount'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户状态(0禁用 1启用)',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'UserStatus'
GO

EXEC sp_addextendedproperty
'MS_Description', N'系统用户(0/null不是 1是[系统用户拥有所有的权限])',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'IsSystem'
GO

EXEC sp_addextendedproperty
'MS_Description', N'在线(0不是 1是)',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'IsOnline'
GO

EXEC sp_addextendedproperty
'MS_Description', N'首次登录时间',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'FirstVisit'
GO

EXEC sp_addextendedproperty
'MS_Description', N'上一次登录时间',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'PreviousVisit'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后一次登录时间',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'LastVisit'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'Remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'相片',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'Picture'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'CreateUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除时间',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'DeleteTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'DeleteUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改时间',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'LastModifyTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'LastModifyUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'个人标签',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'Tags'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录用户类型',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'LoginUserType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录用户对应表Id',
'SCHEMA', N'dbo',
'TABLE', N'SysUser',
'COLUMN', N'LoginUserId'
GO


-- ----------------------------
-- Records of SysUser
-- ----------------------------
INSERT INTO [dbo].[SysUser] ([Id], [IsDelete], [UserName], [Password], [Salt], [RealName], [DepartmentId], [Gender], [Birthday], [Portrait], [Email], [Mobile], [QQ], [WeChat], [LoginCount], [UserStatus], [IsSystem], [IsOnline], [FirstVisit], [PreviousVisit], [LastVisit], [Remark], [Picture], [ApiToken], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [CreateTime], [Tags], [LoginUserType], [LoginUserId]) VALUES (N'16508640061130150', N'0', N'wangxue', N'ae51fabc8d8217a7242a58bb76ca62c1', N'10184', N'王雪', N'16508640061124408', N'1', N'1993-10-06', N'', N'', N'15612345678', N'', N'', N'1', N'1', N'0', N'1', N'2019-09-21 10:48:03.000', N'2019-09-21 10:48:03.000', N'2019-09-21 10:48:03.000', N'', NULL, NULL, NULL, NULL, NULL, N'2021-08-19 17:31:16.303', N'16508640061130151', NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUser] ([Id], [IsDelete], [UserName], [Password], [Salt], [RealName], [DepartmentId], [Gender], [Birthday], [Portrait], [Email], [Mobile], [QQ], [WeChat], [LoginCount], [UserStatus], [IsSystem], [IsOnline], [FirstVisit], [PreviousVisit], [LastVisit], [Remark], [Picture], [ApiToken], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [CreateTime], [Tags], [LoginUserType], [LoginUserId]) VALUES (N'16508640061130151', N'0', N'admin', N'b4e24a62fb8b61b0af033b0a30b20df1', N'98648', N'管理员', N'16508640061124402', N'1', N'2019-01-01', N'/Resource/Portrait/2021/05/19/1621388220355.jpg', N'admin@163.com', N'15766666666', N'810938177', N's810938177', N'46787', N'1', N'1', N'1', N'2018-12-12 16:00:10.000', N'2021-10-15 09:42:39.103', N'2021-10-15 09:42:39.103', N'123123', NULL, N'776585632e1248588ffc326218def49d', NULL, NULL, NULL, N'2021-10-15 09:42:39.140', N'0', NULL, N'超管', NULL, NULL)
GO

INSERT INTO [dbo].[SysUser] ([Id], [IsDelete], [UserName], [Password], [Salt], [RealName], [DepartmentId], [Gender], [Birthday], [Portrait], [Email], [Mobile], [QQ], [WeChat], [LoginCount], [UserStatus], [IsSystem], [IsOnline], [FirstVisit], [PreviousVisit], [LastVisit], [Remark], [Picture], [ApiToken], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [CreateTime], [Tags], [LoginUserType], [LoginUserId]) VALUES (N'347386923079176192', N'0', N'ces', N'27d8cd7ace1823f0fabb026c76328427', N'45689', N'123', N'16508640061124403', N'1', N'2021-08-16', NULL, NULL, N'123', NULL, NULL, N'9', N'1', NULL, N'1', NULL, N'2021-08-19 17:23:30.963', N'2021-08-19 17:23:30.963', N'', NULL, N'7ebf0c65c5064ef4b0ee311d1e33b901', N'16508640061130151', NULL, NULL, N'2021-08-19 17:23:30.973', N'0', N'2021-08-16 14:31:36.610', NULL, NULL, NULL)
GO


-- ----------------------------
-- Table structure for SysUserBelong
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SysUserBelong]') AND type IN ('U'))
	DROP TABLE [dbo].[SysUserBelong]
GO

CREATE TABLE [dbo].[SysUserBelong] (
  [Id] bigint  NOT NULL,
  [CreateTime] datetime  NOT NULL,
  [UserId] bigint  NOT NULL,
  [BelongId] bigint  NOT NULL,
  [BelongType] int  NOT NULL,
  [CreateUserId] bigint  NULL,
  [DeleteTime] datetime  NULL,
  [DeleteUserId] bigint  NULL,
  [LastModifyTime] datetime  NULL,
  [LastModifyUserId] bigint  NULL,
  [IsDelete] bit  NULL
)
GO

ALTER TABLE [dbo].[SysUserBelong] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户Id',
'SCHEMA', N'dbo',
'TABLE', N'SysUserBelong',
'COLUMN', N'UserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'职位Id或者角色Id',
'SCHEMA', N'dbo',
'TABLE', N'SysUserBelong',
'COLUMN', N'BelongId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'所属类型(1职位 2角色)',
'SCHEMA', N'dbo',
'TABLE', N'SysUserBelong',
'COLUMN', N'BelongType'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysUserBelong',
'COLUMN', N'CreateUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除时间',
'SCHEMA', N'dbo',
'TABLE', N'SysUserBelong',
'COLUMN', N'DeleteTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysUserBelong',
'COLUMN', N'DeleteUserId'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改时间',
'SCHEMA', N'dbo',
'TABLE', N'SysUserBelong',
'COLUMN', N'LastModifyTime'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后修改人ID编号',
'SCHEMA', N'dbo',
'TABLE', N'SysUserBelong',
'COLUMN', N'LastModifyUserId'
GO


-- ----------------------------
-- Records of SysUserBelong
-- ----------------------------
INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'259363197956395008', N'2020-12-16 16:56:45.127', N'259345304376053760', N'258606017569361920', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'259363198329688064', N'2020-12-16 16:56:45.217', N'259345304376053760', N'16508640061130140', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'259363198677815296', N'2020-12-16 16:56:45.300', N'259345304376053760', N'16508640061130141', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'259426846125330432', N'2020-12-16 21:09:40.037', N'16508640061130151', N'258606017569361920', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'259426846565732352', N'2020-12-16 21:09:40.140', N'16508640061130151', N'16508640061130140', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'259432564475826176', N'2020-12-16 21:32:23.397', N'16508640061130152', N'258606017569361920', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'259432564895256576', N'2020-12-16 21:32:23.497', N'16508640061130152', N'16508640061130141', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'259432565268549632', N'2020-12-16 21:32:23.587', N'16508640061130152', N'16508640061130140', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'283461040950218752', N'2021-02-21 04:52:58.617', N'16508640061130150', N'258606017569361920', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'283461041021521920', N'2021-02-21 04:52:58.630', N'16508640061130150', N'277766285150916608', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'283461041080242176', N'2021-02-21 04:52:58.647', N'16508640061130150', N'16508640061130140', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'286770553761566720', N'2021-03-02 08:03:47.957', N'286770553635737600', N'277766285150916608', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'286770553816092672', N'2021-03-02 08:03:47.970', N'286770553635737600', N'286619174082449408', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'286770553853841408', N'2021-03-02 08:03:47.977', N'286770553635737600', N'16508640061130139', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'286770553883201536', N'2021-03-02 08:03:47.987', N'286770553635737600', N'16508640061130140', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'286770553912561664', N'2021-03-02 08:03:47.993', N'286770553635737600', N'16508640061130141', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'286770553941921792', N'2021-03-02 08:03:48.000', N'286770553635737600', N'16508640061130143', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'286770553971281920', N'2021-03-02 08:03:48.007', N'286770553635737600', N'16508640061130142', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'286770554000642048', N'2021-03-02 08:03:48.013', N'286770553635737600', N'16508640061130144', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'286770554025807872', N'2021-03-02 08:03:48.020', N'286770553635737600', N'16508640061130145', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'287262428117340160', N'2021-03-03 16:38:19.940', N'287262427932790784', N'258606017569361920', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'287262428171866112', N'2021-03-03 16:38:19.953', N'287262427932790784', N'277766285150916608', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'287262428205420544', N'2021-03-03 16:38:19.963', N'287262427932790784', N'16508640061130139', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'297014870753153024', N'2021-03-30 14:31:03.457', N'297014869461307392', N'258606017569361920', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'297014871122251776', N'2021-03-30 14:31:03.547', N'297014869461307392', N'16508640061130142', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'297016001441042432', N'2021-03-30 14:35:33.033', N'297016000946114560', N'258606017569361920', N'2', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'297016001784975360', N'2021-03-30 14:35:33.117', N'297016000946114560', N'16508640061130141', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'297376920335552512', N'2021-03-31 14:29:42.803', N'297308940515938304', N'258606017569361920', N'2', N'16508640061130151', NULL, NULL, N'2021-03-31 14:29:42.803', N'16508640061130151', N'0')
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'297376920574627840', N'2021-03-31 14:29:42.860', N'297308940515938304', N'16508640061130142', N'1', N'16508640061130151', NULL, NULL, N'2021-03-31 14:29:42.860', N'16508640061130151', N'0')
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'297377008873115648', N'2021-03-31 14:30:03.913', N'297377008554348544', N'258606017569361920', N'2', N'16508640061130151', NULL, NULL, N'2021-03-31 14:30:03.913', N'16508640061130151', N'0')
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'297377009095413760', N'2021-03-31 14:30:03.967', N'297377008554348544', N'16508640061130141', N'1', N'16508640061130151', NULL, NULL, N'2021-03-31 14:30:03.967', N'16508640061130151', N'0')
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'313344285162344448', N'2021-05-14 15:58:19.240', N'313344284575141888', N'258606017569361920', N'2', N'16508640061130151', NULL, NULL, N'2021-05-14 15:58:19.243', N'16508640061130151', N'0')
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'313344285439168512', N'2021-05-14 15:58:19.303', N'313344284575141888', N'16508640061130142', N'1', N'16508640061130151', NULL, NULL, N'2021-05-14 15:58:19.303', N'16508640061130151', N'0')
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'347383742655893504', N'2021-08-16 14:18:58.327', N'347383742500704256', N'258606017569361920', N'2', N'16508640061130151', NULL, NULL, N'2021-08-16 14:18:58.330', N'16508640061130151', N'0')
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'347383742676865024', N'2021-08-16 14:18:58.333', N'347383742500704256', N'16508640061130142', N'1', N'16508640061130151', NULL, NULL, N'2021-08-16 14:18:58.333', N'16508640061130151', N'0')
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'348412980892798976', N'2021-08-19 10:28:47.847', N'347386923079176192', N'348410673211904000', N'2', N'16508640061130151', NULL, NULL, N'2021-08-19 10:28:47.847', N'16508640061130151', N'0')
GO

INSERT INTO [dbo].[SysUserBelong] ([Id], [CreateTime], [UserId], [BelongId], [BelongType], [CreateUserId], [DeleteTime], [DeleteUserId], [LastModifyTime], [LastModifyUserId], [IsDelete]) VALUES (N'348412980917964800', N'2021-08-19 10:28:47.850', N'347386923079176192', N'16508640061130142', N'1', N'16508640061130151', NULL, NULL, N'2021-08-19 10:28:47.850', N'16508640061130151', N'0')
GO


-- ----------------------------
-- Primary Key structure for table SysApiAuthorize
-- ----------------------------
ALTER TABLE [dbo].[SysApiAuthorize] ADD CONSTRAINT [PK__SysApiAu__3214EC07F7C5DE0B] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysAutoJob
-- ----------------------------
ALTER TABLE [dbo].[SysAutoJob] ADD CONSTRAINT [PK_SysAutoJob] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysAutoJobLog
-- ----------------------------
ALTER TABLE [dbo].[SysAutoJobLog] ADD CONSTRAINT [PK_SysAutoJobLog] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysCodeTemplet
-- ----------------------------
ALTER TABLE [dbo].[SysCodeTemplet] ADD CONSTRAINT [PK__SysCodeT__3214EC07E6AD90F3] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysDataDict
-- ----------------------------
ALTER TABLE [dbo].[SysDataDict] ADD CONSTRAINT [PK_SysDataDict] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysDataDictDetail
-- ----------------------------
ALTER TABLE [dbo].[SysDataDictDetail] ADD CONSTRAINT [PK_SysDataDictDetail] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysDepartment
-- ----------------------------
ALTER TABLE [dbo].[SysDepartment] ADD CONSTRAINT [PK_SysDepartment] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysLogApi
-- ----------------------------
ALTER TABLE [dbo].[SysLogApi] ADD CONSTRAINT [PK_SysLogApi] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysLogLogin
-- ----------------------------
ALTER TABLE [dbo].[SysLogLogin] ADD CONSTRAINT [PK_SysLogLogin] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysMenu
-- ----------------------------
ALTER TABLE [dbo].[SysMenu] ADD CONSTRAINT [PK_SysMenu] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysMenuAuthorize
-- ----------------------------
ALTER TABLE [dbo].[SysMenuAuthorize] ADD CONSTRAINT [PK_SysMenuAuthorize] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysPosition
-- ----------------------------
ALTER TABLE [dbo].[SysPosition] ADD CONSTRAINT [PK_SysPosition] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysRole
-- ----------------------------
ALTER TABLE [dbo].[SysRole] ADD CONSTRAINT [PK_SysRole] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysTest
-- ----------------------------
ALTER TABLE [dbo].[SysTest] ADD CONSTRAINT [PK__SysTest__3214EC0708CC6546] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysUser
-- ----------------------------
ALTER TABLE [dbo].[SysUser] ADD CONSTRAINT [PK_SysUser] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SysUserBelong
-- ----------------------------
ALTER TABLE [dbo].[SysUserBelong] ADD CONSTRAINT [PK_SysUserBelong] PRIMARY KEY CLUSTERED ([Id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

